﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Net;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Monitor.Server;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Process.Log
{
    internal static class LogExtensions
    {
        internal static void AddErrors(this LogBase log, IEnumerable<string> errors)
        {
            foreach (string error in errors)
                log.WriteLine(error, Severity.Error);
        }

        internal static void ClusterError(this LogBase log, Exception e)
        {
            log.WriteLine("Error during initialization of cluster node.", Severity.Error);
            log.WriteLine(e);
        }

        internal static void ClusterReady(this LogBase log, NodeConfiguration node)
        {
            log.WriteLine($"Requesting to cluster {node.Address}", Severity.Notice);
        }

        internal static void ConfigFile(this LogBase log, string file)
        {
            log.WriteLine($"Using configuration {file}", Severity.Notice);
        }

        internal static void DatabaseError(this LogBase log, Exception e)
        {
            log.WriteLine("Error during initialization of database.", Severity.Error);
            log.WriteLine(e);
        }

        internal static void HostImportError(this LogBase log, Exception e)
        {
            log.WriteLine($"Could not import host data {e.Message}", Severity.Warn);
            log.WriteLine(e, Severity.Trace);
        }

        internal static void HostNotFound(this LogBase log, int hostId)
        {
            log.WriteLine($"Host with ID {hostId} does not exists in database.", Severity.Warn);
        }

        internal static void InitializationError(this LogBase log, Exception e)
        {
            log.WriteLine($"Cannot create logs: {e.Message}", Severity.Error);
            log.WriteLine(e);
        }

        internal static void InstanceName(this LogBase log, string name)
        {
            log.WriteLine($"Instance named {name}", Severity.Notice);
        }

        internal static void JobInitEnd(this LogBase log)
        {
            log.WriteLine("All jobs initialized.", Severity.Debug);
        }

        internal static void JobInitStart(this LogBase log)
        {
            log.WriteLine("Initializing jobs.", Severity.Info);
        }

        internal static void KnownServer(this LogBase log, int id, string name)
        {
            log.WriteLine(
                $"Found known time server {name} with id {id} in list.",
                Severity.Info);
        }

        internal static void ListenerError(this LogBase log, Exception e)
        {
            log.WriteLine("Error during initialization of command channels.", Severity.Warn);
            log.WriteLine(e);
        }

        internal static void ListenerReady(this LogBase log, Listener listener)
        {
            log.WriteLine($"Opening command channel on {listener}", Severity.Notice);
        }

        internal static void NewHost(this LogBase log, HostConfiguration server, IPAddress ip)
        {
            log.WriteLine(
                $"Created a new host in database ID {server.HostId} with name {server.ServerName} and IP {ip}.",
                Severity.Info);
        }

        internal static void NewPeer(this LogBase log, AssociationEntry entry)
        {
            log.WriteLine(
                $"Created a new peer in database with name {entry.Remote} and IP {entry.Remote}.",
                Severity.Info);
        }

        internal static void NoConfig(this LogBase log, string file)
        {
            log.WriteLine($"Cannot find configuration file {file}", Severity.Error);
        }

        internal static void OpenNtpConfigError(this LogBase log)
        {
            log.WriteLine("Traffic data collection is not supported for OpenNTP.", Severity.Warn);
        }

        internal static void PeerImportError(this LogBase log, Exception e)
        {
            log.WriteLine($"Could not import peer data {e.Message}", Severity.Warn);
            log.WriteLine(e, Severity.Trace);
        }

        internal static void PeerIpAmbiguous(this LogBase log, AssociationEntry entry)
        {
            log.WriteLine(
                $"Ambiguous peer IP {entry.Remote}. Multiple occurrences found in database.",
                Severity.Warn);
        }

        internal static void PeerIpNotFound(this LogBase log, AssociationEntry entry)
        {
            log.WriteLine($"Peer with IP {entry.Remote} does not exists in database.", Severity.Warn);
        }

        internal static void PidFileError(this LogBase log, Exception e)
        {
            log.WriteLine($"Cannot create pid file. {e.Message}", Severity.Warn);
        }

        internal static void ProcessId(this LogBase log, int pid)
        {
            log.WriteLine($"Running with process ID {pid}", Severity.Notice);
        }

        internal static void SchedulerError(this LogBase log, Exception e)
        {
            log.WriteLine("Error during initialization of scheduler.", Severity.Error);
            log.WriteLine(e);
        }

        internal static void Starting(this LogBase log, string version)
        {
            log.WriteLine($"NTP Analyzer {version} started.", Severity.Notice);
        }

        internal static void TableError(this LogBase log, Exception e)
        {
            log.WriteLine("Could not populate tables with data.", Severity.Error);
            log.WriteLine(e);
        }

        internal static void UserId(this LogBase log, uint userId)
        {
            log.WriteLine($"Running under user id {userId}", Severity.Info);
        }

        internal static void UserIdError(this LogBase log, uint userId)
        {
            log.WriteLine($"Failed to set user id {userId}", Severity.Warn);
        }

        internal static void HostNameNotFound(this LogBase log, string ip, Exception e)
        {
            log.WriteLine($"Could not find host name for IP {ip}.", Severity.Info);
            log.WriteLine(e, Severity.Debug);
        }
    }
}