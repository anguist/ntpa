// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.IO;
using Ntp.Analyzer.Export;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process.Description
{
    public abstract class StreamJob : JobDescription
    {
        protected StreamJob(IJobConfiguration config, LogBase log)
            : base(config, log)
        {
        }

        protected void SaveStream(IStreamGenerator generator)
        {
            Stream stream = null;
            try
            {
                stream = generator.Generate();
                foreach (var destination in generator.Destinations)
                {
                    stream.Position = 0;
                    destination.Write(stream, Log);
                }
            }
            finally
            {
                stream?.Dispose();
            }
        }

        protected void SaveStream(IStreamGenerator generator, string file)
        {
            Stream stream = null;
            try
            {
                stream = generator.Generate();
                foreach (var destination in generator.Destinations)
                {
                    stream.Position = 0;
                    destination.Param = file;
                    destination.Write(stream, Log);
                }
            }
            finally
            {
                stream?.Dispose();
            }
        }
    }
}