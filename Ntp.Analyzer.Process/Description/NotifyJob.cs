// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Text;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Monitor.Server;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process.Description
{
    public sealed class NotifyJob : JobDescription, IDisposable
    {
        public NotifyJob(
            NotifyConfiguration config,
            LogBase log,
            int pid,
            string configFile
            )
            : base(config, log)
        {
            this.config = config;
            this.pid = pid;
            this.configFile = configFile;
            jobs = new List<Job>();

            client = new SmtpClient(config.SmptServer, config.SmtpPort);

            if (config.SmtpUser != null)
            {
                client.Credentials = new NetworkCredential(config.SmtpUser, config.SmtpPass ?? string.Empty);
            }

            client.EnableSsl = config.EnableSsl;
        }

        private readonly SmtpClient client;
        private readonly NotifyConfiguration config;
        private readonly string configFile;
        private readonly List<Job> jobs;
        private readonly int pid;

        public override ThreadType ThreadType => ThreadType.MultiThreaded;

        public override string JobType => "Notification mail";

        public override int Priority => 99;

        protected override void InternalExecute()
        {
            var message = new MailMessage(
                config.Sender,
                config.Mail,
                config.Subject,
                BuildContent()
                );

            client.Send(message);
        }

        /// <summary>
        /// Sets the job list to be monitored.
        /// </summary>
        /// <param name="items">Scheduled items.</param>
        internal void SetJobList(IEnumerable<Job> items)
        {
            jobs.AddRange(items);
        }

        /// <summary>
        /// Builds the message content.
        /// </summary>
        /// <returns>The content.</returns>
        private string BuildContent()
        {
            var builder = new StringBuilder();
            builder.AppendLine("List of running items in NTP Analyzer");
            builder.Append(Billboard.Jobs(jobs));
            builder.Append("Using configuration: ");
            builder.AppendLine(configFile);
            builder.Append("Running with PID: ");
            builder.AppendLine(pid.ToString(CultureInfo.InvariantCulture));
            return builder.ToString();
        }

        #region IDisposable Support

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                client.Dispose();
            }
        }

        ~NotifyJob()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}