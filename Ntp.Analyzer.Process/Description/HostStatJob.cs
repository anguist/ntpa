// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Import;
using Ntp.Analyzer.Objects;
using Ntp.Analyzer.Process.Log;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process.Description
{
    /// <summary>
    /// Job which read statistics about an ntp host and saves the result to a database.
    /// </summary>
    public sealed class HostStatJob : JobDescription
    {
        public HostStatJob(StatConfiguration config, LogBase log)
            : base(config, log)
        {
            this.config = config;
        }

        private readonly StatConfiguration config;

        public override ThreadType ThreadType => ThreadType.MultiThreaded;

        public override string JobType => "Host statistics";

        public override int Priority => 2;

        protected override void InternalExecute()
        {
            var host = DataFace.Instance.Hosts.SingleOrDefault(h => h.Id == config.HostId);
            if (host == null)
            {
                Log.HostNotFound(config.HostId);
                return;
            }

            var bulk = config.Bulk == null
                ? new ReadingBulk(host.Name, config.TimeStamp)
                : DataFace.Instance.ReadingBulks.Single(b => b.Name == config.Bulk.Name);

            var importer = ImportFactory.CreateHostImporter(config.ServerName, config.ServerType, host, bulk, Log);

            try
            {
                importer.Execute();
            }
            catch (Exception e)
            {
                Log.HostImportError(e);
                return;
            }

            foreach (var reading in importer)
            {
                DataFace.Instance.HostReadings.Save(reading);
            }
        }
    }
}