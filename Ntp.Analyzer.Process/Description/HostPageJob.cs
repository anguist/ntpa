// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Linq;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Page;
using Ntp.Analyzer.Process.Log;
using Ntp.Analyzer.Statistics;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process.Description
{
    public sealed class HostPageJob : StreamJob
    {
        public HostPageJob(MenuConfiguration menu, HostPageConfiguration page, LogBase log)
            : base(page, log)
        {
            this.menu = menu;
            this.page = page;
        }

        private readonly MenuConfiguration menu;
        private readonly HostPageConfiguration page;

        public override ThreadType ThreadType => ThreadType.MultiThreaded;

        public override string JobType => "Host page build";

        public override int Priority => 10;

        protected override void InternalExecute()
        {
            var host = DataFace.Instance.Hosts.SingleOrDefault(h => h.Id == page.HostId);
            if (host == null)
            {
                Log.HostNotFound(page.HostId);
                return;
            }

            var builder = page.QueryDirect
                ? new DirectStatusBuilder(host, page.ServerName, page.ServerType, Log)
                : (StatusBuilder) new DbStatusBuilder(host);

            builder.Build();

            var pageBuilder = page.Theme == PageTheme.Bootstrap
                ? new BootstrapHostPageBuilder(menu, page, builder.Entries.ToArray())
                : (PageBuilderBase) new DefaultHostPageBuilder(page, builder.Entries.ToArray());

            SaveStream(pageBuilder);
        }
    }
}