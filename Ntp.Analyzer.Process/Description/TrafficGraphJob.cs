// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Linq;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Graph;
using Ntp.Analyzer.Process.Log;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process.Description
{
    public sealed class TrafficGraphJob : StreamJob
    {
        public TrafficGraphJob(TrafficGraphConfiguration config, LogBase log)
            : base(config, log)
        {
            this.config = config;
        }

        private readonly TrafficGraphConfiguration config;

        public override ThreadType ThreadType => ThreadType.SingleThreaded;

        public override string JobType => "Traffic graph generation";

        public override int Priority => 20;

        protected override void InternalExecute()
        {
            var host = DataFace.Instance.Hosts.SingleOrDefault(h => h.Id == config.HostId);
            if (host == null)
            {
                Log.HostNotFound(config.HostId);
                return;
            }

            var graph = new TrafficGraph(config, host);
            SaveStream(graph);
        }
    }
}