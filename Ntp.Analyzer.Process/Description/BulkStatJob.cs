// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process.Description
{
    public sealed class BulkStatJob : JobDescription
    {
        public BulkStatJob(ReadingBulkConfiguration config, LogBase log)
            : base(config, log)
        {
            this.config = config;
        }

        private readonly ReadingBulkConfiguration config;

        public override ThreadType ThreadType => ThreadType.NoThread;

        public override string JobType => "Bulk stat";

        public override int Priority => 1;

        protected override void InternalExecute()
        {
            var bulk = new ReadingBulk(config.Name, config.TimeStamp);
            DataFace.Instance.ReadingBulks.Save(bulk);
        }
    }
}