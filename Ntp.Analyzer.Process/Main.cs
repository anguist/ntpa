// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.IO;
using Ntp.Common.Log;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Process
{
    public sealed class Main
    {
        public Main(string configFile, int pid, string pidFile, string name, LogGroup initlog)
        {
            this.configFile = configFile;
            this.pid = pid;
            this.pidFile = pidFile;
            this.name = name;
            this.initlog = initlog;
        }

        private readonly string configFile;
        private readonly LogGroup initlog;
        private readonly string name;
        private readonly int pid;
        private readonly string pidFile;

        public void Run()
        {
            try
            {
                bool reload = true;
                while (reload)
                {
                    var i = new Initializer(configFile, pid, pidFile, name, initlog);
                    i.Run();

                    if (!i.Ready)
                        break;

                    var cluster = new Cluster(i.Scheduler, i.Controller, i.Nodes, i.Log);
                    cluster.Activate();

                    reload = i.Controller.Reload;

                    foreach (var listener in i.Listeners)
                        listener.Close();

                    LogFactory.Cleanup();
                }
            }
            finally
            {
                if (pidFile != null)
                {
                    try
                    {
                        File.Delete(pidFile);
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
        }
    }
}