﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Text;
using System.Threading;
using Ntp.Common.Log;

namespace Ntp.Common.App
{
    public class Heartbeat
    {
        public Heartbeat(LogBase log, int interval)
        {
            this.log = log;
            this.interval = interval;
        }

        private readonly int interval;
        private readonly LogBase log;
        private bool alive;
        private int beats;
        private DateTime start;

        public void Start()
        {
            start = DateTime.Now;
            alive = true;
            beats = 0;

            var thread = new Thread(Pulse)
            {
                Name = "Heartbeat",
                IsBackground = true,
                Priority = ThreadPriority.AboveNormal
            };

            thread.Start();
            log.HeartbeatStarted(interval);
        }

        public void Stop()
        {
            alive = false;
        }

        private void Pulse()
        {
            while (alive)
            {
                int next = (beats + 1)*interval;
                var sleep = start.AddMinutes(next).Subtract(DateTime.Now).TotalMilliseconds;
                Thread.Sleep(Convert.ToInt32(sleep));
                beats++;

                var span = TimeSpan.FromMinutes(beats*interval);
                var builder = new StringBuilder();

                if (span.Days != 0)
                {
                    builder.Append(span.Days);
                    builder.Append(" day");
                }

                if (span.Days > 1)
                {
                    builder.Append("s");
                }

                if (span.Days >= 1)
                {
                    if (span.Hours != 0 && span.Minutes == 0)
                    {
                        builder.Append(" and ");
                    }
                    else if (span.Hours == 0 && span.Minutes != 0)
                    {
                        builder.Append(" and ");
                    }
                    else if (span.Hours != 0 && span.Minutes != 0)
                    {
                        builder.Append(", ");
                    }
                }

                if (span.Hours != 0)
                {
                    builder.Append(span.Hours);
                    builder.Append(" hour");
                }

                if (span.Hours > 1)
                {
                    builder.Append("s");
                }

                if (span.Hours >= 1 && span.Minutes != 0)
                {
                    builder.Append(" ");
                }

                if (span.Hours != 0 && span.Minutes != 0)
                {
                    builder.Append("and ");
                }

                if (span.Minutes != 0)
                {
                    builder.Append(span.Minutes);
                    builder.Append(" minute");
                }

                if (span.Minutes > 1)
                {
                    builder.Append("s");
                }

                log.HeartbeatUptime(builder.ToString());
            }
        }
    }
}