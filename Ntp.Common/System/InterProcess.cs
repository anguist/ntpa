﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Mono.Unix;
using Mono.Unix.Native;
using Ntp.Common.Log;

namespace Ntp.Common.System
{
    public static class InterProcess
    {
        public enum Signal
        {
            Error = 0,
            Exit,
            Reload,
            Refresh,
            Notify
        }

        private static UnixSignal sigterm, sigquit, sigint, sighup, sigusr1, sigusr2;

        private static readonly UnixSignal[] Signals =
        {
            sigint = new UnixSignal(Signum.SIGINT),
            sigterm = new UnixSignal(Signum.SIGTERM),
            sigquit = new UnixSignal(Signum.SIGQUIT),
            sighup = new UnixSignal(Signum.SIGHUP),
            sigusr1 = new UnixSignal(Signum.SIGUSR1),
            sigusr2 = new UnixSignal(Signum.SIGUSR2)
        };

        public static Signal Wait(string name, LogBase log)
        {
            while (true)
            {
                int i;
                try
                {
                    i = UnixSignal.WaitAny(Signals, -1);
                }
                catch (Exception e)
                {
                    log.SignalError(e);
                    return Signal.Error;
                }

                if (i < 0 || i >= Signals.Length)
                {
                    log.UnknownSignal(Signals[i].Signum.ToString());
                    continue;
                }

                log.ReceivedSignal(Signals[i].Signum.ToString());

                if (sigint.IsSet || sigterm.IsSet || sigquit.IsSet)
                {
                    sigint.Reset();
                    sigterm.Reset();
                    sigquit.Reset();
                    return Signal.Exit;
                }

                if (sighup.IsSet)
                {
                    sighup.Reset();
                    return Signal.Refresh;
                }

                if (sigusr1.IsSet)
                {
                    sigusr1.Reset();
                    return Signal.Reload;
                }

                if (sigusr2.IsSet)
                {
                    sigusr2.Reset();
                    return Signal.Notify;
                }
            }
        }
    }
}