// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace Ntp.Common.Process
{
    /// <summary>
    /// A job which have been scheduled for execution.
    /// </summary>
    public sealed class ScheduledJob
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledJob" /> class.
        /// </summary>
        /// <param name="job">Job.</param>
        /// <param name="run">Run.</param>
        public ScheduledJob(Job job, DateTime run)
        {
            Job = job;
            Run = run;
        }

        /// <summary>
        /// Gets the job to execute.
        /// </summary>
        /// <value>The job.</value>
        public Job Job { get; }

        /// <summary>
        /// Gets the time of planned execution.
        /// </summary>
        /// <value>The run.</value>
        public DateTime Run { get; }

        /// <summary>
        /// Returns a <see cref="string" /> that represents the current <see cref="ScheduledJob" />.
        /// </summary>
        /// <returns>A <see cref="string" /> that represents the current <see cref="ScheduledJob" />.</returns>
        public override string ToString()
        {
            return $"{Job} scheduled to run {Run.ToString("HH:mm:ss")}";
        }
    }
}