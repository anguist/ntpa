// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Common.Log;

namespace Ntp.Common.Process
{
    /// <summary>
    /// Base class for jobs following the GoF Command Pattern.
    /// </summary>
    public abstract class JobDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobDescription" /> class.
        /// </summary>
        /// <param name="config">Configuration for the job.</param>
        /// <param name="log">Log to use when registering events.</param>
        protected JobDescription(IJobConfiguration config, LogBase log)
        {
            Configuration = config;
            Log = log;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name => Configuration.ConfigName;

        /// <summary>
        /// Gets a value indicating whether this <see cref="JobDescription" /> should run as a single thread.
        /// </summary>
        /// <value><c>true</c> if single thread; otherwise, <c>false</c>.</value>
        public abstract ThreadType ThreadType { get; }

        /// <summary>
        /// Gets the type of the job as text.
        /// </summary>
        /// <value>The type of the job.</value>
        public abstract string JobType { get; }

        /// <summary>
        /// Gets the priority to use when scheduling jobs.
        /// </summary>
        /// <value>The priority.</value>
        public abstract int Priority { get; }

        /// <summary>
        /// Gets the configuration for the job.
        /// </summary>
        /// <value>The configuration.</value>
        public IJobConfiguration Configuration { get; }

        /// <summary>
        /// Gets the log to use when registering events.
        /// </summary>
        /// <value>The log.</value>
        protected LogBase Log { get; }

        /// <summary>
        /// Returns a <see cref="string" /> that represents the current <see cref="JobDescription" />.
        /// </summary>
        /// <returns>A <see cref="string" /> that represents the current <see cref="JobDescription" />.</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Implementing method for descendants.
        /// </summary>
        protected abstract void InternalExecute();

        /// <summary>
        /// Perform execution of this instance.
        /// </summary>
        internal void Execute()
        {
            InternalExecute();
        }
    }
}