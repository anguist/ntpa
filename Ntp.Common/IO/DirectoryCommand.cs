﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using System.Linq;

namespace Ntp.Common.IO
{
    public static class DirectoryCommand
    {
        private static readonly object Locker = new object();

        public static string CreateRecursiveFromFile(string file)
        {
            int pos1 = file.LastIndexOf(Path.DirectorySeparatorChar);
            int pos2 = file.LastIndexOf(Path.AltDirectorySeparatorChar);

            int pos = pos1 > pos2 ? pos1 : pos2;
            if (pos == -1)
                return null;

            string path = file.Substring(0, pos);
            return CreateRecursive(path);
        }

        private static string CreateRecursive(string path)
        {
            // Make it thread safe
            lock (Locker)
            {
                string fullPath = path.Trim();
                string lastPath;

                // Trim
                do
                {
                    lastPath = fullPath;
                    fullPath = fullPath.TrimEnd().
                        TrimEnd(Path.DirectorySeparatorChar).
                        TrimEnd(Path.AltDirectorySeparatorChar);
                } while (lastPath != fullPath);

                if (fullPath.Length <= 1)
                    return null;

                // Find components
                string created = null;
                char[] s = {Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar};
                var elements = fullPath.Split(s, StringSplitOptions.RemoveEmptyEntries).ToList();

                if (elements.Count <= 1)
                    return null;

                // Find root
                string currentPath = elements[0];
                elements.RemoveAt(0);

                // Adjust Unix paths
                if (path.Trim()[0] == Path.DirectorySeparatorChar || path.Trim()[0] == Path.AltDirectorySeparatorChar)
                    currentPath = Path.DirectorySeparatorChar + currentPath;

                // Create it
                foreach (string element in elements)
                {
                    currentPath = $"{currentPath}{Path.DirectorySeparatorChar}{element}";
                    if (Directory.Exists(currentPath))
                        continue;

                    Directory.CreateDirectory(currentPath);
                    created = currentPath;
                }

                return created;
            }
        }
    }
}