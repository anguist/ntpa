﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Mono.Unix.Native;

namespace Ntp.Common.IO
{
    public static class Permission
    {
        public static bool ChangeFileMode(string file, uint mode)
        {
            // Changing file mode is only supported on Unix platforms
            if (Environment.OSVersion.Platform != PlatformID.Unix)
                return true;

            var permissions = NativeConvert.FromOctalPermissionString(mode.ToString());
            return Syscall.chmod(file, permissions) == 0;
        }

        public static bool ChangeFileOwner(string file, uint user, uint? group)
        {
            // Changing file owner is only supported on Unix platforms
            if (Environment.OSVersion.Platform != PlatformID.Unix)
                return true;

            return Syscall.chown(file, user, group ?? unchecked((uint) -1)) == 0;
        }

        public static uint? GetGroupId(string groupName)
        {
            // Group IDs are only supported on Unix platforms
            if (Environment.OSVersion.Platform != PlatformID.Unix)
                return 0;

            var gr = Syscall.getgrnam(groupName);
            return gr?.gr_gid;
        }

        public static uint? GetUserId(string userName)
        {
            // User IDs are only supported on Unix platforms
            if (Environment.OSVersion.Platform != PlatformID.Unix)
                return 0;

            var pw = Syscall.getpwnam(userName);
            return pw?.pw_uid;
        }

        public static bool SetUserId(uint userId)
        {
            // Setting user ID is only supported on Unix platforms
            if (Environment.OSVersion.Platform != PlatformID.Unix)
                return true;

            try
            {
                Syscall.setuid(userId);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}