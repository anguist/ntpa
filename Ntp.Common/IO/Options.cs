// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using MessageLocalizerConverter = System.Converter<string, string>;

/**
 * Authors:
 *  Jonathan Pryor <jpryor@novell.com>
 *  Federico Di Gregorio <fog@initd.org>
 *  Rolf Bjarne Kvinge <rolf@xamarin.com>
 *
 * Copyright (C) 2008 Novell (http://www.novell.com)
 * Copyright (C) 2009 Federico Di Gregorio.
 * Copyright (C) 2012 Xamarin Inc (http://www.xamarin.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 * Compile With:
 *   gmcs -debug+ -r:System.Core Options.cs -o:NDesk.Options.dll
 *   gmcs -debug+ -d:LINQ -r:System.Core Options.cs -o:NDesk.Options.dll
 *
 * The LINQ version just changes the implementation of
 * OptionSet.Parse(IEnumerable<string>), and confers no semantic changes.
 *
 *
 * A Getopt::Long-inspired option parsing library for C#.
 *
 * NDesk.Options.OptionSet is built upon a key/value table, where the
 * key is a option format string and the value is a delegate that is 
 * invoked when the format string is matched.
 *
 * Option format strings:
 *  Regex-like BNF Grammar: 
 *    name: .+
 *    type: [=:]
 *    sep: ( [^{}]+ | '{' .+ '}' )?
 *    aliases: ( name type sep ) ( '|' name type sep )*
 * 
 * Each '|'-delimited name is an alias for the associated action.  If the
 * format string ends in a '=', it has a required value.  If the format
 * string ends in a ':', it has an optional value.  If neither '=' or ':'
 * is present, no value is supported.  `=' or `:' need only be defined on one
 * alias, but if they are provided on more than one they must be consistent.
 *
 * Each alias portion may also end with a "key/value separator", which is used
 * to split option values if the option accepts > 1 value.  If not specified,
 * it defaults to '=' and ':'.  If specified, it can be any character except
 * '{' and '}' OR the *string* between '{' and '}'.  If no separator should be
 * used (i.e. the separate values should be distinct arguments), then "{}"
 * should be used as the separator.
 *
 * Options are extracted either from the current option by looking for
 * the option name followed by an '=' or ':', or is taken from the
 * following option IFF:
 *  - The current option does not contain a '=' or a ':'
 *  - The current option requires a value (i.e. not a Option type of ':')
 *
 * The `name' used in the option format string does NOT include any leading
 * option indicator, such as '-', '--', or '/'.  All three of these are
 * permitted/required on any named option.
 *
 * Option bundling is permitted so long as:
 *   - '-' is used to start the option group
 *   - all of the bundled options are a single character
 *   - at most one of the bundled options accepts a value, and the value
 *     provided starts from the next character to the end of the string.
 *
 * This allows specifying '-a -b -c' as '-abc', and specifying '-D name=value'
 * as '-Dname=value'.
 *
 * Option processing is disabled by specifying "--".  All options after "--"
 * are returned by OptionSet.Parse() unchanged and unprocessed.
 *
 * Unprocessed options are returned from OptionSet.Parse().
 *
 * Examples:
 *  int verbose = 0;
 *  OptionSet p = new OptionSet ()
 *    .Add ("v", v => ++verbose)
 *    .Add ("name=|value=", v => Console.WriteLine (v));
 *  p.Parse (new string[]{"-v", "--v", "/v", "-name=A", "/name", "B", "extra"});
 *
 * The above would parse the argument string array, and would invoke the
 * lambda expression three times, setting `verbose' to 3 when complete.  
 * It would also print out "A" and "B" to standard output.
 * The returned array would contain the string "extra".
 *
 * C# 3.0 collection initializers are supported and encouraged:
 *  var p = new OptionSet () {
 *    { "h|?|help", v => ShowHelp () },
 *  };
 *
 * System.ComponentModel.TypeConverter is also supported, allowing the use of
 * custom data types in the callback type; TypeConverter.ConvertFromString()
 * is used to convert the value option to an instance of the specified
 * type:
 *
 *  var p = new OptionSet () {
 *    { "foo=", (Foo f) => Console.WriteLine (f.ToString ()) },
 *  };
 *
 * Random other tidbits:
 *  - Boolean options (those w/o '=' or ':' in the option format string)
 *    are explicitly enabled if they are followed with '+', and explicitly
 *    disabled if they are followed with '-':
 *      string a = null;
 *      var p = new OptionSet () {
 *        { "a", s => a = s },
 *      };
 *      p.Parse (new string[]{"-a"});   // sets v != null
 *      p.Parse (new string[]{"-a+"});  // sets v != null
 *      p.Parse (new string[]{"-a-"});  // sets v == null
 */

namespace Ntp.Common.IO
{
    internal static class StringCoda
    {
        public static IEnumerable<string> WrappedLines(string self, params int[] widths)
        {
            IEnumerable<int> w = widths;
            return WrappedLines(self, w);
        }

        public static IEnumerable<string> WrappedLines(string self, IEnumerable<int> widths)
        {
            if (widths == null)
                throw new ArgumentNullException(nameof(widths));
            return CreateWrappedLinesIterator(self, widths);
        }

        private static IEnumerable<string> CreateWrappedLinesIterator(string self, IEnumerable<int> widths)
        {
            if (string.IsNullOrEmpty(self))
            {
                yield return string.Empty;
                yield break;
            }
            using (var ewidths = widths.GetEnumerator())
            {
                bool? hw = null;
                var width = GetNextWidth(ewidths, int.MaxValue, ref hw);
                var start = 0;
                do
                {
                    var end = GetLineEnd(start, width, self);
                    var c = self[end - 1];
                    if (char.IsWhiteSpace(c))
                        --end;
                    var needContinuation = end != self.Length && !IsEolChar(c);
                    var continuation = "";
                    if (needContinuation)
                    {
                        --end;
                        continuation = "-";
                    }
                    var line = self.Substring(start, end - start) + continuation;
                    yield return line;
                    start = end;
                    if (char.IsWhiteSpace(c))
                        ++start;
                    width = GetNextWidth(ewidths, width, ref hw);
                } while (start < self.Length);
            }
        }

        private static int GetLineEnd(int start, int length, string description)
        {
            var end = Math.Min(start + length, description.Length);
            var sep = -1;
            for (var i = start; i < end; ++i)
            {
                if (description[i] == '\n')
                    return i + 1;
                if (IsEolChar(description[i]))
                    sep = i + 1;
            }
            if (sep == -1 || end == description.Length)
                return end;
            return sep;
        }

        private static int GetNextWidth(IEnumerator<int> ewidths, int curWidth, ref bool? eValid)
        {
            if (eValid.HasValue && !eValid.Value)
                return curWidth;

            curWidth = (eValid = ewidths.MoveNext()).Value ? ewidths.Current : curWidth;
            // '.' is any character, - is for a continuation
            const string minWidth = ".-";
            if (curWidth < minWidth.Length)
                throw new ArgumentOutOfRangeException(nameof(curWidth),
                    $"Element must be >= {minWidth.Length}, was {curWidth}.");
            return curWidth;
            // no more elements, use the last element.
        }

        private static bool IsEolChar(char c)
        {
            return !char.IsLetterOrDigit(c);
        }
    }

    public sealed class OptionValueCollection : IList, IList<string>
    {
        internal OptionValueCollection(OptionContext c)
        {
            this.c = c;
        }

        private readonly OptionContext c;

        private readonly List<string> values = new List<string>();

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return values.GetEnumerator();
        }

        #endregion

        #region IEnumerable<T>

        public IEnumerator<string> GetEnumerator()
        {
            return values.GetEnumerator();
        }

        #endregion

        public string[] ToArray()
        {
            return values.ToArray();
        }

        public List<string> ToList()
        {
            return new List<string>(values);
        }

        public override string ToString()
        {
            return string.Join(", ", values.ToArray());
        }

        #region ICollection

        void ICollection.CopyTo(Array array, int index)
        {
            (values as ICollection).CopyTo(array, index);
        }

        bool ICollection.IsSynchronized => (values as ICollection).IsSynchronized;
        object ICollection.SyncRoot => (values as ICollection).SyncRoot;

        #endregion

        #region ICollection<T>

        public void Add(string item)
        {
            values.Add(item);
        }

        public void Clear()
        {
            values.Clear();
        }

        public bool Contains(string item)
        {
            return values.Contains(item);
        }

        public void CopyTo(string[] array, int arrayIndex)
        {
            values.CopyTo(array, arrayIndex);
        }

        public bool Remove(string item)
        {
            return values.Remove(item);
        }

        public int Count => values.Count;
        public bool IsReadOnly => false;

        #endregion

        #region IList

        int IList.Add(object value)
        {
            return (values as IList).Add(value);
        }

        bool IList.Contains(object value)
        {
            return (values as IList).Contains(value);
        }

        int IList.IndexOf(object value)
        {
            return (values as IList).IndexOf(value);
        }

        void IList.Insert(int index, object value)
        {
            (values as IList).Insert(index, value);
        }

        void IList.Remove(object value)
        {
            (values as IList).Remove(value);
        }

        void IList.RemoveAt(int index)
        {
            (values as IList).RemoveAt(index);
        }

        bool IList.IsFixedSize => false;

        object IList.this[int index]
        {
            get { return this[index]; }
            set { (values as IList)[index] = value; }
        }

        #endregion

        #region IList<T>

        public int IndexOf(string item)
        {
            return values.IndexOf(item);
        }

        public void Insert(int index, string item)
        {
            values.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            values.RemoveAt(index);
        }

        private void AssertValid(int index)
        {
            if (c.Option == null)
                throw new InvalidOperationException("OptionContext.Option is null.");
            if (index >= c.Option.MaxValueCount)
                throw new ArgumentOutOfRangeException(nameof(index));
            if (c.Option.OptionValueType == OptionValueType.Required &&
                index >= values.Count)
                throw new OptionException(string.Format(
                    c.OptionSet.MessageLocalizer("Missing required value for option '{0}'."), c.OptionName),
                    c.OptionName);
        }

        public string this[int index]
        {
            get
            {
                AssertValid(index);
                return index >= values.Count ? null : values[index];
            }
            set { values[index] = value; }
        }

        #endregion
    }

    public class OptionContext
    {
        public OptionContext(OptionSet set)
        {
            OptionSet = set;
            OptionValues = new OptionValueCollection(this);
        }

        public Option Option { get; set; }

        public string OptionName { get; set; }

        public int OptionIndex { get; set; }

        public OptionSet OptionSet { get; }

        public OptionValueCollection OptionValues { get; }
    }

    public enum OptionValueType
    {
        None,
        Optional,
        Required
    }

    public abstract class Option
    {
        protected Option(string prototype, string description)
            : this(prototype, description, 1, false)
        {
        }

        protected Option(string prototype, string description, int maxValueCount)
            : this(prototype, description, maxValueCount, false)
        {
        }

        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        protected Option(string prototype, string description, int maxValueCount, bool hidden)
        {
            if (prototype == null)
                throw new ArgumentNullException(nameof(prototype));
            if (prototype.Length == 0)
                throw new ArgumentException("Cannot be the empty string.", nameof(prototype));
            if (maxValueCount < 0)
                throw new ArgumentOutOfRangeException(nameof(maxValueCount));

            Prototype = prototype;
            Description = description;
            MaxValueCount = maxValueCount;
            Names = this is OptionSet.Category
                // append GetHashCode() so that "duplicate" categories have distinct
                // names, e.g. adding multiple "" categories should be valid.
                // ReSharper disable once VirtualMemberCallInConstructor
                ? new[] {prototype + GetHashCode()}
                : prototype.Split('|');

            if (this is OptionSet.Category)
                return;

            OptionValueType = ParsePrototype();
            Hidden = hidden;

            if (MaxValueCount == 0 && OptionValueType != OptionValueType.None)
                throw new ArgumentException(
                    "Cannot provide maxValueCount of 0 for OptionValueType.Required or " +
                    "OptionValueType.Optional.",
                    nameof(maxValueCount));

            if (OptionValueType == OptionValueType.None && maxValueCount > 1)
                throw new ArgumentException(
                    $"Cannot provide maxValueCount of {maxValueCount} for OptionValueType.None.",
                    nameof(maxValueCount));

            if (Array.IndexOf(Names, "<>") >= 0 &&
                ((Names.Length == 1 && OptionValueType != OptionValueType.None) ||
                 (Names.Length > 1 && MaxValueCount > 1)))
                throw new ArgumentException(
                    "The default option handler '<>' cannot require values.",
                    nameof(prototype));
        }

        private static readonly char[] NameTerminator = {'=', ':'};

        public string Prototype { get; }

        public string Description { get; }

        public OptionValueType OptionValueType { get; }

        public int MaxValueCount { get; }

        public bool Hidden { get; }

        internal string[] Names { get; }

        internal string[] ValueSeparators { get; private set; }

        public string[] GetNames()
        {
            return (string[]) Names.Clone();
        }

        public string[] GetValueSeparators()
        {
            if (ValueSeparators == null)
                return new string[0];
            return (string[]) ValueSeparators.Clone();
        }

        public void Invoke(OptionContext c)
        {
            OnParseComplete(c);
            c.OptionName = null;
            c.Option = null;
            c.OptionValues.Clear();
        }

        public override string ToString()
        {
            return Prototype;
        }

        protected abstract void OnParseComplete(OptionContext c);

        protected static T Parse<T>(string value, OptionContext c)
        {
            var tt = typeof(T);
            var ti = tt;

            var nullable =
                ti.IsValueType &&
                ti.IsGenericType &&
                !ti.IsGenericTypeDefinition &&
                ti.GetGenericTypeDefinition() == typeof(Nullable<>);

            var targetType = nullable ? tt.GetGenericArguments()[0] : tt;
            var t = default(T);
            try
            {
                if (value != null)
                {
                    var conv = TypeDescriptor.GetConverter(targetType);
                    t = (T) conv.ConvertFromString(value);
                }
            }
            catch (Exception e)
            {
                throw new OptionException(
                    string.Format(
                        c.OptionSet.MessageLocalizer("Could not convert string `{0}' to type {1} for option `{2}'."),
                        value, targetType.Name, c.OptionName),
                    c.OptionName, e);
            }
            return t;
        }

        private static void AddSeparators(string name, int end, ICollection<string> seps)
        {
            var start = -1;
            for (var i = end + 1; i < name.Length; ++i)
            {
                switch (name[i])
                {
                    case '{':
                        if (start != -1)
                            throw new ArgumentException($"Ill-formed name/value separator found in \"{name}\".");
                        start = i + 1;
                        break;
                    case '}':
                        if (start == -1)
                            throw new ArgumentException($"Ill-formed name/value separator found in \"{name}\".");
                        seps.Add(name.Substring(start, i - start));
                        start = -1;
                        break;
                    default:
                        if (start == -1)
                            seps.Add(name[i].ToString());
                        break;
                }
            }

            if (start != -1)
                throw new ArgumentException($"Ill-formed name/value separator found in \"{name}\".");
        }

        private OptionValueType ParsePrototype()
        {
            var type = '\0';
            var seps = new List<string>();

            for (var i = 0; i < Names.Length; ++i)
            {
                var name = Names[i];
                if (name.Length == 0)
                    throw new ArgumentException("Empty option names are not supported.");

                var end = name.IndexOfAny(NameTerminator);
                if (end == -1)
                    continue;
                Names[i] = name.Substring(0, end);
                if (type == '\0' || type == name[end])
                    type = name[end];
                else
                    throw new ArgumentException($"Conflicting option types: '{type}' vs. '{name[end]}'.");
                AddSeparators(name, end, seps);
            }

            if (type == '\0')
                return OptionValueType.None;

            if (MaxValueCount <= 1 && seps.Count != 0)
                throw new ArgumentException(
                    $"Cannot provide key/value separators for Options taking {MaxValueCount} value(s).");

            if (MaxValueCount > 1)
            {
                if (seps.Count == 0)
                    ValueSeparators = new[] {":", "="};
                else if (seps.Count == 1 && seps[0].Length == 0)
                    ValueSeparators = null;
                else
                    ValueSeparators = seps.ToArray();
            }

            return type == '=' ? OptionValueType.Required : OptionValueType.Optional;
        }
    }

    public abstract class ArgumentSource
    {
        public abstract string Description { get; }
        public abstract bool GetArguments(string value, out IEnumerable<string> replacement);

        public static IEnumerable<string> GetArguments(TextReader reader)
        {
            return GetArguments(reader, false);
        }

        public static IEnumerable<string> GetArgumentsFromFile(string file)
        {
            return GetArguments(File.OpenText(file), true);
        }

        public abstract string[] GetNames();

        // Cribbed from mcs/driver.cs:LoadArgs(string)
        private static IEnumerable<string> GetArguments(TextReader reader, bool close)
        {
            try
            {
                var arg = new StringBuilder();

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var t = line.Length;

                    for (var i = 0; i < t; i++)
                    {
                        var c = line[i];

                        if (c == '"' || c == '\'')
                        {
                            var end = c;

                            for (i++; i < t; i++)
                            {
                                c = line[i];

                                if (c == end)
                                    break;
                                arg.Append(c);
                            }
                        }
                        else if (c == ' ')
                        {
                            if (arg.Length > 0)
                            {
                                yield return arg.ToString();
                                arg.Length = 0;
                            }
                        }
                        else
                            arg.Append(c);
                    }
                    if (arg.Length > 0)
                    {
                        yield return arg.ToString();
                        arg.Length = 0;
                    }
                }
            }
            finally
            {
                if (close)
                    reader.Dispose();
            }
        }
    }

    public class ResponseFileSource : ArgumentSource
    {
        public override string Description => "Read response file for more options.";

        public override bool GetArguments(string value, out IEnumerable<string> replacement)
        {
            if (string.IsNullOrEmpty(value) || !value.StartsWith("@"))
            {
                replacement = null;
                return false;
            }
            replacement = GetArgumentsFromFile(value.Substring(1));
            return true;
        }

        public override string[] GetNames()
        {
            return new[] {"@file"};
        }
    }

    [Serializable]
    public class OptionException : Exception
    {
        public OptionException()
        {
        }

        public OptionException(string message, string optionName)
            : base(message)
        {
            OptionName = optionName;
        }

        public OptionException(string message, string optionName, Exception innerException)
            : base(message, innerException)
        {
            OptionName = optionName;
        }

        protected OptionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            OptionName = info.GetString("OptionName");
        }

        public string OptionName { get; }
#pragma warning disable 618 // SecurityPermissionAttribute is obsolete
        [SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
#pragma warning restore 618
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("OptionName", OptionName);
        }
    }

    public delegate void OptionAction<in TKey, in TValue>(TKey key, TValue value);

    public class OptionSet : KeyedCollection<string, Option>
    {
        public OptionSet()
            : this(f => f)
        {
        }

        public OptionSet(MessageLocalizerConverter localizer)
        {
            MessageLocalizer = localizer;
            ArgumentSources = new ReadOnlyCollection<ArgumentSource>(sources);
        }

        private const int OptionWidth = 29;
        private const int DescriptionFirstWidth = 80 - OptionWidth;
        private const int DescriptionRemWidth = 80 - OptionWidth - 2;

        private readonly List<ArgumentSource> sources = new List<ArgumentSource>();

        private readonly Regex valueOption = new Regex(
            @"^(?<flag>--|-|/)(?<name>[^:=]+)((?<sep>[:=])(?<value>.*))?$");

        public MessageLocalizerConverter MessageLocalizer { get; }

        public ReadOnlyCollection<ArgumentSource> ArgumentSources { get; }

        public OptionSet Add(string header)
        {
            if (header == null)
                throw new ArgumentNullException(nameof(header));
            Add(new Category(header));
            return this;
        }


        public new OptionSet Add(Option option)
        {
            base.Add(option);
            return this;
        }

        public OptionSet Add(string prototype, Action<string> action)
        {
            return Add(prototype, null, action);
        }

        public OptionSet Add(string prototype, string description, Action<string> action)
        {
            return Add(prototype, description, action, false);
        }

        public OptionSet Add(string prototype, string description, Action<string> action, bool hidden)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            Option p = new ActionOption(prototype, description, 1,
                delegate(OptionValueCollection v) { action(v[0]); }, hidden);
            base.Add(p);
            return this;
        }

        public OptionSet Add(string prototype, OptionAction<string, string> action)
        {
            return Add(prototype, null, action);
        }

        public OptionSet Add(string prototype, string description, OptionAction<string, string> action)
        {
            return Add(prototype, description, action, false);
        }

        public OptionSet Add(string prototype, string description, OptionAction<string, string> action, bool hidden)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            Option p = new ActionOption(prototype, description, 2,
                delegate(OptionValueCollection v) { action(v[0], v[1]); }, hidden);
            base.Add(p);
            return this;
        }

        public OptionSet Add<T>(string prototype, Action<T> action)
        {
            return Add(prototype, null, action);
        }

        public OptionSet Add<T>(string prototype, string description, Action<T> action)
        {
            return Add(new ActionOption<T>(prototype, description, action));
        }

        public OptionSet Add<TKey, TValue>(string prototype, OptionAction<TKey, TValue> action)
        {
            return Add(prototype, null, action);
        }

        public OptionSet Add<TKey, TValue>(string prototype, string description, OptionAction<TKey, TValue> action)
        {
            return Add(new ActionOption<TKey, TValue>(prototype, description, action));
        }

        public OptionSet Add(ArgumentSource source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            sources.Add(source);
            return this;
        }

        public List<string> Parse(IEnumerable<string> arguments)
        {
            if (arguments == null)
                throw new ArgumentNullException(nameof(arguments));

            var c = CreateOptionContext();
            c.OptionIndex = -1;
            var process = true;
            var unprocessed = new List<string>();
            var def = Contains("<>") ? this["<>"] : null;
            var ae = new ArgumentEnumerator(arguments);

            foreach (var argument in ae)
            {
                ++c.OptionIndex;
                if (argument == "--")
                {
                    process = false;
                    continue;
                }
                if (!process)
                {
                    Unprocessed(unprocessed, def, c, argument);
                    continue;
                }
                if (AddSource(ae, argument))
                    continue;
                if (!Parse(argument, c))
                    Unprocessed(unprocessed, def, c, argument);
            }

            c.Option?.Invoke(c);
            return unprocessed;
        }

        public void WriteOptionDescriptions(TextWriter o)
        {
            foreach (var p in this)
            {
                var written = 0;

                if (p.Hidden)
                    continue;

                var c = p as Category;
                if (c != null)
                {
                    WriteDescription(o, p.Description, "", 80, 80);
                    continue;
                }

                if (!WriteOptionPrototype(o, p, ref written))
                    continue;

                if (written < OptionWidth)
                    o.Write(new string(' ', OptionWidth - written));
                else
                {
                    o.WriteLine();
                    o.Write(new string(' ', OptionWidth));
                }

                WriteDescription(o, p.Description, new string(' ', OptionWidth + 2),
                    DescriptionFirstWidth, DescriptionRemWidth);
            }

            foreach (var s in sources)
            {
                var names = s.GetNames();
                if (names == null || names.Length == 0)
                    continue;

                var written = 0;

                Write(o, ref written, "  ");
                Write(o, ref written, names[0]);
                for (var i = 1; i < names.Length; ++i)
                {
                    Write(o, ref written, ", ");
                    Write(o, ref written, names[i]);
                }

                if (written < OptionWidth)
                    o.Write(new string(' ', OptionWidth - written));
                else
                {
                    o.WriteLine();
                    o.Write(new string(' ', OptionWidth));
                }

                WriteDescription(o, s.Description, new string(' ', OptionWidth + 2),
                    DescriptionFirstWidth, DescriptionRemWidth);
            }
        }

        protected virtual OptionContext CreateOptionContext()
        {
            return new OptionContext(this);
        }


        protected override string GetKeyForItem(Option item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            if (item.Names != null && item.Names.Length > 0)
                return item.Names[0];
            // This should never happen, as it's invalid for Option to be
            // constructed w/o any names.
            throw new InvalidOperationException("Option has no names!");
        }

        [Obsolete("Use KeyedCollection.this[string]")]
        protected Option GetOptionForName(string option)
        {
            if (option == null)
                throw new ArgumentNullException(nameof(option));
            try
            {
                return base[option];
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        protected bool GetOptionParts(string argument, out string flag, out string name, out string sep,
            out string value)
        {
            if (argument == null)
                throw new ArgumentNullException(nameof(argument));

            flag = name = sep = value = null;
            var m = valueOption.Match(argument);
            if (!m.Success)
            {
                return false;
            }
            flag = m.Groups["flag"].Value;
            name = m.Groups["name"].Value;
            if (m.Groups["sep"].Success && m.Groups["value"].Success)
            {
                sep = m.Groups["sep"].Value;
                value = m.Groups["value"].Value;
            }
            return true;
        }

        protected override void InsertItem(int index, Option item)
        {
            base.InsertItem(index, item);
            AddImpl(item);
        }

        protected virtual bool Parse(string argument, OptionContext c)
        {
            if (c.Option != null)
            {
                ParseValue(argument, c);
                return true;
            }

            string f, n, s, v;
            if (!GetOptionParts(argument, out f, out n, out s, out v))
                return false;

            if (Contains(n))
            {
                var p = this[n];
                c.OptionName = f + n;
                c.Option = p;
                switch (p.OptionValueType)
                {
                    case OptionValueType.None:
                        c.OptionValues.Add(n);
                        c.Option.Invoke(c);
                        break;
                    case OptionValueType.Optional:
                    case OptionValueType.Required:
                        ParseValue(v, c);
                        break;
                }
                return true;
            }

            // no match; is it a bool option?
            return ParseBool(argument, n, c) || ParseBundledValue(f, $"{n}{s}{v}", c);
        }

        protected override void RemoveItem(int index)
        {
            var p = Items[index];
            base.RemoveItem(index);
            // KeyedCollection.RemoveItem() handles the 0th item
            for (var i = 1; i < p.Names.Length; ++i)
            {
                Dictionary.Remove(p.Names[i]);
            }
        }

        protected override void SetItem(int index, Option item)
        {
            base.SetItem(index, item);
            AddImpl(item);
        }

        private void AddImpl(Option option)
        {
            if (option == null)
                throw new ArgumentNullException(nameof(option));
            var added = new List<string>(option.Names.Length);
            try
            {
                // KeyedCollection.InsertItem/SetItem handle the 0th name.
                for (var i = 1; i < option.Names.Length; ++i)
                {
                    Dictionary.Add(option.Names[i], option);
                    added.Add(option.Names[i]);
                }
            }
            catch (Exception)
            {
                foreach (var name in added)
                    Dictionary.Remove(name);
                throw;
            }
        }

        private bool AddSource(ArgumentEnumerator ae, string argument)
        {
            foreach (var source in sources)
            {
                IEnumerable<string> replacement;
                if (!source.GetArguments(argument, out replacement))
                    continue;
                ae.Add(replacement);
                return true;
            }
            return false;
        }

        private static string GetArgumentName(int index, int maxIndex, string description)
        {
            if (description == null)
                return maxIndex == 1 ? "VALUE" : "VALUE" + (index + 1);

            var nameStart = maxIndex == 1 ? new[] {"{0:", "{"} : new[] {"{" + index + ":"};

            foreach (var t in nameStart)
            {
                int start, j = 0;
                do
                {
                    start = description.IndexOf(t, j, StringComparison.Ordinal);
                } while (start >= 0 && j != 0 && description[j++ - 1] == '{');
                if (start == -1)
                    continue;
                var end = description.IndexOf("}", start, StringComparison.Ordinal);
                if (end == -1)
                    continue;
                return description.Substring(start + t.Length, end - start - t.Length);
            }

            return maxIndex == 1 ? "VALUE" : "VALUE" + (index + 1);
        }

        private static string GetDescription(string description)
        {
            if (description == null)
                return string.Empty;
            var sb = new StringBuilder(description.Length);
            var start = -1;
            for (var i = 0; i < description.Length; ++i)
            {
                switch (description[i])
                {
                    case '{':
                        if (i == start)
                        {
                            sb.Append('{');
                            start = -1;
                        }
                        else if (start < 0)
                            start = i + 1;
                        break;
                    case '}':
                        if (start < 0)
                        {
                            if (i + 1 == description.Length || description[i + 1] != '}')
                                throw new InvalidOperationException("Invalid option description: " + description);
                            ++i;
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append(description.Substring(start, i - start));
                            start = -1;
                        }
                        break;
                    case ':':
                        if (start < 0)
                            goto default;
                        start = i + 1;
                        break;
                    default:
                        if (start < 0)
                            sb.Append(description[i]);
                        break;
                }
            }
            return sb.ToString();
        }

        private static IEnumerable<string> GetLines(string description, int firstWidth, int remWidth)
        {
            return StringCoda.WrappedLines(description, firstWidth, remWidth);
        }

        private static int GetNextOptionIndex(string[] names, int i)
        {
            while (i < names.Length && names[i] == "<>")
            {
                ++i;
            }
            return i;
        }

        private static void Invoke(OptionContext c, string name, string value, Option option)
        {
            c.OptionName = name;
            c.Option = option;
            c.OptionValues.Add(value);
            option.Invoke(c);
        }

        private bool ParseBool(string option, string n, OptionContext c)
        {
            string rn;
            if (n.Length >= 1 && (n[n.Length - 1] == '+' || n[n.Length - 1] == '-') &&
                Contains(rn = n.Substring(0, n.Length - 1)))
            {
                var p = this[rn];
                var v = n[n.Length - 1] == '+' ? option : null;
                c.OptionName = option;
                c.Option = p;
                c.OptionValues.Add(v);
                p.Invoke(c);
                return true;
            }
            return false;
        }

        private bool ParseBundledValue(string f, string n, OptionContext c)
        {
            if (f != "-")
                return false;
            for (var i = 0; i < n.Length; ++i)
            {
                var opt = f + n[i];
                var rn = n[i].ToString();
                if (!Contains(rn))
                {
                    if (i == 0)
                        return false;
                    throw new OptionException(string.Format(MessageLocalizer(
                        "Cannot use unregistered option '{0}' in bundle '{1}'."), rn, f + n), null);
                }
                var p = this[rn];
                switch (p.OptionValueType)
                {
                    case OptionValueType.None:
                        Invoke(c, opt, n, p);
                        break;
                    case OptionValueType.Optional:
                    case OptionValueType.Required:
                    {
                        var v = n.Substring(i + 1);
                        c.Option = p;
                        c.OptionName = opt;
                        ParseValue(v.Length != 0 ? v : null, c);
                        return true;
                    }
                    default:
                        throw new InvalidOperationException("Unknown OptionValueType: " + p.OptionValueType);
                }
            }
            return true;
        }

        private void ParseValue(string option, OptionContext c)
        {
            if (option != null)
                foreach (var o in c.Option.ValueSeparators != null
                    ? option.Split(c.Option.ValueSeparators, c.Option.MaxValueCount - c.OptionValues.Count,
                        StringSplitOptions.None)
                    : new[] {option})
                {
                    c.OptionValues.Add(o);
                }
            if (c.OptionValues.Count == c.Option.MaxValueCount ||
                c.Option.OptionValueType == OptionValueType.Optional)
                c.Option.Invoke(c);
            else if (c.OptionValues.Count > c.Option.MaxValueCount)
            {
                throw new OptionException(MessageLocalizer(
                    $"Error: Found {c.OptionValues.Count} option values when expecting {c.Option.MaxValueCount}."),
                    c.OptionName);
            }
        }

        private static void Unprocessed(ICollection<string> extra, Option def, OptionContext c, string argument)
        {
            if (def == null)
            {
                extra.Add(argument);
                return;
            }
            c.OptionValues.Add(argument);
            c.Option = def;
            c.Option.Invoke(c);
        }

        private static void Write(TextWriter o, ref int n, string s)
        {
            n += s.Length;
            o.Write(s);
        }

        private void WriteDescription(TextWriter o, string value, string prefix, int firstWidth, int remWidth)
        {
            var indent = false;
            foreach (var line in GetLines(MessageLocalizer(GetDescription(value)), firstWidth, remWidth))
            {
                if (indent)
                    o.Write(prefix);
                o.WriteLine(line);
                indent = true;
            }
        }

        private bool WriteOptionPrototype(TextWriter o, Option p, ref int written)
        {
            var names = p.Names;

            var i = GetNextOptionIndex(names, 0);
            if (i == names.Length)
                return false;

            if (names[i].Length == 1)
            {
                Write(o, ref written, "  -");
                Write(o, ref written, names[0]);
            }
            else
            {
                Write(o, ref written, "      --");
                Write(o, ref written, names[0]);
            }

            for (i = GetNextOptionIndex(names, i + 1);
                i < names.Length;
                i = GetNextOptionIndex(names, i + 1))
            {
                Write(o, ref written, ", ");
                Write(o, ref written, names[i].Length == 1 ? "-" : "--");
                Write(o, ref written, names[i]);
            }

            if (p.OptionValueType == OptionValueType.Optional ||
                p.OptionValueType == OptionValueType.Required)
            {
                if (p.OptionValueType == OptionValueType.Optional)
                {
                    Write(o, ref written, MessageLocalizer("["));
                }
                Write(o, ref written, MessageLocalizer("=" + GetArgumentName(0, p.MaxValueCount, p.Description)));
                var sep = p.ValueSeparators != null && p.ValueSeparators.Length > 0
                    ? p.ValueSeparators[0]
                    : " ";
                for (var c = 1; c < p.MaxValueCount; ++c)
                {
                    Write(o, ref written, MessageLocalizer(sep + GetArgumentName(c, p.MaxValueCount, p.Description)));
                }
                if (p.OptionValueType == OptionValueType.Optional)
                {
                    Write(o, ref written, MessageLocalizer("]"));
                }
            }
            return true;
        }

        internal sealed class Category : Option
        {
            // Prototype starts with '=' because this is an invalid prototype
            // (see Option.ParsePrototype(), and thus it'll prevent Category
            // instances from being accidentally used as normal options.
            public Category(string description)
                : base("=:Category:= " + description, description)
            {
            }

            protected override void OnParseComplete(OptionContext c)
            {
                throw new NotSupportedException("Category.OnParseComplete should not be invoked.");
            }
        }

        private sealed class ActionOption : Option
        {
            public ActionOption(string prototype, string description, int count, Action<OptionValueCollection> action,
                bool hidden)
                : base(prototype, description, count, hidden)
            {
                if (action == null)
                    throw new ArgumentNullException(nameof(action));
                this.action = action;
            }

            private readonly Action<OptionValueCollection> action;

            protected override void OnParseComplete(OptionContext c)
            {
                action(c.OptionValues);
            }
        }

        private sealed class ActionOption<T> : Option
        {
            public ActionOption(string prototype, string description, Action<T> action)
                : base(prototype, description, 1)
            {
                if (action == null)
                    throw new ArgumentNullException(nameof(action));
                this.action = action;
            }

            private readonly Action<T> action;

            protected override void OnParseComplete(OptionContext c)
            {
                action(Parse<T>(c.OptionValues[0], c));
            }
        }

        private sealed class ActionOption<TKey, TValue> : Option
        {
            public ActionOption(string prototype, string description, OptionAction<TKey, TValue> action)
                : base(prototype, description, 2)
            {
                if (action == null)
                    throw new ArgumentNullException(nameof(action));
                this.action = action;
            }

            private readonly OptionAction<TKey, TValue> action;

            protected override void OnParseComplete(OptionContext c)
            {
                action(
                    Parse<TKey>(c.OptionValues[0], c),
                    Parse<TValue>(c.OptionValues[1], c));
            }
        }

        private class ArgumentEnumerator : IEnumerable<string>
        {
            public ArgumentEnumerator(IEnumerable<string> arguments)
            {
                sources.Add(arguments.GetEnumerator());
            }

            private readonly List<IEnumerator<string>> sources = new List<IEnumerator<string>>();

            public IEnumerator<string> GetEnumerator()
            {
                do
                {
                    var c = sources[sources.Count - 1];
                    if (c.MoveNext())
                        yield return c.Current;
                    else
                    {
                        c.Dispose();
                        sources.RemoveAt(sources.Count - 1);
                    }
                } while (sources.Count > 0);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public void Add(IEnumerable<string> arguments)
            {
                sources.Add(arguments.GetEnumerator());
            }
        }
    }
}