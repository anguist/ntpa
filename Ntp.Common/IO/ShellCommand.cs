﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using Ntp.Common.Log;

namespace Ntp.Common.IO
{
    public sealed class ShellCommand
    {
        public ShellCommand(string command, string arguments, LogBase log, string message)
        {
            this.command = command;
            this.arguments = arguments;
            this.log = log;
            ErrorMessage = message;
        }

        private readonly string arguments;
        private readonly string command;

        private readonly LogBase log;
        public static string WorkingDirectory { get; set; }

        public string ErrorMessage { get; set; }

        public StreamReader Execute()
        {
            var proc = new global::System.Diagnostics.Process
            {
                EnableRaisingEvents = false,
                StartInfo =
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    WorkingDirectory = WorkingDirectory,
                    FileName = command,
                    Arguments = $" {arguments}"
                }
            };

            string error;
            log.ShellCommandExecuting(command, arguments);

            try
            {
                proc.Start();
                error = proc.StandardError.ReadToEnd().Replace(Environment.NewLine, " ");
                proc.WaitForExit();
            }
            catch (Exception e)
            {
                log.WriteLine(e.Message, Severity.Warn);
                throw;
            }

            if (error == string.Empty)
                return proc.StandardOutput;

            log.ShellCommandError(ErrorMessage, error);
            return null;
        }
    }
}