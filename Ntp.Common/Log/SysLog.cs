﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Mono.Unix;
using Mono.Unix.Native;

namespace Ntp.Common.Log
{
    public sealed class SysLog : LogBase
    {
        public SysLog(string name, Severity threshold)
            : base(threshold, true)
        {
            this.name = name;
        }

        private const SyslogFacility Facility = SyslogFacility.LOG_DAEMON;
        private const SyslogOptions Options = SyslogOptions.LOG_PID;
        private static bool initialized;

        private readonly string name;

        public override void Close()
        {
            if (initialized)
                return;

            Syscall.closelog();
            initialized = false;
        }

        public override void Initialize()
        {
            if (initialized)
                return;

            string ident = name;
            Syscall.openlog(UnixMarshal.StringToHeap(ident), Options, Facility);
            initialized = true;
        }

        public override void Resume()
        {
            // SysLog cannot resume
        }

        public override void Suspend()
        {
            // SysLog cannot suspend
        }

        public override void WriteLine(Exception exception)
        {
            WriteLine(exception, Severity.Error);
        }

        public override void WriteLine(Exception exception, Severity severity)
        {
            WriteLine(exception.Message, Severity.Error);
        }

        public override void WriteLine(string text, Severity severity)
        {
            if (initialized)
                Initialize();

            if (severity < Threshold)
                return;

            Syscall.syslog(Facility, GetSyslogLevel(severity), text);
        }

        private static SyslogLevel GetSyslogLevel(Severity severity)
        {
            switch (severity)
            {
                case Severity.Error:
                    return SyslogLevel.LOG_ERR;
                case Severity.Warn:
                    return SyslogLevel.LOG_WARNING;
                case Severity.Notice:
                    return SyslogLevel.LOG_NOTICE;
                case Severity.Info:
                    return SyslogLevel.LOG_INFO;
                case Severity.Debug:
                case Severity.Trace:
                    return SyslogLevel.LOG_DEBUG;
                default:
                    return SyslogLevel.LOG_DEBUG;
            }
        }
    }
}