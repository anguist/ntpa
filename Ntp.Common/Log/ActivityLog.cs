// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections;
using System.Collections.Generic;

namespace Ntp.Common.Log
{
    public sealed class ActivityLog : LogBase, IEnumerable<string>
    {
        internal ActivityLog()
            : base(Severity.Debug)
        {
            activity = new List<string>(2001);
        }

        private readonly List<string> activity;
        private readonly object locker = new object();

        public IEnumerator<string> GetEnumerator()
        {
            return activity.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return activity.GetEnumerator();
        }

        public override void Close()
        {
            activity.Clear();
        }

        public override void Initialize()
        {
            activity.Clear();
        }

        public override void Resume()
        {
        }

        public override void Suspend()
        {
        }

        public override void WriteLine(string text, Severity severity)
        {
            string severityText;

            if (severity == Severity.Trace)
                return;

            switch (severity)
            {
                case Severity.Error:
                    severityText = "E";
                    break;
                case Severity.Warn:
                    severityText = "W";
                    break;
                case Severity.Notice:
                    severityText = "N";
                    break;
                case Severity.Info:
                    severityText = "I";
                    break;
                case Severity.Debug:
                    severityText = "D";
                    break;
                case Severity.Trace:
                    severityText = "T";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(severity));
            }

            var now = DateTime.Now;
            string entry = string.Concat(
                now.ToString("HH:mm:ss"),
                " ", severityText, "  ",
                text);

            lock (locker)
            {
                if (activity.Count == 1000)
                    activity.RemoveAt(0);

                activity.Add(entry);
            }
        }

        public override void WriteLine(Exception exception)
        {
            WriteLine(exception.Message, Severity.Error);
            WriteLine(exception.StackTrace, Severity.Debug);
        }

        public override void WriteLine(Exception exception, Severity severity)
        {
            if (severity == Severity.Trace)
                return;

            WriteLine(exception.Message, severity);
            WriteLine(exception.StackTrace, severity);
        }
    }
}