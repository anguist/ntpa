﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Diagnostics;
using System.Security;

namespace Ntp.Common.Log
{
    public sealed class EventLog : LogBase
    {
        public EventLog(string name, Severity threshold)
            : base(threshold, true)
        {
            this.name = name;
        }

        private const string Log = "Application";
        private static bool initialized;
        private readonly string name;

        public override void Close()
        {
        }

        public override void Initialize()
        {
            if (initialized)
                return;

            CreateEventLog();
            initialized = true;
        }

        public override void Resume()
        {
            // EventLog does not support resume
        }

        public override void Suspend()
        {
            // EventLog does not support suspend
        }

        public override void WriteLine(string text, Severity severity)
        {
            if (!initialized)
                Initialize();

            if (severity < Threshold)
                return;

            WriteEventLogEntry(text, GetEventLogEntryType(severity), 0);
        }

        public override void WriteLine(Exception exception, Severity severity)
        {
            if (!initialized)
                Initialize();

            if (severity < Threshold)
                return;

            if (!initialized)
                Initialize();

            if (severity < Threshold)
                return;

            WriteEventLogEntry(exception.ToString(), GetEventLogEntryType(severity), 0);
        }

        public override void WriteLine(Exception exception)
        {
            WriteEventLogEntry(exception.ToString(), EventLogEntryType.Error, 1);
        }

        #region EventLog Implementation

        private void CreateEventLog()
        {
            try
            {
                if (!global::System.Diagnostics.EventLog.SourceExists(name))
                {
                    global::System.Diagnostics.EventLog.CreateEventSource(name, Log);
                }
            }
            catch (SecurityException)
            {
            }
        }

        private static EventLogEntryType GetEventLogEntryType(Severity severity)
        {
            switch (severity)
            {
                case Severity.Trace:
                case Severity.Debug:
                case Severity.Info:
                case Severity.Notice:
                    return EventLogEntryType.Information;
                case Severity.Warn:
                    return EventLogEntryType.Warning;
                case Severity.Error:
                    return EventLogEntryType.Error;
                default:
                    return EventLogEntryType.Error;
            }
        }

        private void WriteEventLogEntry(string text, EventLogEntryType type, int code)
        {
            try
            {
                global::System.Diagnostics.EventLog.WriteEntry(name, text, type, code);
            }
            catch (SecurityException)
            {
            }
        }

        #endregion
    }
}