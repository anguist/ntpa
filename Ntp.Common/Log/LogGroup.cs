// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ntp.Common.Log
{
    public sealed class LogGroup : LogBase, IEnumerable<LogBase>
    {
        internal LogGroup()
            : base(Severity.Trace)
        {
            logs = new List<LogBase>();
        }

        private readonly List<LogBase> logs;

        private LogBase SysLog
        {
            get
            {
                return logs.
                    Where(l => l.IsSysLog).
                    OrderBy(l => l.Threshold).
                    FirstOrDefault();
            }
        }

        private LogBase ConsoleLog
        {
            get
            {
                return logs.
                    Where(l => l is ConsoleLog).
                    OrderBy(l => l.Threshold).
                    FirstOrDefault();
            }
        }

        public IEnumerator<LogBase> GetEnumerator()
        {
            return logs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(LogBase log)
        {
            var logGroup = log as LogGroup;
            if (logGroup != null)
            {
                logs.AddRange(logGroup);
            }
            else
            {
                logs.Add(log);
            }
        }

        public override void Close()
        {
            foreach (var log in logs)
                log.Close();
        }

        public override void Initialize()
        {
            foreach (var log in logs)
                log.Initialize();
        }

        public override void Resume()
        {
            foreach (var log in logs)
                log.Resume();
        }

        public override void Suspend()
        {
            foreach (var log in logs)
                log.Suspend();
        }

        public override void WriteLine(string text, Severity severity)
        {
            SysLog?.WriteLine(text, severity);
            ConsoleLog?.WriteLine(text, severity);

            foreach (var log in logs.Where(l => l is FileLog))
                log.WriteLine(text, severity);
        }

        public override void WriteLine(Exception exception)
        {
            SysLog?.WriteLine(exception);
            ConsoleLog?.WriteLine(exception);

            foreach (var log in logs.Where(l => l is FileLog))
                log.WriteLine(exception);
        }

        public override void WriteLine(Exception exception, Severity severity)
        {
            SysLog?.WriteLine(exception, severity);
            ConsoleLog?.WriteLine(exception, severity);

            foreach (var log in logs.Where(l => l is FileLog))
                log.WriteLine(exception, severity);
        }
    }
}