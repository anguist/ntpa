// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using Ntp.Common.System;

namespace Ntp.Common.Log
{
    public static class LogFactory
    {
        public static string ApplicationName;
        private static readonly List<LogBase> Logs = new List<LogBase>();

        public static void Cleanup()
        {
            Close();
            Logs.Clear();
        }

        public static void Close()
        {
            foreach (var log in Logs)
                log.Close();
        }

        public static ActivityLog CreateActivityLog()
        {
            return new ActivityLog();
        }

        public static LogGroup CreateErrorLog(string name)
        {
            var log = new LogGroup
            {
                new ConsoleLog(Severity.Error)
                {
                    ShowSeverity = false,
                    ShowTimeStamp = false
                }
            };
            return log;
        }

        public static LogGroup CreateGroupLog()
        {
            return new LogGroup();
        }

        public static LogBase CreateLog(ILogConfiguration config)
        {
            var log = CreateLogInternal(config);
            Logs.Add(log);
            return log;
        }

        public static LogBase CreateLog(IEnumerable<ILogConfiguration> configs)
        {
            var group = new LogGroup();

            foreach (var config in configs)
            {
                var log = CreateLogInternal(config);
                group.Add(log);
                Logs.Add(log);
            }

            return group;
        }

        public static LogBase CreateSysLog(string name)
        {
            var log = ProcessInfo.CreateSystemLog(name);
            Logs.Add(log);
            return log;
        }

        public static void Resume()
        {
            foreach (var log in Logs)
                log.Resume();
        }

        public static void Suspend()
        {
            foreach (var log in Logs)
                log.Suspend();
        }

        private static LogBase CreateLogInternal(ILogConfiguration config)
        {
            switch (config.LogType)
            {
                case LogType.Unknown:
                case LogType.Console:
                    return new ConsoleLog(config.Threshold, config.TimeFormat)
                    {
                        ShowSeverity = config.ShowSeverity,
                        ShowTimeStamp = config.ShowTimestamp
                    };
                case LogType.Syslog:
                    return new SysLog(ApplicationName, config.Threshold);
                case LogType.EventLog:
                    return new EventLog(ApplicationName, config.Threshold);
                case LogType.File:
                    return new FileLog(config.File, config.Threshold, config.TimeFormat)
                    {
                        ShowSeverity = config.ShowSeverity,
                        ShowTimeStamp = config.ShowTimestamp
                    };
                default:
                    throw new ArgumentOutOfRangeException(nameof(config));
            }
        }
    }
}