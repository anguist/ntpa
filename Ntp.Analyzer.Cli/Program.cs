// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Globalization;
using System.IO;
using System.Threading;
using Ntp.Analyzer.Process;
using Ntp.Common.IO;
using Ntp.Common.Log;
using Ntp.Common.System;

namespace Ntp.Analyzer.Cli
{
    public static class Program
    {
        private static bool usage;

        public static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            string name = "ntpa";
            string tempDir = Directory.GetCurrentDirectory();
            string configFile = null;
            string pidFile = null;

            var initlog = LogFactory.CreateErrorLog(name);
            initlog.Add(LogFactory.CreateSysLog(name));

            var p = new OptionSet
            {
                {"h|?|help", v => { ShowUsage(); }},
                {"daemon=", v => { name = v; }},
                {"config=", v => { configFile = v; }},
                {"writepid=", v => { pidFile = v; }},
                {"temp=", v => { tempDir = v; }}
            };

            var rem = p.Parse(args).ToArray();

            if (usage)
            {
                return;
            }

            if (rem.Length > 0)
            {
                initlog.WriteLine("Unknown option: " + rem[0], Severity.Error);
                return;
            }

            if (configFile == null)
            {
                initlog.WriteLine("Please specify configuration file with option --config", Severity.Error);
                return;
            }

            ShellCommand.WorkingDirectory = tempDir;
            int pid = ProcessInfo.ProcessId;

            try
            {
                var main = new Main(configFile, pid, pidFile, name, initlog);
                main.Run();
            }
            catch (Exception e)
            {
                initlog.WriteLine("Unexpected error: " + e.Message, Severity.Error);
                initlog.WriteLine(e.StackTrace, Severity.Error);
            }
        }

        private static void ShowUsage()
        {
            Console.WriteLine("NTP Analyzer v0.8.2");
            Console.WriteLine("Usage: ntpa --config file [--temp dir] [--writepid file] [--daemon name]");
            usage = true;
        }
    }
}