// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Net;
using Ntp.Analyzer.Monitor.Client;

namespace Ntp.Analyzer.Monitor.Cli
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length >= 1 && args[0] == "help")
            {
                ShowUsage();
                return;
            }

            if (args.Length != 3)
            {
                ShowUsage();
                return;
            }

            IPAddress address;

            if (!IPAddress.TryParse(args[0], out address))
            {
                Console.WriteLine("IP address is not valid.");
                ShowUsage();
                return;
            }

            int port;

            if (!int.TryParse(args[1], out port))
            {
                Console.WriteLine("Port number is not valid.");
                ShowUsage();
                return;
            }

            string command = args[2];

            var req = new TextRequest(address, port);
            string result = req.Send(command);

            Console.WriteLine(result);
        }

        private static void ShowUsage()
        {
            Console.WriteLine("NTP Analyzer command query tool.");
            Console.WriteLine("Usage: ntpac host port command");
        }
    }
}