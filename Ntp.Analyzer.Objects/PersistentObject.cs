// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Ntp.Analyzer.Objects
{
    public abstract class PersistentObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersistentObject" /> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <remarks>
        /// Constructor used when creating instances from data storage.
        /// </remarks>
        protected PersistentObject(int id)
        {
            Id = id;
            NewObject = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistentObject" /> class.
        /// </summary>
        protected PersistentObject()
        {
            Id = -1;
            NewObject = true;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <remarks>
        /// The identifier is equal to the primary key in database terminology.
        /// </remarks>
        /// <value>The identifier.</value>
        public int Id { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="PersistentObject" /> is a new object
        /// which does not yet exists in the peristing storage (database etc).
        /// </summary>
        /// <value>
        /// <c>true</c> if new object; otherwise, <c>false</c>.
        /// </value>
        public bool NewObject { get; protected set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() != GetType())
                return false;

            return NewObject
                // ReSharper disable once BaseObjectEqualsIsObjectEquals
                ? base.Equals(obj)
                : Id == ((PersistentObject) obj).Id;
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return NewObject
                // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
                ? base.GetHashCode()
                // ReSharper disable once NonReadonlyMemberInGetHashCode
                : (int) (Id*2654435761%2 ^ 32);
        }

        /// <summary>
        /// Sets the identifier after the object have been stored in persistent storage.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void SetId(int id)
        {
            Id = id;
            NewObject = false;
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents the current <see cref="PersistentObject" />.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents the current <see cref="PersistentObject" />.
        /// </returns>
        public override string ToString()
        {
            return $"[PersistentObject: Id={Id}, NewObject={NewObject}]";
        }
    }
}