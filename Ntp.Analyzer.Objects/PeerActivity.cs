﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Globalization;

namespace Ntp.Analyzer.Objects
{
    public sealed class PeerActivity : PersistentObject
    {
        public PeerActivity(int id, Peer peer, Host host, DateTime lastActive, int zone, bool hide)
            : base(id)
        {
            Peer = peer;
            Host = host;
            Hide = hide;

            var adjusted = new DateTime(
                lastActive.Year, lastActive.Month, lastActive.Day,
                lastActive.Hour, lastActive.Minute, lastActive.Second,
                DateTimeKind.Utc);

            LastActive = adjusted.AddMinutes(zone);
        }

        public PeerActivity(Peer peer, Host host, DateTime lastActive)
        {
            Peer = peer;
            Host = host;
            LastActive = lastActive;
        }

        public string Name => Id.ToString(CultureInfo.InvariantCulture);

        public Peer Peer { get; }

        public Host Host { get; }

        public DateTime LastActive { get; set; }

        public bool Hide { get; }

        public bool IsActive => !Hide;

        public int UtcOffset
        {
            get
            {
                switch (LastActive.Kind)
                {
                    case DateTimeKind.Utc:
                    case DateTimeKind.Unspecified:
                        return 0;
                    case DateTimeKind.Local:
                        return Convert.ToInt32(TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).TotalMinutes);
                    default:
                        return 0;
                }
            }
        }
    }
}