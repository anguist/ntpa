// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace Ntp.Analyzer.Objects
{
    public abstract class Reading : PersistentObject
    {
        protected Reading(
            int id,
            DateTime time,
            int zone,
            Host host)
            : base(id)
        {
            var adjusted = new DateTime(
                time.Year, time.Month, time.Day,
                time.Hour, time.Minute, time.Second,
                time.Millisecond,
                DateTimeKind.Utc);
            Time = adjusted.AddMinutes(zone);
            Host = host;
        }

        protected Reading(
            Host host,
            ReadingBulk bulk
            )
        {
            Time = bulk?.Time ?? DateTime.UtcNow;
            Host = host;
        }

        public Host Host { get; }

        public DateTime Time { get; }

        public DateTime UtcTime
        {
            get
            {
                switch (Time.Kind)
                {
                    case DateTimeKind.Utc:
                    case DateTimeKind.Unspecified:
                        return Time;
                    case DateTimeKind.Local:
                        return Time.Subtract(TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow));
                    default:
                        return Time;
                }
            }
        }

        public DateTime LocalTime
        {
            get
            {
                switch (Time.Kind)
                {
                    case DateTimeKind.Utc:
                    case DateTimeKind.Unspecified:
                        return UtcTime.Add(TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow));
                    case DateTimeKind.Local:
                        return Time;
                    default:
                        return UtcTime.Add(TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow));
                }
            }
        }

        public int UtcOffset
        {
            get
            {
                switch (Time.Kind)
                {
                    case DateTimeKind.Utc:
                    case DateTimeKind.Unspecified:
                        return 0;
                    case DateTimeKind.Local:
                        return Convert.ToInt32(TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).TotalMinutes);
                    default:
                        return 0;
                }
            }
        }

        public DateTime RoundedUtcTime => new DateTime(
            UtcTime.Year, UtcTime.Month, UtcTime.Day,
            UtcTime.Hour, UtcTime.Minute, 0,
            DateTimeKind.Utc);

        public DateTime RoundedLocalTime => new DateTime(
            LocalTime.Year, LocalTime.Month, LocalTime.Day,
            LocalTime.Hour, LocalTime.Minute, 0,
            DateTimeKind.Local);
    }
}