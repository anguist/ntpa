// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Net;

namespace Ntp.Analyzer.Objects
{
    public sealed class CalgaryTimeServer : TimeServer
    {
        public CalgaryTimeServer(string name, IPAddress address, int orgId)
        {
            Name = name;
            Address = address;
            SetId(orgId);
        }

        public CalgaryTimeServer(
            int id, int stratum, string country,
            string name, IPAddress address, string v6Address,
            bool? useDns, bool? poolMember, string location,
            string displayLocation, string organization, string geo, string server,
            string serviceArea, string accessDetails, string accessPolicy,
            bool? notification, string autoKey, string symKey, string symUrl,
            string contact, string providerPage, string providerUrl, DateTime updated)
            : base(id)
        {
            Stratum = stratum;
            Country = country;
            Name = name;
            Address = address;
            V6Address = Scrub(v6Address);
            this.useDns = useDns;
            this.poolMember = poolMember;
            Location = location;
            DisplayLocation = displayLocation;
            Organization = Scrub(organization);
            Geo = Scrub(geo);
            Location = location;
            Server = server;
            ServiceArea = serviceArea;
            AccessDetails = accessDetails;
            AccessPolicy = Scrub(accessPolicy);
            this.notification = notification;
            AutoKey = Scrub(autoKey);
            SymKey = Scrub(symKey);
            SymUrl = Scrub(symUrl);
            Contact = contact;
            ProviderPage = providerPage;
            ProviderUrl = providerUrl;
            Updated = updated;
        }

        private readonly bool? notification;
        private readonly bool? poolMember;
        private readonly bool? useDns;

        public override int Stratum { get; }

        public override string Country { get; }

        public override string Name { get; }

        public override IPAddress Address { get; }

        public override string V6Address { get; }

        public override bool ShouldUseDns => useDns ?? false;

        public override bool IsPoolMember => poolMember ?? false;

        public override string Location { get; }

        public override string DisplayLocation { get; }

        public override string Organization { get; }

        public override string Geo { get; }

        public override string Server { get; }

        public override string ServiceArea { get; }

        public override string AccessPolicy { get; }

        public override string AccessDetails { get; }

        public override bool ShouldNotify => notification ?? false;

        public override string AutoKey { get; }

        public override string SymKey { get; }

        public override string SymUrl { get; }

        public override string Contact { get; }

        public override string ProviderPage { get; }

        public override string ProviderUrl { get; }

        public override DateTime Updated { get; }

        private static string Scrub(string input)
        {
            if (input == null || input.Trim() == string.Empty)
                return null;

            return input.Trim();
        }
    }
}