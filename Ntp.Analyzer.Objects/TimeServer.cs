﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Net;

namespace Ntp.Analyzer.Objects
{
    public abstract class TimeServer : PersistentObject
    {
        protected TimeServer()
        {
        }

        protected TimeServer(int id)
            : base(id)
        {
        }

        public abstract int Stratum { get; }

        public abstract string Country { get; }

        public abstract string Name { get; }

        public abstract IPAddress Address { get; }

        public abstract string V6Address { get; }

        public abstract bool ShouldUseDns { get; }

        public abstract bool IsPoolMember { get; }

        public abstract string Location { get; }

        public abstract string DisplayLocation { get; }

        public abstract string Organization { get; }

        public abstract string Geo { get; }

        public abstract string Server { get; }

        public abstract string ServiceArea { get; }

        public abstract string AccessPolicy { get; }

        public abstract string AccessDetails { get; }

        public abstract bool ShouldNotify { get; }

        public abstract string AutoKey { get; }

        public abstract string SymKey { get; }

        public abstract string SymUrl { get; }

        public abstract string Contact { get; }

        public abstract string ProviderPage { get; }

        public abstract string ProviderUrl { get; }

        public abstract DateTime Updated { get; }

        public bool IsOrgServer => Id < 5000;
    }
}