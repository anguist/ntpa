// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace Ntp.Analyzer.Objects
{
    public abstract class DispersionReading : Reading
    {
        protected DispersionReading(
            int id,
            DateTime time,
            int zone,
            Host host,
            Peer peer,
            double offset,
            double jitter
            )
            : base(id, time, zone, host)
        {
            Peer = peer;
            Offset = offset;
            Jitter = jitter;
        }

        protected DispersionReading(
            Host host,
            Peer peer,
            ReadingBulk bulk,
            double offset,
            double jitter
            )
            : base(host, bulk)
        {
            Peer = peer;
            Offset = offset;
            Jitter = jitter;
        }

        public Peer Peer { get; }

        public double Offset { get; }

        public double Jitter { get; }
    }
}