// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace Ntp.Analyzer.Objects
{
    public sealed class PeerReading : DispersionReading
    {
        public PeerReading(
            int id,
            DateTime time,
            int zone,
            Host host,
            Peer peer,
            int last,
            int reach,
            double delay,
            double offset,
            double jitter
            )
            : base(id, time, zone, host, peer, offset, jitter)
        {
            Reach = reach;
            LastPoll = last;
            Delay = delay;
        }

        public PeerReading(
            Host host,
            Peer peer,
            ReadingBulk bulk,
            AssociationEntry entry)
            : base(host, peer, bulk, entry.Offset, entry.Jitter)
        {
            Reach = entry.Reach;
            LastPoll = entry.LastPoll;
            Delay = entry.Delay;
        }

        public int LastPoll { get; }

        public int Reach { get; }

        public double Delay { get; }
    }
}