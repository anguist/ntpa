// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Ntp.Analyzer.Objects
{
    public sealed class IoStatsEntry
    {
        public IoStatsEntry(
            int timeSinceReset,
            int receiveBuffers,
            int freeReceiveBuffers,
            int usedReceiveBuffers,
            int lowWaterRefills,
            long droppedPackets,
            long ignoredPackets,
            long receivedPackets,
            long packetsSent,
            long packetsNotSent,
            int interruptsHandled,
            int receivedByInt)
        {
            TimeSinceReset = timeSinceReset;
            ReceiveBuffers = receiveBuffers;
            FreeReceiveBuffers = freeReceiveBuffers;
            UsedReceiveBuffers = usedReceiveBuffers;
            LowWaterRefills = lowWaterRefills;
            DroppedPackets = droppedPackets;
            IgnoredPackets = ignoredPackets;
            ReceivedPackets = receivedPackets;
            PacketsSent = packetsSent;
            PacketsNotSent = packetsNotSent;
            InterruptsHandled = interruptsHandled;
            ReceivedByInt = receivedByInt;
        }

        public int TimeSinceReset { get; }

        public int ReceiveBuffers { get; }

        public int FreeReceiveBuffers { get; }

        public int UsedReceiveBuffers { get; }

        public int LowWaterRefills { get; }

        public long DroppedPackets { get; }

        public long IgnoredPackets { get; }

        public long ReceivedPackets { get; }

        public long PacketsSent { get; }

        public long PacketsNotSent { get; }

        public int InterruptsHandled { get; }

        public int ReceivedByInt { get; }
    }
}