﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    public sealed class DriftFileImporter
    {
        public DriftFileImporter(Host host, string file, ReadingBulk bulk, LogBase log)
        {
            this.host = host;
            this.file = file;
            this.bulk = bulk;
            this.log = log;
        }

        private readonly ReadingBulk bulk;
        private readonly string file;
        private readonly Host host;
        private readonly LogBase log;

        public DriftReading Reading { get; private set; }

        public void Execute()
        {
            StreamReader reader = null;
            string text;

            try
            {
                reader = new StreamReader(file);
                text = reader.ReadLine();
            }
            catch (Exception e)
            {
                log.DriftFileReadError(host.Name, file, e);
                return;
            }
            finally
            {
                reader?.Close();
            }

            double value;
            if (double.TryParse(text, out value))
            {
                Reading = new DriftReading(host, value, bulk);
                return;
            }

            log.DriftReadError(host.Name, file);
        }
    }
}