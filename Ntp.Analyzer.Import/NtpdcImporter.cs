// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    /// <summary>
    /// Ntpdc importer.
    /// </summary>
    public sealed class NtpdcImporter : Importer<HostReading>
    {
        internal NtpdcImporter(string address, Host host, ReadingBulk bulk, LogBase log)
            : base(log)
        {
            this.address = address;
            this.host = host;
            this.bulk = bulk;
        }

        private readonly string address;
        private readonly ReadingBulk bulk;
        private readonly Host host;
        private List<Peer> peers;

        protected override string Command => "ntpdc";

        protected override string Arguments => "-nc loopinfo -c sysinfo " + address;

        protected override string ErrorMessage => LogMessage.ImportHostError;

        protected override void Initialize()
        {
            base.Initialize();

            if (host == null)
                return;

            peers = DataFace.Instance.Peers.ToList();
        }

        protected override void ReadFromStream()
        {
            string peerIp = null;
            double offset = 0;
            double frequency = 0;
            double jitter = 0;
            double stability = 0;

            while (Reader.Peek() != -1)
            {
                string line = Reader.ReadLine();

                if (string.IsNullOrWhiteSpace(line) || line.Length < 24)
                    continue;

                string name = line.Substring(0, 22).Replace(":", string.Empty).TrimEnd();
                string value = line.Substring(22).TrimEnd();

                switch (name)
                {
                    case "system peer":
                        peerIp = value.Trim();
                        break;
                    case "offset":
                        offset = Convert.ToDouble(value.Replace("s", string.Empty).Trim());
                        break;
                    case "frequency":
                        frequency = Convert.ToDouble(value.Replace("ppm", string.Empty).Trim());
                        break;
                    case "jitter":
                        jitter = Convert.ToDouble(value.Replace("s", string.Empty).Trim());
                        break;
                    case "stability":
                        stability = Convert.ToDouble(value.Replace("ppm", string.Empty).Trim());
                        break;
                }
            }

            if (peerIp != null && peerIp.StartsWith("0.0.0.0"))
            {
                Log.NoSyncing(host.Name);
                return;
            }

            CreateEntry(peerIp, offset, jitter, frequency, stability);
        }

        private void CreateEntry(string peerIp, double offset, double jitter, double frequency, double stability)
        {
            IEnumerable<Peer> peerList = peers.Where(p => p.Ip == peerIp).ToList();

            Peer peer;

            switch (peerList.Count())
            {
                case 1:
                    peer = peerList.Single();
                    break;
                case 0:
                    Log.PeerNotFound(host.Name, peerIp);
                    return;
                default:
                    Log.MultiplePeersFound(host.Name, peerIp);
                    return;
            }

            Log.Syncing(host.Name, peer.Name);

            var reading = new HostReading(host, peer, bulk, offset, jitter, frequency, stability);
            Entries.Add(reading);
        }
    }
}