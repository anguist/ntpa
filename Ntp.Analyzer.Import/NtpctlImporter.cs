﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    public abstract class NtpctlImporter<T> : Importer<T>
    {
        protected NtpctlImporter(int hostId, LogBase log)
            : base(log)
        {
            this.hostId = hostId;
        }

        private readonly int hostId;

        protected override string Command => "ntpctl";

        protected override string Arguments => "-sp";

        protected AssociationEntry ParseLines(string peer, string stats)
        {
            double result;

            char state = stats[1];
            char weight = stats[4];
            int trustlevel = Convert.ToInt32(stats.Substring(6, 2).Trim());
            int stratus = Convert.ToInt32(stats.Substring(9, 2).Trim().Replace("-", "15"));
            int lastPoll = CalcPoll(stats.Substring(11, 6));
            int pollFrequency = CalcPoll(stats.Substring(17, 6));

            string value = stats.Substring(31, 10).Trim().Replace("ms", string.Empty);
            double offset = double.TryParse(value, out result)
                ? result
                : 0.0;

            value = stats.Substring(41, 10).Trim().Replace("ms", string.Empty);
            double delay = double.TryParse(value, out result)
                ? result
                : 0.0;

            value = stats.Substring(51, stats.Length - 51).Trim().Replace("ms", string.Empty);
            double jitter = double.TryParse(value, out result)
                ? result
                : 0.0;

            string remote = peer.Substring(0, peer.IndexOf(" ", StringComparison.Ordinal)).Trim();

            var entry = new AssociationEntry(
                hostId, state, remote, null, stratus, weight,
                lastPoll, pollFrequency, trustlevel,
                delay, offset, jitter);

            return entry;
        }

        private static int CalcPoll(string pollString)
        {
            char pollUnit = pollString.Substring(pollString.Length - 1, 1)[0];
            string pollValue = pollString.Substring(0, pollString.Length - 1).Trim();

            switch (pollUnit)
            {
                case 's':
                    return Convert.ToInt32(pollValue);
                case 'm':
                    return Convert.ToInt32(pollValue)*60;
                case 'h':
                    return Convert.ToInt32(pollValue)*60*60;
                case 'd':
                    return Convert.ToInt32(pollValue)*60*60*24;
                case '-':
                    return -1;
                default:
                    return Convert.ToInt32(pollString);
            }
        }
    }
}