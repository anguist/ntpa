﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    internal static class LogMessage
    {
        internal const string ImportHostError = "Error while reading host statistics.";
        internal const string ImportPeerError = "Error while reading peer statistics.";
        internal const string ImportIoError = "Error while reading I/O statistics.";
    }

    internal static class LogExtensions
    {
        internal static void DriftFileReadError(this LogBase log, string host, string file, Exception e)
        {
            log.WriteLine(
                $"Could not read drift file {file} on host {host}: {e.Message}",
                Severity.Warn);

            log.WriteLine(e, Severity.Trace);
        }

        internal static void DriftReadError(this LogBase log, string host, string file)
        {
            log.WriteLine(
                $"Could not read content of drift file {file} on host {host}.",
                Severity.Warn);
        }

        internal static void MultiplePeersFound(this LogBase log, string host, string peerIp)
        {
            log.WriteLine(
                $"Could not import host stats from {host}. Found more than one peer with IP {peerIp} in database.",
                Severity.Warn);
        }

        internal static void NoSyncing(this LogBase log, string host)
        {
            log.WriteLine(
                $"{host} is not syncing. Adjust clock to start sync.",
                Severity.Warn);
        }

        internal static void NtpValueError(this LogBase log, string name)
        {
            log.WriteLine(
                $"Received an unknown value from NTP Daemon: {name}",
                Severity.Warn);
        }

        internal static void OpenNtpUnsynced(this LogBase log)
        {
            log.WriteLine(
                "OpenNTP clock is unsynced.",
                Severity.Info);
        }

        internal static void PeerNotFound(this LogBase log, string host, string peerIp)
        {
            log.WriteLine(
                $"Could not import host stats from {host}. Peer with IP {peerIp} was not found in database.",
                Severity.Warn);
        }

        internal static void Syncing(this LogBase log, string host, string peer)
        {
            log.WriteLine(
                $"{host} is syncing to {peer}.",
                Severity.Info);
        }
    }
}