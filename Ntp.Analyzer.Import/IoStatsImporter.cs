// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    public sealed class IoStatsImporter : Importer<IoStatsEntry>
    {
        internal IoStatsImporter(string address, bool ntpq, LogBase log)
            : base(log)
        {
            this.address = address;
            this.ntpq = ntpq;
        }

        private readonly string address;
        private readonly bool ntpq;

        protected override string Command => ntpq ? "ntpq" : "ntpdc";

        protected override string Arguments => "-c iostats " + address;

        protected override string ErrorMessage => LogMessage.ImportIoError;

        protected override void ReadFromStream()
        {
            int timeSinceReset = -1;
            int receiveBuffers = -1;
            int freeReceiveBuffers = -1;
            int usedReceiveBuffers = -1;
            int lowWaterRefills = -1;
            long droppedPackets = -1;
            long ignoredPackets = -1;
            long receivedPackets = -1;
            long packetsSent = -1;
            long packetsNotSent = -1;
            int interruptsHandled = -1;
            int receivedByInt = -1;

            while (Reader.Peek() != -1)
            {
                string line = Reader.ReadLine();

                if (string.IsNullOrWhiteSpace(line) || line.Length < 24)
                    continue;

                string name = line.Substring(0, 22).Replace(":", string.Empty).TrimEnd();
                string value = line.Substring(22).TrimEnd();

                long parsedValue;

                if (!long.TryParse(value, out parsedValue))
                {
                    parsedValue = -2;
                }

                switch (name)
                {
                    case "time since reset":
                        timeSinceReset = (int) parsedValue;
                        break;
                    case "receive buffers":
                        receiveBuffers = (int) parsedValue;
                        break;
                    case "free receive buffers":
                        freeReceiveBuffers = (int) parsedValue;
                        break;
                    case "used receive buffers":
                        usedReceiveBuffers = (int) parsedValue;
                        break;
                    case "low water refills":
                        lowWaterRefills = (int) parsedValue;
                        break;
                    case "dropped packets":
                        droppedPackets = parsedValue;
                        break;
                    case "ignored packets":
                        ignoredPackets = parsedValue;
                        break;
                    case "received packets":
                        receivedPackets = parsedValue;
                        break;
                    case "packets sent":
                        packetsSent = parsedValue;
                        break;
                    case "packets not sent":
                    case "packet send failures":
                        packetsNotSent = parsedValue;
                        break;
                    case "input wakeups":
                    case "interrupts handled":
                        interruptsHandled = (int) parsedValue;
                        break;
                    case "useful input wakeups":
                    case "received by int":
                        receivedByInt = (int) parsedValue;
                        break;
                    default:
                        Log.NtpValueError(name);
                        break;
                }
            }

            Entries.Add(new IoStatsEntry(
                timeSinceReset,
                receiveBuffers,
                freeReceiveBuffers,
                usedReceiveBuffers,
                lowWaterRefills,
                droppedPackets,
                ignoredPackets,
                receivedPackets,
                packetsSent,
                packetsNotSent,
                interruptsHandled,
                receivedByInt));
        }
    }
}