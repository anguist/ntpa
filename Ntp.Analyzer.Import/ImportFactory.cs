﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    public static class ImportFactory
    {
        public static DriftFileImporter CreateDriftImporter(
            string file,
            ServerType type,
            Host host,
            ReadingBulk bulk,
            LogBase log)
        {
            return new DriftFileImporter(host, file, bulk, log);
        }

        public static Importer<HostReading> CreateHostImporter(
            string address,
            ServerType type,
            Host host,
            ReadingBulk bulk,
            LogBase log)
        {
            switch (type)
            {
                case ServerType.Ntpdc:
                    return new NtpdcImporter(address, host, bulk, log);
                case ServerType.Ntpq:
                    return new NtpqHostImporter(address, host, bulk, log);
                case ServerType.Ntpctl:
                    return new NtpctlHostImporter(host, bulk, log);
                default:
                    return null;
            }
        }

        public static Importer<IoStatsEntry> CreateIoImporter(
            string address,
            ServerType type,
            LogBase log)
        {
            return new IoStatsImporter(address, type == ServerType.Ntpq, log);
        }

        public static Importer<AssociationEntry> CreatePeerImporter(
            string address,
            ServerType type,
            Host host,
            LogBase log)
        {
            switch (type)
            {
                case ServerType.Ntpdc:
                case ServerType.Ntpq:
                    return new NtpqPeerImporter(address, host.Id, log);
                case ServerType.Ntpctl:
                    return new NtpctlPeerImporter(host.Id, log);
                default:
                    return null;
            }
        }
    }
}