﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    public sealed class NtpctlPeerImporter : NtpctlImporter<AssociationEntry>
    {
        internal NtpctlPeerImporter(int hostId, LogBase log)
            : base(hostId, log)
        {
        }

        protected override string ErrorMessage => LogMessage.ImportPeerError;

        protected override void ReadFromStream()
        {
            // Skip header
            Reader.ReadLine();
            Reader.ReadLine();

            while (Reader.Peek() != -1)
            {
                string peer = Reader.ReadLine();
                string stat = Reader.ReadLine();
                var entry = ParseLines(peer, stat);
                Entries.Add(entry);
            }
        }
    }
}