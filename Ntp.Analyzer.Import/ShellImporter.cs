﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using System.IO;
using Ntp.Common.IO;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Import
{
    public abstract class Importer<T> : IEnumerable<T>
    {
        protected Importer(LogBase log)
        {
            Log = log;
        }

        private readonly object locker = new object();
        private List<T> entries;

        protected StreamReader Reader { get; private set; }

        protected IList<T> Entries => entries;

        protected LogBase Log { get; }

        protected abstract string Command { get; }

        protected abstract string Arguments { get; }

        protected abstract string ErrorMessage { get; }

        public IEnumerator<T> GetEnumerator()
        {
            Execute();
            return entries.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Execute()
        {
            lock (locker)
            {
                if (entries != null)
                    return;

                Initialize();

                var command = new ShellCommand(Command, Arguments, Log, ErrorMessage);
                Reader = command.Execute();
                if (Reader == null)
                    return;

                ReadFromStream();
            }
        }

        protected virtual void Initialize()
        {
            entries = new List<T>();
        }

        protected abstract void ReadFromStream();
    }
}