// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Drawing;
using NPlot;
using Ntp.Analyzer.Interface;

namespace Ntp.Analyzer.Graph
{
    /// <summary>
    /// A dispersion graph contains an series of offsets and jitter values together with a time coordinate.
    /// </summary>
    public abstract class DispersionGraph : GraphBase
    {
        protected DispersionGraph(IDispersionGraphConfiguration configuration)
            : base(configuration)
        {
            config = configuration;
            Time = new List<DateTime>();
            Offset = new List<double>();
            Jitter = new List<double>();
        }

        private readonly IDispersionGraphConfiguration config;

        protected readonly List<double> Jitter;
        protected readonly List<double> Offset;
        protected readonly List<DateTime> Time;

        protected override void AddPlots()
        {
            var offsetPlot = SetupPlot("Offset", Color.Red, Time, Offset);
            var jitterPlot = SetupPlot("Jitter", Color.Blue, Time, Jitter);

            if (config.Jitter.HasValue)
                Surface.Add(jitterPlot, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left);

            if (config.Offset.HasValue)
                Surface.Add(offsetPlot, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left);
        }
    }
}