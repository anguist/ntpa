// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Export;
using Ntp.Analyzer.Objects;
using Ntp.Analyzer.Render;
using Ntp.Analyzer.Render.Host;

namespace Ntp.Analyzer.Page
{
    /// <summary>
    /// Build a HTML status page showing the state of a hosted NTP server.
    /// </summary>
    public sealed class DefaultHostPageBuilder : PageBuilderBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ntp.Analyzer.Page.DefaultHostPageBuilder" /> class.
        /// </summary>
        /// <param name="config">Config.</param>
        /// <param name="lines">Lines.</param>
        public DefaultHostPageBuilder(
            HostPageConfiguration config,
            IEnumerable<StatusLine> lines)
        {
            this.config = config;
            this.lines = new List<StatusLine>(lines);
        }

        private readonly HostPageConfiguration config;
        private readonly List<StatusLine> lines;

        public override IEnumerable<StreamDestination> Destinations => config.Destinations.Destinations;

        public override Stream Generate()
        {
            Host host = DataFace.Instance.Hosts.Single(h => h.Id == config.HostId);

            // Create page and table
            var hostPage = new DefaultHostPageRender(config, host.Ip, config.Frequency*60);

            var statusTable = new DefaultHostTableRender(config);
            hostPage.Add(statusTable);

            // Create table content
            foreach (StatusLine line in lines)
            {
                statusTable.Add(new HostLineRender(line, config.PeerPage));
            }

            // Create graph links
            hostPage.Add(new DefaultHostGraphRender(config.WebPath, config.Graphs));

            // Generate HTML
            var htmlRender = new HtmlRenderer(hostPage);
            string html = htmlRender.Render();

            return ToStream(html);
        }
    }
}