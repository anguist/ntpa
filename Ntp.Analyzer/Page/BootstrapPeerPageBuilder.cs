// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Export;
using Ntp.Analyzer.Objects;
using Ntp.Analyzer.Render;
using Ntp.Analyzer.Render.Peer;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Page
{
    public sealed class BootstrapPeerPageBuilder : PageBuilderBase
    {
        public BootstrapPeerPageBuilder(
            Host host,
            Peer peer,
            MenuConfiguration menu,
            PeerPageConfiguration page)
        {
            this.host = host;
            this.peer = peer;
            this.menu = menu;
            this.page = page;
        }

        private readonly Host host;
        private readonly MenuConfiguration menu;
        private readonly PeerPageConfiguration page;
        private readonly Peer peer;

        public override IEnumerable<StreamDestination> Destinations => page.Destinations.Destinations;

        public override Stream Generate()
        {
            DateTime now = DateTime.Now;
            var schedule = new JobScheduleDescription(false, false, page.Frequency);
            DateTime next = schedule.CalculateNextRun(now);
            int wait = Convert.ToInt32(next.Subtract(now).TotalSeconds) + 1;

            // Create page
            var peerPage = new BootstrapPeerPageRender(page.WebPath, peer.Name, wait);

            if (menu != null)
            {
                peerPage.Add(new BootstrapMenuRender(page.WebPath, menu, page));
            }

            peerPage.Add(new BootstrapPeerInfoRender(page.WebPath, peer.Server, now, next, peer.Name, peer.Ip));
            peerPage.Add(new BootstrapPeerGraphRender(page.WebPath, peer, host, page, page.Graphs));

            // Generate HTML
            var htmlRender = new HtmlRenderer(peerPage);
            string html = htmlRender.Render();

            return ToStream(html);
        }
    }
}