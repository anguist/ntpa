// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Export;
using Ntp.Analyzer.Objects;
using Ntp.Analyzer.Render;
using Ntp.Analyzer.Render.Host;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Page
{
    public sealed class BootstrapHostPageBuilder : PageBuilderBase
    {
        public BootstrapHostPageBuilder(
            MenuConfiguration menu,
            HostPageConfiguration page,
            IEnumerable<StatusLine> lines)
        {
            this.menu = menu;
            this.page = page;
            this.lines = new List<StatusLine>(lines);
        }

        private readonly List<StatusLine> lines;
        private readonly MenuConfiguration menu;
        private readonly HostPageConfiguration page;

        public override IEnumerable<StreamDestination> Destinations => page.Destinations.Destinations;

        public override Stream Generate()
        {
            DateTime now = page.ShowUtc == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;

            var schedule = new JobScheduleDescription(false, false, page.Frequency);
            DateTime next = schedule.CalculateNextRun(now);
            int wait = Convert.ToInt32(next.Subtract(now).TotalSeconds) + 1;

            Host host = DataFace.Instance.Hosts.Single(h => h.Id == page.HostId);

            var hostPage = new BootstrapHostPageRender(page.WebPath, page.Title, wait);

            if (menu != null)
            {
                var menuBar = new BootstrapMenuRender(page.WebPath, menu, page);
                hostPage.Add(menuBar);
            }

            var statusTable = new BootstrapHostTableRender(page, host.Ip, now, next);
            hostPage.Add(statusTable);

            // Create table content
            foreach (StatusLine line in lines)
            {
                statusTable.Add(new HostLineRender(line, page.PeerPage));
            }

            // Create graph links
            hostPage.Add(new BootstrapHostGraphRender(page.WebPath, page, page.Graphs));

            // Generate HTML
            var htmlRender = new HtmlRenderer(hostPage);
            string html = htmlRender.Render();

            return ToStream(html);
        }
    }
}