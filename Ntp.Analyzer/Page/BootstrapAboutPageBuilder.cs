// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.IO;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Export;
using Ntp.Analyzer.Render;

namespace Ntp.Analyzer.Page
{
    public sealed class BootstrapAboutPageBuilder : PageBuilderBase
    {
        public BootstrapAboutPageBuilder(
            MenuConfiguration menu,
            AboutPageConfiguration page)
        {
            this.menu = menu;
            this.page = page;
        }

        private readonly MenuConfiguration menu;
        private readonly AboutPageConfiguration page;

        public override IEnumerable<StreamDestination> Destinations => page.Destinations.Destinations;

        public override Stream Generate()
        {
            // Create page
            var aboutPage = new BootstrapAboutPageRender(
                page.WebPath, page.Title, page.Header, page.Content, page.ServerId
                );

            if (menu != null)
            {
                aboutPage.Add(new BootstrapMenuRender(page.WebPath, menu, page));
            }

            // Generate HTML
            var htmlRender = new HtmlRenderer(aboutPage);
            string html = htmlRender.Render();

            return ToStream(html);
        }
    }
}