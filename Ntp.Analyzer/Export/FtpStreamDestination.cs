// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using System.Net;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Export
{
    public sealed class FtpStreamDestination : StreamDestination
    {
        public FtpStreamDestination(string path, string file)
        {
            this.path = path;
            this.file = file;
        }

        private readonly string file;
        private readonly string path;

        public override string Link => string.Empty;

        public override string Value => file;

        public override string Location => file;

        public override string AbsoluteLocation => path + file;

        public override void Test()
        {
            /*
            string testFile = AbsoluteLocation;
            string testContent = "FileStreamDestinationTest";

            if (File.Exists(testFile))
            {
                File.Delete(testFile);
            }

            File.WriteAllText(testFile, testContent);

            File.Delete(testFile);
            */
        }

        public override void Write(Stream stream, LogBase log)
        {
            string absFile = AbsoluteLocation;

            if (File.Exists(absFile))
            {
                File.Delete(absFile);
            }

            try
            {
                var request = (FtpWebRequest) WebRequest.Create("ftp://172.20.83.1/test.png");
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential("ntapf", "ntpafps");
                request.UsePassive = false;

                request.ContentLength = stream.Length;
                Stream requestStream = request.GetRequestStream();
                stream.CopyTo(requestStream);
                requestStream.Close();

                var response = (FtpWebResponse) request.GetResponse();

                Console.WriteLine($"Upload File Complete, status {response.StatusDescription}");

                response.Close();
            }
            catch (Exception e)
            {
                log.WriteLine(e);
            }
        }
    }
}