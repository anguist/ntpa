// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.IO;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;
using Ntp.Analyzer.Interface;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Export
{
    public class FileStreamDestination : FileSystemDestination
    {
        public FileStreamDestination(string file)
        {
            this.file = file;
            value = file;
            ConfigValue = file;
        }

        private readonly string file;
        private readonly string value;
        private IPathProvider path;

        [NtpaSetting(Symbol.KeywordFile)]
        public string ConfigValue { get; }

        public override string Link => value;

        public override string Value => value;

        public override string Location => file;

        public override string AbsoluteLocation => JoinPath(path.FilePath, file);

        public override void Test()
        {
            TestFile(AbsoluteLocation + "-configtest", "FileStreamDestinationTest");
        }

        public override void Write(Stream stream, LogBase log)
        {
            PrepareFile(AbsoluteLocation, log);
            WriteFile(stream, AbsoluteLocation, log);
            ApplyPermissions(AbsoluteLocation, log);
        }

        internal void SetPath(IPathProvider pathProvider)
        {
            path = pathProvider;
        }
    }
}