﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Log
{
    internal static class LogExtensions
    {
        internal static void DestinationFileError(this LogBase log, string file, Exception e)
        {
            log.WriteLine(
                $"Could not write file {file}",
                e != null ? Severity.Warn : Severity.Notice);

            if (e != null)
            {
                log.WriteLine(e, Severity.Trace);
            }
        }

        internal static void DestinationNewDirectory(this LogBase log, string created)
        {
            log.WriteLine(
                $"Created new directory {created}",
                Severity.Notice);
        }

        internal static void DestinationOwnerError(this LogBase log, string file, Exception e = null)
        {
            log.WriteLine(
                $"Could not change ownership of file {file}",
                e != null ? Severity.Warn : Severity.Notice);

            if (e != null)
            {
                log.WriteLine(e, Severity.Trace);
            }
        }

        internal static void DestinationPathError(this LogBase log, string file, Exception e)
        {
            log.WriteLine(
                $"Could not create path for file {file}",
                e != null ? Severity.Warn : Severity.Notice);

            if (e != null)
            {
                log.WriteLine(e, Severity.Trace);
            }
        }

        internal static void DestinationPermissionError(this LogBase log, string file, Exception e = null)
        {
            log.WriteLine(
                $"Could not change permission on file {file}",
                e != null ? Severity.Warn : Severity.Notice);

            if (e != null)
            {
                log.WriteLine(e, Severity.Trace);
            }
        }

        internal static void WroteFile(this LogBase log, string file)
        {
            log.WriteLine(
                $"Wrote new file {file}",
                Severity.Debug);
        }
    }
}