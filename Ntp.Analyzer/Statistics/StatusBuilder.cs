﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Data;
using Ntp.Analyzer.Objects;

namespace Ntp.Analyzer.Statistics
{
    public abstract class StatusBuilder
    {
        protected StatusBuilder(Host host)
        {
            Host = host;
            Entries = new List<StatusLine>();
        }

        protected Host Host { get; }

        public List<StatusLine> Entries { get; }

        protected abstract IList<AssociationEntry> PeerAssociations { get; }

        public void Build()
        {
            var peers = DataFace.Instance.PeerActivities.
                Where(p => Equals(p.Host, Host) && p.IsActive);

            foreach (PeerActivity entry in peers)
            {
                var peerStatus = PeerAssociations.
                    SingleOrDefault(p => p.Remote == entry.Peer.Ip && p.HostId == entry.Host.Id);

                if (peerStatus == null)
                    continue;

                var line = new StatusLine(peerStatus, entry);
                Entries.Add(line);
            }
        }
    }
}