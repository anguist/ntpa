﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Config.Syntax.Option;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class HostGraphSyntaxNode : SyntaxNode<HostGraphConfiguration>
    {
        public HostGraphSyntaxNode(string name, int line)
            : base(Symbol.KeywordHostGraph, name, line, true)
        {
        }

        protected override HostGraphConfiguration InternalCompile()
        {
            var frequency = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFrequency) as IntegerSettingNode;
            var initialRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordInitialRun) as BooleanSettingNode;
            var fixedRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFixedRun) as BooleanSettingNode;
            var title = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTitle) as StringSettingNode;
            var width = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordWidth) as IntegerSettingNode;
            var height = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHeight) as IntegerSettingNode;
            var stamp = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTimeStamp) as TimeStampNode ??
                        Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphTime) as TimeStampNode;
            var timespan = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTimespan) as IntegerSettingNode;
            var filter = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFilterFactor) as NumericSettingNode;
            var gfrequency = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphFrequency) as NumericSettingNode;
            var stability = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphStability) as NumericSettingNode;
            var jitter = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordJitter) as NumericSettingNode;
            var offset = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordOffset) as NumericSettingNode;
            var dest = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDestinations) as FileDestinationSyntaxNode;
            var links = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordWebLinks) as WebLinkSyntaxNode;

            return new HostGraphConfiguration(
                Name,
                frequency?.Value,
                initialRun?.Value,
                fixedRun?.Value,
                title?.Value,
                width?.Value,
                height?.Value,
                timespan?.Value,
                stamp?.DateTimeKind,
                filter?.Value,
                gfrequency?.Value,
                stability?.Value,
                jitter?.Value,
                offset?.Value,
                dest?.Compile(),
                links?.Compile()
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordFrequency,
                Symbol.KeywordInitialRun,
                Symbol.KeywordFixedRun,
                Symbol.KeywordTitle,
                Symbol.KeywordWidth,
                Symbol.KeywordHeight,
                Symbol.KeywordTimespan,
                Symbol.KeywordTimeStamp,
                Symbol.KeywordGraphTime,
                Symbol.KeywordFilterFactor,
                Symbol.KeywordGraphFrequency,
                Symbol.KeywordGraphStability,
                Symbol.KeywordJitter,
                Symbol.KeywordOffset,
                Symbol.KeywordDestinations,
                Symbol.KeywordWebLinks
            });

            CheckAllIsPresent(new List<Symbol>
            {
                Symbol.KeywordTitle,
                Symbol.KeywordDestinations
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordGraphFrequency,
                Symbol.KeywordGraphStability,
                Symbol.KeywordJitter,
                Symbol.KeywordOffset
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordGraphTime,
                Symbol.KeywordTimeStamp
            });
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordFrequency);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordInitialRun);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordFixedRun);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordTitle);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordWidth);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordHeight);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordTimespan);
            CheckTypeIs<TimeStampNode>(Symbol.KeywordTimeStamp);
            CheckTypeIs<TimeStampNode>(Symbol.KeywordGraphTime);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordFilterFactor);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphFrequency);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphStability);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordJitter);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordOffset);
        }
    }
}