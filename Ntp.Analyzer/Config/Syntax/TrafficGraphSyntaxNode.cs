﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Config.Syntax.Option;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class TrafficGraphSyntaxNode : SyntaxNode<TrafficGraphConfiguration>
    {
        public TrafficGraphSyntaxNode(string name, int line)
            : base(Symbol.KeywordTrafficGraph, name, line, true)
        {
        }

        protected override TrafficGraphConfiguration InternalCompile()
        {
            var frequency = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFrequency) as IntegerSettingNode;
            var initialRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordInitialRun) as BooleanSettingNode;
            var fixedRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFixedRun) as BooleanSettingNode;
            var title = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTitle) as StringSettingNode;
            var width = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordWidth) as IntegerSettingNode;
            var height = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHeight) as IntegerSettingNode;
            var timespan = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTimespan) as IntegerSettingNode;
            var stamp = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTimeStamp) as TimeStampNode ??
                        Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphTime) as TimeStampNode;
            var recv = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphReceived) as NumericSettingNode;
            var ignore = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphIgnored) as NumericSettingNode;
            var drop = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphDropped) as NumericSettingNode;
            var sent = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphSent) as NumericSettingNode;
            var notSent = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphNotSent) as NumericSettingNode;
            var avgRecv = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphAvgReceived) as NumericSettingNode;
            var avgIgnore = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphAvgIgnored) as NumericSettingNode;
            var avgDrop = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphAvgDropped) as NumericSettingNode;
            var avgSent = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphAvgSent) as NumericSettingNode;
            var avgNotSent = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphAvgNotSent) as NumericSettingNode;
            var interval = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphPlotInterval) as IntegerSettingNode;
            var rate = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphPacketRate) as IntegerSettingNode;
            var dest = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDestinations) as FileDestinationSyntaxNode;
            var links = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordWebLinks) as WebLinkSyntaxNode;

            return new TrafficGraphConfiguration(
                Name,
                frequency?.Value,
                initialRun?.Value,
                fixedRun?.Value,
                title?.Value,
                width?.Value,
                height?.Value,
                timespan?.Value,
                stamp?.DateTimeKind,
                recv?.Value,
                ignore?.Value,
                drop?.Value,
                sent?.Value,
                notSent?.Value,
                avgRecv?.Value,
                avgIgnore?.Value,
                avgDrop?.Value,
                avgSent?.Value,
                avgNotSent?.Value,
                interval?.Value,
                rate?.Value,
                dest?.Compile(),
                links?.Compile()
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordFrequency,
                Symbol.KeywordInitialRun,
                Symbol.KeywordFixedRun,
                Symbol.KeywordTitle,
                Symbol.KeywordWidth,
                Symbol.KeywordHeight,
                Symbol.KeywordTimespan,
                Symbol.KeywordTimeStamp,
                Symbol.KeywordGraphTime,
                Symbol.KeywordGraphReceived,
                Symbol.KeywordGraphIgnored,
                Symbol.KeywordGraphDropped,
                Symbol.KeywordGraphSent,
                Symbol.KeywordGraphNotSent,
                Symbol.KeywordGraphAvgReceived,
                Symbol.KeywordGraphAvgIgnored,
                Symbol.KeywordGraphAvgDropped,
                Symbol.KeywordGraphAvgSent,
                Symbol.KeywordGraphAvgNotSent,
                Symbol.KeywordGraphPlotInterval,
                Symbol.KeywordGraphPacketRate,
                Symbol.KeywordDestinations,
                Symbol.KeywordWebLinks
            });

            CheckAllIsPresent(new List<Symbol>
            {
                Symbol.KeywordTitle,
                Symbol.KeywordDestinations
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordGraphReceived,
                Symbol.KeywordGraphIgnored,
                Symbol.KeywordGraphDropped,
                Symbol.KeywordGraphSent,
                Symbol.KeywordGraphNotSent,
                Symbol.KeywordGraphAvgReceived,
                Symbol.KeywordGraphAvgIgnored,
                Symbol.KeywordGraphAvgDropped,
                Symbol.KeywordGraphAvgSent,
                Symbol.KeywordGraphAvgNotSent,
                Symbol.KeywordGraphPlotInterval,
                Symbol.KeywordGraphPacketRate
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordGraphTime,
                Symbol.KeywordTimeStamp
            });
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordFrequency);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordInitialRun);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordFixedRun);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordTitle);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordWidth);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordHeight);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordTimespan);
            CheckTypeIs<TimeStampNode>(Symbol.KeywordGraphTime);
            CheckTypeIs<TimeStampNode>(Symbol.KeywordTimeStamp);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphReceived);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphIgnored);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphDropped);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphSent);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphNotSent);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphAvgReceived);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphAvgIgnored);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphAvgDropped);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphAvgSent);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphAvgNotSent);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphPlotInterval);
            CheckTypeIs<NumericSettingNode>(Symbol.KeywordGraphPacketRate);
        }
    }
}