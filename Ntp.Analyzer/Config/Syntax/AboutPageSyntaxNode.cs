﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Config.Syntax.Option;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class AboutPageSyntaxNode : SyntaxNode<AboutPageConfiguration>
    {
        public AboutPageSyntaxNode(string name, int line)
            : base(Symbol.KeywordAboutPage, name, line, true)
        {
        }

        private Uri location;

        protected override AboutPageConfiguration InternalCompile()
        {
            var frequency = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFrequency) as IntegerSettingNode;
            var initialRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordInitialRun) as BooleanSettingNode;
            var fixedRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFixedRun) as BooleanSettingNode;
            var title = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTitle) as StringSettingNode ??
                        Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPageTitle) as StringSettingNode;
            var serverId = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordServerId) as IntegerSettingNode;
            var contentTitle = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordContentTitle) as StringSettingNode;
            var content = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordContent) as StringSettingNode;
            var pageTemplate = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPageTemplate) as PageThemeNode;
            var dest = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDestinations) as FileDestinationSyntaxNode;

            return new AboutPageConfiguration(
                Name,
                frequency?.Value,
                initialRun?.Value,
                fixedRun?.Value,
                title?.Value,
                pageTemplate?.PageTheme,
                serverId?.Value,
                contentTitle?.Value,
                content?.Value,
                location,
                dest?.Compile()
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordFrequency,
                Symbol.KeywordInitialRun,
                Symbol.KeywordFixedRun,
                Symbol.KeywordTitle,
                Symbol.KeywordPageTitle,
                Symbol.KeywordPageTemplate,
                Symbol.KeywordContentTitle,
                Symbol.KeywordContent,
                Symbol.KeywordServerId,
                Symbol.KeywordLink
            });

            CheckAllIsPresent(new List<Symbol>
            {
                Symbol.KeywordDestinations,
                Symbol.KeywordContentTitle,
                Symbol.KeywordContent
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordTitle,
                Symbol.KeywordPageTitle
            });
        }

        protected override void ValidateReferences(SymbolTable table)
        {
            var link = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordLink) as StringSettingNode;
            if (link == null)
                return;

            var keyword = Keyword.Find(Symbol.KeywordLink).Name;
            location = CheckLink(link.Value, keyword);
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordFrequency);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordInitialRun);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordFixedRun);
            CheckTypeIs<PageThemeNode>(Symbol.KeywordPageTemplate);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordTitle);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordPageTitle);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordContentTitle);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordContent);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordServerId);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordLink);
        }
    }
}