﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.IO;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class PermissionSyntaxNode : SyntaxNode<PermissionConfiguration>
    {
        public PermissionSyntaxNode(string name, int line)
            : base(Symbol.KeywordPermission, name, line)
        {
        }

        private uint? appUserId;
        private uint? fileUserId;
        private uint? groupId;

        protected override PermissionConfiguration InternalCompile()
        {
            var appUserName = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordExecUser) as StringSettingNode;
            var fileUserName = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordUser) as StringSettingNode;
            var groupName = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGroup) as StringSettingNode;
            var fileMode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordMode) as IntegerSettingNode;

            return new PermissionConfiguration(
                Name,
                appUserId,
                appUserName?.Value,
                fileUserId,
                fileUserName?.Value,
                groupId,
                groupName?.Value,
                (uint?) fileMode?.Value
                );
        }

        protected override void InternalResolve(SymbolTable table)
        {
            // Find app user ID
            var appUserIdNode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordExecUser) as IntegerSettingNode;
            var appUserNameNode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordExecUser) as StringSettingNode;
            if (appUserNameNode != null)
            {
                appUserId = Permission.GetUserId(appUserNameNode.Value);
            }
            else if (appUserIdNode != null)
            {
                appUserId = (uint?) appUserIdNode.Value;
            }

            // Find file user ID
            var fileUserIdNode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordUser) as IntegerSettingNode;
            var fileUserNameNode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordUser) as StringSettingNode;
            if (fileUserNameNode != null)
            {
                fileUserId = Permission.GetUserId(fileUserNameNode.Value);
            }
            else if (fileUserIdNode != null)
            {
                fileUserId = (uint?) fileUserIdNode.Value;
            }

            // Find file group ID
            var groupIdNode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGroup) as IntegerSettingNode;
            var groupNameNode = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGroup) as StringSettingNode;
            if (groupNameNode != null)
            {
                groupId = Permission.GetGroupId(groupNameNode.Value);
            }
            else if (groupIdNode != null)
            {
                groupId = (uint?) groupIdNode.Value;
            }
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordExecUser,
                Symbol.KeywordUser,
                Symbol.KeywordGroup,
                Symbol.KeywordMode
            });
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode, StringSettingNode>(Symbol.KeywordExecUser);
            CheckTypeIs<IntegerSettingNode, StringSettingNode>(Symbol.KeywordUser);
            CheckTypeIs<IntegerSettingNode, StringSettingNode>(Symbol.KeywordGroup);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordMode);
        }
    }
}