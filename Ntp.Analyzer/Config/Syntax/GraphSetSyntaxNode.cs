﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class GraphSetSyntaxNode : SyntaxNode<GraphSetConfiguration>
    {
        public GraphSetSyntaxNode(string name, Symbol owner, int line)
            : base(Symbol.KeywordGraphSet, name, line)
        {
            graphs = new List<ISyntaxNode>();
            this.owner = owner;
        }

        private readonly List<ISyntaxNode> graphs;
        private readonly Symbol owner;

        protected override GraphSetConfiguration InternalCompile()
        {
            var title = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTitle) as StringSettingNode;
            var linkIndex = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordLinkIndex) as IntegerSettingNode;

            var list = new List<GraphBaseConfiguration>();
            foreach (var graph in graphs)
            {
                if (graph is HostGraphSyntaxNode)
                {
                    list.Add((graph as HostGraphSyntaxNode).Compile());
                }
                else if (graph is TrafficGraphSyntaxNode)
                {
                    list.Add((graph as TrafficGraphSyntaxNode).Compile());
                }
                else if (graph is PeerGraphSyntaxNode)
                {
                    list.Add((graph as PeerGraphSyntaxNode).Compile());
                }
                else
                {
                    AddError("Internal error in configuration compiler: GraphSetConfiguration");
                }
            }

            return new GraphSetConfiguration(
                Name,
                title?.Value,
                linkIndex?.Value,
                list
                );
        }

        protected override void InternalResolve(SymbolTable table)
        {
            graphs.AddRange(
                Nodes.
                    Where(n => n.Symbol == Symbol.KeywordGraph).
                    Cast<StringSettingNode>().
                    Select(graph => table.Lookup(graph.Value))
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordLinkIndex,
                Symbol.KeywordTitle
            });

            CheckAllIsPresent(new List<Symbol> {Symbol.KeywordGraph});
        }

        protected override void ValidateReferences(SymbolTable table)
        {
            string keyword = Keyword.Find(Symbol.KeywordGraph).Name;
            string peerGraphWord = Keyword.Find(Symbol.KeywordPeerGraph).Name;
            var graphNames = Nodes.Where(n => n.Symbol == Symbol.KeywordGraph).Cast<StringSettingNode>();

            foreach (var graph in graphNames)
            {
                var reference = table.Lookup(graph.Value);
                if (reference == null)
                {
                    AddReferenceNameError(graph, keyword, graph.Value);
                }
                else if (owner == Symbol.KeywordHostPage &&
                         !(reference is HostGraphSyntaxNode || reference is TrafficGraphSyntaxNode))
                {
                    AddReferenceTypeError(graph, keyword, keyword, graph.Value);
                }
                else if (owner == Symbol.KeywordPeerPage && !(reference is PeerGraphSyntaxNode))
                {
                    AddReferenceTypeError(graph, keyword, peerGraphWord, graph.Value);
                }
            }
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordLinkIndex);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordTitle);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordGraph);
        }
    }
}