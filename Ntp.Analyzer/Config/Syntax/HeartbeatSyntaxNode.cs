﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class HeartbeatSyntaxNode : SyntaxNode<HeartbeatConfiguration>
    {
        public HeartbeatSyntaxNode(string name, int line)
            : base(Symbol.KeywordDaemon, name, line)
        {
        }

        protected override HeartbeatConfiguration InternalCompile()
        {
            var rate = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHeartbeat) as IntegerSettingNode;
            return new HeartbeatConfiguration(Name, rate?.Value);
        }

        protected override void ValidateMandatories()
        {
            CheckAllIsPresent(new List<Symbol> {Symbol.KeywordHeartbeat});
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordHeartbeat);
        }
    }
}