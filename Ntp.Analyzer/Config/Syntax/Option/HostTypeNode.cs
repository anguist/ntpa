﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;
using Ntp.Analyzer.Import;

namespace Ntp.Analyzer.Config.Syntax.Option
{
    public sealed class HostTypeNode : SymbolSettingNode
    {
        public HostTypeNode()
            : base(Keyword.HostType, Symbol.Undefined, 0)
        {
        }

        public override string SettingValue => Keyword.Find(Symbol.KeywordNtpdc).Name + ", " +
                                               Keyword.Find(Symbol.KeywordNtpq).Name + " or " +
                                               Keyword.Find(Symbol.KeywordNtpctl).Name;

        public ServerType? HostType
        {
            get
            {
                switch (Value)
                {
                    case Symbol.KeywordNtpdc:
                        return ServerType.Ntpdc;
                    case Symbol.KeywordNtpq:
                        return ServerType.Ntpq;
                    case Symbol.KeywordNtpctl:
                        return ServerType.Ntpctl;
                    default:
                        return null;
                }
            }
        }
    }
}