﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Text;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Config.Syntax.Option
{
    public sealed class SeverityNode : SymbolSettingNode
    {
        public SeverityNode()
            : base(Keyword.Severity, Symbol.Undefined, 0)
        {
        }

        public override string SettingValue
        {
            get
            {
                var b = new StringBuilder();
                b.Append(Keyword.Find(Symbol.KeywordError).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordWarn).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordNotice).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordInfo).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordDebug).Name);
                b.Append(" or ");
                b.Append(Keyword.Find(Symbol.KeywordTrace).Name);
                return b.ToString();
            }
        }

        public Severity Severity
        {
            get
            {
                switch (Value)
                {
                    case Symbol.KeywordError:
                        return Severity.Error;
                    case Symbol.KeywordWarn:
                        return Severity.Warn;
                    case Symbol.KeywordNotice:
                        return Severity.Notice;
                    case Symbol.KeywordInfo:
                        return Severity.Info;
                    case Symbol.KeywordDebug:
                        return Severity.Debug;
                    case Symbol.KeywordTrace:
                        return Severity.Trace;
                    default:
                        return Severity.Trace;
                }
            }
        }
    }
}