﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Text;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Config.Syntax.Option
{
    public sealed class LogTypeNode : SymbolSettingNode
    {
        public LogTypeNode()
            : base(Keyword.Type, Symbol.Undefined, 0)
        {
        }

        public override string SettingValue
        {
            get
            {
                var b = new StringBuilder();
                b.Append(Keyword.Find(Symbol.KeywordConsole).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordSyslog).Name);
                b.Append(" or ");
                b.Append(Keyword.Find(Symbol.KeywordFile).Name);
                return b.ToString();
            }
        }

        public LogType LogType
        {
            get
            {
                switch (Value)
                {
                    case Symbol.KeywordConsole:
                        return LogType.Console;
                    case Symbol.KeywordSyslog:
                        return LogType.Syslog;
                    case Symbol.KeywordFile:
                        return LogType.File;
                    default:
                        return LogType.Unknown;
                }
            }
        }
    }
}