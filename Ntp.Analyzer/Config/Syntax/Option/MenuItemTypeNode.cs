﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Text;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax.Option
{
    public class MenuItemTypeNode : SymbolSettingNode
    {
        public MenuItemTypeNode()
            : base(Keyword.Type, Symbol.Undefined, 0)
        {
        }

        public override string SettingValue
        {
            get
            {
                var b = new StringBuilder();
                b.Append(Keyword.Find(Symbol.KeywordLink).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordName).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordPage).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordSpacer).Name);
                b.Append(", ");
                b.Append(Keyword.Find(Symbol.KeywordHeader).Name);
                b.Append(" or ");
                b.Append(Keyword.Find(Symbol.KeywordDropdown).Name);
                return b.ToString();
            }
        }

        public MenuItemType ItemType
        {
            get
            {
                switch (Value)
                {
                    case Symbol.KeywordLink:
                        return MenuItemType.Link;
                    case Symbol.KeywordName:
                        return MenuItemType.Name;
                    case Symbol.KeywordPage:
                        return MenuItemType.Page;
                    case Symbol.KeywordSpacer:
                        return MenuItemType.Spacer;
                    case Symbol.KeywordHeader:
                        return MenuItemType.Header;
                    case Symbol.KeywordDropdown:
                        return MenuItemType.Dropdown;
                    default:
                        return MenuItemType.Unknown;
                }
            }
        }
    }
}