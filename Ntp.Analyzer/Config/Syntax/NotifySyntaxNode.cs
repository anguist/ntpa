﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class NotifySyntaxNode : SyntaxNode<NotifyConfiguration>
    {
        public NotifySyntaxNode(string name, int line)
            : base(Symbol.KeywordNotify, name, line)
        {
        }

        protected override NotifyConfiguration InternalCompile()
        {
            var frequency = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFrequency) as IntegerSettingNode;
            var initialRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordInitialRun) as BooleanSettingNode;
            var fixedRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFixedRun) as BooleanSettingNode;
            var user = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordUser) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSmtpUser) as StringSettingNode;
            var pass = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPass) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSmtpPass) as StringSettingNode;
            var host = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordIp) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHost) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostAddress) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSmtpHost) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordAddress) as StringSettingNode;
            var port = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPort) as IntegerSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSmtpPort) as IntegerSettingNode;
            var ssl = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordEnableSsl) as BooleanSettingNode;
            var sender = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSender) as StringSettingNode;
            var recipient = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordRecipient) as StringSettingNode ??
                            Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordMail) as StringSettingNode;
            var subject = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSubject) as StringSettingNode;

            return new NotifyConfiguration(
                Name,
                initialRun?.Value,
                fixedRun?.Value,
                frequency?.Value,
                recipient?.Value,
                subject?.Value,
                sender?.Value,
                host?.Value,
                port?.Value,
                user?.Value,
                pass?.Value,
                ssl?.Value
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordFrequency,
                Symbol.KeywordInitialRun,
                Symbol.KeywordFixedRun,
                Symbol.KeywordUser,
                Symbol.KeywordSmtpUser,
                Symbol.KeywordPass,
                Symbol.KeywordSmtpPass,
                Symbol.KeywordIp,
                Symbol.KeywordHost,
                Symbol.KeywordAddress,
                Symbol.KeywordHostAddress,
                Symbol.KeywordSmtpHost,
                Symbol.KeywordPort,
                Symbol.KeywordSmtpPort,
                Symbol.KeywordEnableSsl,
                Symbol.KeywordSender,
                Symbol.KeywordMail,
                Symbol.KeywordRecipient,
                Symbol.KeywordSubject
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordRecipient,
                Symbol.KeywordMail
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordIp,
                Symbol.KeywordHost,
                Symbol.KeywordAddress,
                Symbol.KeywordHostAddress,
                Symbol.KeywordSmtpHost
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordSmtpPort,
                Symbol.KeywordPort
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordSmtpUser,
                Symbol.KeywordUser
            });

            CheckOnlyOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordSmtpPass,
                Symbol.KeywordPass
            });

            CheckAllIsPresent(new List<Symbol>
            {
                Symbol.KeywordSender,
                Symbol.KeywordSubject
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordRecipient,
                Symbol.KeywordMail
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordIp,
                Symbol.KeywordHost,
                Symbol.KeywordAddress,
                Symbol.KeywordSmtpHost
            });
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordFrequency);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordInitialRun);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordFixedRun);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordUser);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordPass);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordSmtpUser);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordSmtpPass);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordIp);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordHost);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordAddress);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordSmtpHost);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordPort);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordSmtpPort);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordEnableSsl);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordSender);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordRecipient);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordSubject);
        }
    }
}