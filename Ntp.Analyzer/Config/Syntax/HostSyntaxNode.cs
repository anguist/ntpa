﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Config.Syntax.Option;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class HostSyntaxNode : SyntaxNode<HostConfiguration>
    {
        public HostSyntaxNode(string name, int line)
            : base(Symbol.KeywordHost, name, line)
        {
        }

        private Uri location;

        protected override HostConfiguration InternalCompile()
        {
            var hostId = Nodes.Single(n => n.Symbol == Symbol.KeywordHostId) as IntegerSettingNode;
            var hostName = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordIp) as StringSettingNode ??
                           Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordAddress) as StringSettingNode ??
                           Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostAddress) as StringSettingNode;
            var hostType = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostType) as HostTypeNode;
            var filePath = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFilePath) as StringSettingNode;
            var menu = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordMenu) as MenuSyntaxNode;
            var aboutPage = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordAboutPage) as AboutPageSyntaxNode;
            var page1 = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostGraphPage) as HostGraphPageSyntaxNode;
            var page2 = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPeerGraphPage) as PeerGraphPageSyntaxNode;
            var hostStats = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostStats) as StatSyntaxNode;
            var hostIoStats = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostIoStats) as StatSyntaxNode;
            var peerStats = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPeerStats) as StatSyntaxNode;
            var driftStats = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDriftStats) as StatSyntaxNode;
            var hostGraphs = Nodes.Where(n => n.Symbol == Symbol.KeywordHostGraph).Cast<HostGraphSyntaxNode>();
            var hostPages = Nodes.Where(n => n.Symbol == Symbol.KeywordHostPage).Cast<HostPageSyntaxNode>();
            var peerGraphs = Nodes.Where(n => n.Symbol == Symbol.KeywordPeerGraph).Cast<PeerGraphSyntaxNode>();
            var peerPages = Nodes.Where(n => n.Symbol == Symbol.KeywordPeerPage).Cast<PeerPageSyntaxNode>();
            var trafficGraphs = Nodes.Where(n => n.Symbol == Symbol.KeywordTrafficGraph).Cast<TrafficGraphSyntaxNode>();
            var summery = Nodes.Where(n => n.Symbol == Symbol.KeywordPeerSummaryPage).Cast<PeerSummaryPageSyntaxNode>();

            if (hostId == null)
            {
                throw new InvalidOperationException(@"Internal compiler error: HostSyntaxNode");
            }

            return new HostConfiguration(
                Name,
                hostId.Value,
                hostName?.Value,
                hostType?.HostType,
                filePath?.Value,
                location,
                aboutPage?.Compile(),
                page1?.Compile(),
                hostIoStats?.Compile(),
                hostStats?.Compile(),
                menu?.Compile(),
                page2?.Compile(),
                peerStats?.Compile(),
                (DriftStatConfiguration) driftStats?.Compile(),
                hostGraphs.Select(c => c.Compile()),
                hostPages.Select(c => c.Compile()),
                peerGraphs.Select(c => c.Compile()),
                peerPages.Select(c => c.Compile()),
                summery.Select(c => c.Compile()),
                trafficGraphs.Select(c => c.Compile())
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordHostId,
                Symbol.KeywordFilePath,
                Symbol.KeywordWebPath,
                Symbol.KeywordHostType,
                Symbol.KeywordTimeStamp,
                Symbol.KeywordAboutPage,
                Symbol.KeywordHostGraphPage,
                Symbol.KeywordPeerGraphPage,
                Symbol.KeywordMenu,
                Symbol.KeywordHostStats,
                Symbol.KeywordHostIoStats,
                Symbol.KeywordPeerStats,
                Symbol.KeywordDriftStats
            });

            CheckAllIsPresent(new List<Symbol> {Symbol.KeywordHostId});

            if (Nodes.Count(n => n.RequirePath) != 0)
                CheckAllIsPresent(new List<Symbol> {Symbol.KeywordFilePath});
        }

        protected override void ValidateReferences(SymbolTable table)
        {
            var webPath = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordWebPath) as StringSettingNode;
            if (webPath == null)
                return;

            var keyword = Keyword.Find(Symbol.KeywordLink).Name;
            location = CheckLink(webPath.Value, keyword);
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordHostId);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordFilePath);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordWebPath);
            CheckTypeIs<HostTypeNode>(Symbol.KeywordHostType);
            CheckTypeIs<TimeStampNode>(Symbol.KeywordTimeStamp);
        }
    }
}