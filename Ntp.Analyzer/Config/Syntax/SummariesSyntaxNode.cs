﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Page.Collection;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class SummariesSyntaxNode : SyntaxNode<SummaryCollection>
    {
        public SummariesSyntaxNode(string name, int line)
            : base(Symbol.KeywordSummaries, name, line, true)
        {
            pages = new List<PeerSummaryPageSyntaxNode>();
        }

        private readonly List<PeerSummaryPageSyntaxNode> pages;

        protected override SummaryCollection InternalCompile()
        {
            return new SummaryCollection(Name, pages.Select(p => p.Compile()));
        }

        protected override void InternalResolve(SymbolTable table)
        {
            pages.AddRange(
                Nodes.
                    Where(n => n.Symbol == Symbol.KeywordPeerSummaryPage).
                    Cast<StringSettingNode>().
                    Select(page => table.Lookup(page.Value)).
                    Cast<PeerSummaryPageSyntaxNode>()
                );
        }

        protected override void ValidateMandatories()
        {
            CheckAllIsPresent(new List<Symbol> {Symbol.KeywordPeerSummaryPage});
        }

        protected override void ValidateReferences(SymbolTable table)
        {
            string keyword = Keyword.Find(Symbol.KeywordSummaries).Name;
            string pageKeyword = Keyword.Find(Symbol.KeywordPeerSummaryPage).Name;
            var pageNames = Nodes.Where(n => n.Symbol == Symbol.KeywordPeerSummaryPage).Cast<StringSettingNode>();

            foreach (var page in pageNames)
            {
                var reference = table.Lookup(page.Value);
                if (reference == null)
                {
                    AddReferenceNameError(page, keyword, page.Value);
                }
                else if (!(reference is PeerSummaryPageSyntaxNode))
                {
                    AddReferenceTypeError(page, keyword, pageKeyword, page.Value);
                }
            }
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<StringSettingNode>(Symbol.KeywordPeerSummaryPage);
        }
    }
}