﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;
using Ntp.Analyzer.Export;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class DirDestinationSyntaxNode : SyntaxNode<DirectoryCollection>
    {
        public DirDestinationSyntaxNode(string name, int line)
            : base(Symbol.KeywordDestinations, name, line)
        {
        }

        protected override DirectoryCollection InternalCompile()
        {
            var prefix = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPrefix) as StringSettingNode;
            var prefixval = prefix?.Value;
            var nodes = Nodes.OfType<StringSettingNode>().Where(n => n.Symbol == Symbol.KeywordDirectory);
            var dirs = nodes.Select(n => new DirectoryStreamDestination(n.Value, prefixval));
            return new DirectoryCollection(Name, dirs, prefixval);
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol> {Symbol.KeywordPrefix});
            CheckAllIsPresent(new List<Symbol> {Symbol.KeywordDirectory});
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<StringSettingNode>(Symbol.KeywordPrefix);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordDirectory);
        }
    }
}