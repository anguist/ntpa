﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Config.Syntax.Option;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class DatabaseSyntaxNode : SyntaxNode<DatabaseConfiguration>
    {
        public DatabaseSyntaxNode(string name, int line)
            : base(Symbol.KeywordDatabase, name, line)
        {
        }

        protected override DatabaseConfiguration InternalCompile()
        {
            var host = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordIp) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHost) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordAddress) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordHostAddress) as StringSettingNode;
            var port = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPort) as IntegerSettingNode;
            var database = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordName) as StringSettingNode ??
                           Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDatabase) as StringSettingNode;
            var user = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordUser) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDatabaseUser) as StringSettingNode;
            var pass = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordPass) as StringSettingNode ??
                       Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDatabasePass) as StringSettingNode;
            var create = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordCreate) as BooleanSettingNode;
            var upgrade = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordUpgrade) as BooleanSettingNode;
            var import = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordImport) as BooleanSettingNode;
            var provider = Nodes.Single(n => n.Symbol == Symbol.KeywordDatabaseProvider) as DatabaseProviderNode;
            var conString = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordConString) as StringSettingNode;
            var certFile = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordCertFile) as StringSettingNode;
            var certPass = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordCertPass) as StringSettingNode;
            var timeout = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordConnectionTimeout) as IntegerSettingNode;
            var protocol = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordProtocol) as IntegerSettingNode;
            var ssl = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordEnableSsl) as BooleanSettingNode;

            if (provider == null || host == null || database == null || user == null || pass == null)
            {
                throw new InvalidOperationException(@"Internal compiler error: DatabaseSyntaxNode");
            }

            return new DatabaseConfiguration(
                Name,
                provider.DatabaseProvider,
                host.Value,
                port?.Value,
                database.Value,
                user.Value,
                pass.Value,
                conString?.Value,
                certFile?.Value,
                certPass?.Value,
                timeout?.Value,
                protocol?.Value,
                ssl?.Value,
                create?.Value,
                upgrade?.Value,
                import?.Value
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordDatabaseProvider,
                Symbol.KeywordName,
                Symbol.KeywordHost,
                Symbol.KeywordIp,
                Symbol.KeywordAddress,
                Symbol.KeywordHostAddress,
                Symbol.KeywordPort,
                Symbol.KeywordUser,
                Symbol.KeywordPass,
                Symbol.KeywordCreate,
                Symbol.KeywordImport,
                Symbol.KeywordUpgrade,
                Symbol.KeywordDatabaseName,
                Symbol.KeywordDatabaseUser,
                Symbol.KeywordDatabasePass,
                Symbol.KeywordConString,
                Symbol.KeywordCertFile,
                Symbol.KeywordCertPass,
                Symbol.KeywordConnectionTimeout,
                Symbol.KeywordProtocol,
                Symbol.KeywordEnableSsl,
                Symbol.KeywordSslMode
            });

            CheckAllIsPresent(new List<Symbol> {Symbol.KeywordDatabaseProvider});

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordIp,
                Symbol.KeywordHost,
                Symbol.KeywordAddress,
                Symbol.KeywordHostAddress
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordName,
                Symbol.KeywordDatabaseName
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordUser,
                Symbol.KeywordDatabaseUser
            });

            CheckOneIsPresent(new List<Symbol>
            {
                Symbol.KeywordPass,
                Symbol.KeywordDatabasePass
            });
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<DatabaseProviderNode>(Symbol.KeywordDatabaseProvider);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordName);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordIp);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordHost);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordAddress);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordHostAddress);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordPort);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordUser);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordPass);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordCreate);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordUpgrade);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordConString);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordCertFile);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordCertPass);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordConnectionTimeout);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordProtocol);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordEnableSsl);
        }
    }
}