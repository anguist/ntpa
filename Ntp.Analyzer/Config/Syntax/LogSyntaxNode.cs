﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node;
using Ntp.Analyzer.Config.Syntax.Option;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class LogSyntaxNode : SyntaxNode<LogConfiguration>
    {
        public LogSyntaxNode(string name, int line)
            : base(Symbol.KeywordLog, name, line)
        {
        }

        protected override LogConfiguration InternalCompile()
        {
            var logType = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordType) as LogTypeNode;
            var threshold = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordSeverity) as SeverityNode;
            var showTime = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordShowTimestamp) as BooleanSettingNode;
            var showSeverity = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordShowSeverity) as BooleanSettingNode;
            var timeFormat = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordTimeFormat) as StringSettingNode;
            var file = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFile) as StringSettingNode;

            return new LogConfiguration(
                Name,
                logType?.LogType,
                threshold?.Severity,
                showTime?.Value,
                showSeverity?.Value,
                timeFormat?.Value,
                file?.Value
                );
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordType,
                Symbol.KeywordSeverity,
                Symbol.KeywordShowSeverity,
                Symbol.KeywordShowTimestamp,
                Symbol.KeywordTimeFormat,
                Symbol.KeywordFile
            });

            var type = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordType) as LogTypeNode;
            var fileName = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFile) as StringSettingNode;

            if ((type == null || type.LogType == LogType.File) && fileName == null)
            {
                AddError("Log file name is missing.");
            }
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<StringSettingNode>(Symbol.KeywordTimeFormat);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordFile);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordShowTimestamp);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordShowSeverity);
        }
    }
}