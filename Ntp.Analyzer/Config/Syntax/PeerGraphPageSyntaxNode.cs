﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Config.Syntax.Setting;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Syntax
{
    public sealed class PeerGraphPageSyntaxNode : SyntaxNode<PeerGraphPageConfiguration>
    {
        public PeerGraphPageSyntaxNode(string name, int line)
            : base(Symbol.KeywordPeerGraphPage, name, line, true)
        {
        }

        private Uri location;
        private PeerPageSyntaxNode pageNode;

        protected override PeerGraphPageConfiguration InternalCompile()
        {
            var frequency = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFrequency) as IntegerSettingNode;
            var initialRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordInitialRun) as BooleanSettingNode;
            var fixedRun = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordFixedRun) as BooleanSettingNode;
            var linkIndex = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordLinkIndex) as IntegerSettingNode;
            var dest = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordDestinations) as DirDestinationSyntaxNode;

            return new PeerGraphPageConfiguration(
                Name,
                frequency?.Value,
                initialRun?.Value,
                fixedRun?.Value,
                linkIndex?.Value,
                pageNode.Compile(),
                dest?.Compile(),
                location
                );
        }

        protected override void InternalResolve(SymbolTable table)
        {
            var name = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphPage) as StringSettingNode;
            if (name != null)
                pageNode = table.Lookup(name.Value) as PeerPageSyntaxNode;
        }

        protected override void ValidateMandatories()
        {
            CheckIsUnique(new List<Symbol>
            {
                Symbol.KeywordFrequency,
                Symbol.KeywordInitialRun,
                Symbol.KeywordFixedRun,
                Symbol.KeywordLinkIndex,
                Symbol.KeywordGraphPage,
                Symbol.KeywordLink,
                Symbol.KeywordDestinations
            });

            CheckAllIsPresent(new List<Symbol>
            {
                Symbol.KeywordGraphPage,
                Symbol.KeywordDestinations
            });
        }

        protected override void ValidateReferences(SymbolTable table)
        {
            var pageName = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordGraphPage) as StringSettingNode;
            if (pageName != null)
            {
                string keyword = Keyword.Find(Symbol.KeywordGraphPage).Name;

                var reference = table.Lookup(pageName.Value);
                if (reference == null)
                {
                    AddReferenceNameError(pageName, keyword, pageName.Value);
                }
                else if (!(reference is PeerPageSyntaxNode))
                {
                    string peerPageKeyword = Keyword.Find(Symbol.KeywordPeerPage).Name;
                    AddReferenceTypeError(pageName, keyword, peerPageKeyword, pageName.Value);
                }
            }

            var link = Nodes.SingleOrDefault(n => n.Symbol == Symbol.KeywordLink) as StringSettingNode;
            if (link == null)
                return;

            var linkKeyword = Keyword.Find(Symbol.KeywordLink).Name;
            location = CheckLink(link.Value, linkKeyword);
        }

        protected override void ValidateTypes()
        {
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordFrequency);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordInitialRun);
            CheckTypeIs<BooleanSettingNode>(Symbol.KeywordFixedRun);
            CheckTypeIs<IntegerSettingNode>(Symbol.KeywordLinkIndex);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordGraphPage);
            CheckTypeIs<StringSettingNode>(Symbol.KeywordLink);
        }
    }
}