﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using Ntp.Analyzer.Config.Syntax;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Compiler
{
    /// <summary>
    /// The config compiler transforms syntax nodes into configurations and
    /// assemble all missing links.
    /// </summary>
    public sealed class ConfigCompiler : ICompilerStage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigCompiler" /> class.
        /// </summary>
        /// <param name="root">The root node.</param>
        public ConfigCompiler(ISyntaxNode root)
        {
            this.root = root;
        }

        private readonly ISyntaxNode root;

        /// <summary>
        /// Gets or sets the symbol table.
        /// </summary>
        /// <remarks>The symbol table is not used by the compiler stage</remarks>
        /// <value>The symbol table.</value>
        public SymbolTable SymbolTable { get; set; }

        /// <summary>
        /// Gets the errors from this stage.
        /// </summary>
        /// <remarks>The compiler stage does not yield errors></remarks>
        /// <value>The errors.</value>
        public IEnumerable<string> Errors => new List<string>();

        /// <summary>
        /// Execute this stage.
        /// </summary>
        public void Execute()
        {
            Compile(root);
            Assemble(root);
        }

        /// <summary>
        /// Assemble compiled syntax tree buttom up.
        /// </summary>
        /// <param name="node">Node.</param>
        private static void Assemble(ISyntaxNode node)
        {
            foreach (var child in node)
            {
                Assemble(child);
                child.Assemble(node);
            }
        }

        /// <summary>
        /// Compile the specified syntax node tree buttom up.
        /// </summary>
        /// <param name="node">Node.</param>
        private static void Compile(ISyntaxNode node)
        {
            foreach (var child in node)
            {
                Compile(child);
            }

            node.CompileNode();
        }
    }
}