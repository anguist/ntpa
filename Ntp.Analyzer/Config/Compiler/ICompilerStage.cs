﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Compiler
{
    /// <summary>
    /// A compiler stage.
    /// </summary>
    public interface ICompilerStage
    {
        /// <summary>
        /// Gets or sets the symbol table.
        /// </summary>
        /// <value>The symbol table.</value>
        SymbolTable SymbolTable { get; set; }

        /// <summary>
        /// Gets the errors from this stage.
        /// </summary>
        /// <value>The errors.</value>
        IEnumerable<string> Errors { get; }

        /// <summary>
        /// Execute the compiler stage.
        /// </summary>
        void Execute();
    }
}