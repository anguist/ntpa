﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace Ntp.Analyzer.Config.Compiler
{
    public sealed class ConfigFileReader : IDisposable
    {
        public ConfigFileReader(string file)
        {
            File = file;
            reader = null;
        }

        private TextReader reader;

        public string File { get; }

        public int Line { get; private set; }

        public int Position { get; private set; }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "reader")]
        public void Dispose()
        {
            reader?.Close();
            reader?.Dispose();
        }

        public void NextLine()
        {
            Line++;
            Position = 1;
        }

        public char Peek()
        {
            if (reader == null)
                OpenFile();

            return reader != null && reader.Peek() != -1
                ? (char) reader.Peek()
                : (char) 0;
        }

        /// <summary>
        /// Consumes one character from file.
        /// </summary>
        public char Read()
        {
            if (reader == null)
                OpenFile();

            Position++;

            return reader != null
                ? (char) reader.Read()
                : (char) 0;
        }

        private void OpenFile()
        {
            reader = new StreamReader(File);
            Line = 1;
            Position = 1;
        }
    }
}