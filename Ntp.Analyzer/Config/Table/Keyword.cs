﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Ntp.Analyzer.Config.Table
{
    public sealed class Keyword
    {
        private Keyword(string name, Symbol symbol, Type valueType = null)
        {
            Name = name;
            Symbol = symbol;
            ValueType = valueType;
            LowerText = name.ToLower();
        }

        public static Keyword Undefined = new Keyword("Undefined", Symbol.Undefined);
        public static Keyword DatabaseProvider = new Keyword("Provider", Symbol.KeywordDatabaseProvider);
        public static Keyword PageTheme = new Keyword("Template", Symbol.KeywordPageTemplate);
        public static Keyword TimeStamp = new Keyword("TimeStamp", Symbol.KeywordTimeStamp);
        public static Keyword Severity = new Keyword("Severity", Symbol.KeywordSeverity);
        public static Keyword HostType = new Keyword("HostType", Symbol.KeywordHostType);
        public static Keyword Content = new Keyword("Content", Symbol.KeywordContent, typeof(string));
        public static Keyword Type = new Keyword("Type", Symbol.KeywordType, typeof(string));

        public static IEnumerable<Keyword> Keywords = new List<Keyword>
        {
            DatabaseProvider,
            PageTheme,
            TimeStamp,
            Severity,
            HostType,
            Content,
            Type,
            new Keyword("Database", Symbol.KeywordDatabase),
            new Keyword("Permission", Symbol.KeywordPermission),
            new Keyword("Listener", Symbol.KeywordListener),
            new Keyword("Cluster", Symbol.KeywordCluster),
            new Keyword("Log", Symbol.KeywordLog),
            new Keyword("Reading", Symbol.KeywordReading),
            new Keyword("Notify", Symbol.KeywordNotify),
            new Keyword("Server", Symbol.KeywordServer),
            new Keyword("HostStats", Symbol.KeywordHostStats),
            new Keyword("HostIoStats", Symbol.KeywordHostIoStats),
            new Keyword("PeerStats", Symbol.KeywordPeerStats),
            new Keyword("DriftStats", Symbol.KeywordDriftStats),
            new Keyword("Menu", Symbol.KeywordMenu),
            new Keyword("AboutPage", Symbol.KeywordAboutPage),
            new Keyword("HostPage", Symbol.KeywordHostPage),
            new Keyword("HostGraph", Symbol.KeywordHostGraph),
            new Keyword("PeerGraphs", Symbol.KeywordPeerGraph),
            new Keyword("TrafficGraph", Symbol.KeywordTrafficGraph),
            new Keyword("HostGraphPage", Symbol.KeywordHostGraphPage),
            new Keyword("PeerGraphPage", Symbol.KeywordPeerGraphPage),
            new Keyword("Images", Symbol.KeywordGraphSet),
            new Keyword("Destinations", Symbol.KeywordDestinations),
            new Keyword("Summaries", Symbol.KeywordSummaries),
            new Keyword("Item", Symbol.KeywordItem),
            new Keyword("Cluster", Symbol.KeywordClusterNode),
            new Keyword("Links", Symbol.KeywordWebLinks),
            new Keyword("Default", Symbol.KeywordDefault),
            new Keyword("Bootstrap", Symbol.KeywordBootstrap),
            new Keyword("MySQL", Symbol.KeywordDatabaseProviderMySql),
            new Keyword("PostgreSQL", Symbol.KeywordDatabaseProviderPostgre),
            new Keyword("Console", Symbol.KeywordConsole),
            new Keyword("Syslog", Symbol.KeywordSyslog),
            new Keyword("Error", Symbol.KeywordError),
            new Keyword("Warn", Symbol.KeywordWarn),
            new Keyword("Notice", Symbol.KeywordNotice),
            new Keyword("Info", Symbol.KeywordInfo),
            new Keyword("Debug", Symbol.KeywordDebug),
            new Keyword("Trace", Symbol.KeywordTrace),
            new Keyword("GraphTime", Symbol.KeywordGraphTime),
            new Keyword("PageTime", Symbol.KeywordPageTime),
            new Keyword("UTC", Symbol.KeywordTimeStampUtc),
            new Keyword("Local", Symbol.KeywordTimeStampLocal),
            new Keyword("ntpdc", Symbol.KeywordNtpdc),
            new Keyword("ntpq", Symbol.KeywordNtpq),
            new Keyword("ntpctl", Symbol.KeywordNtpctl),
            new Keyword("Spacer", Symbol.KeywordSpacer),
            new Keyword("Header", Symbol.KeywordHeader),
            new Keyword("True", Symbol.BooleanTrue),
            new Keyword("False", Symbol.BooleanFalse),
            new Keyword("On", Symbol.BooleanTrue),
            new Keyword("Off", Symbol.BooleanFalse),
            new Keyword("Unknown", Symbol.KeywordUnknown),
            new Keyword("Daemon", Symbol.KeywordDaemon),
            new Keyword("Service", Symbol.KeywordService),
            new Keyword("Protocol", Symbol.KeywordProtocol),
            new Keyword("SslMode", Symbol.KeywordSslMode),
            new Keyword("Required", Symbol.KeywordRequired),
            new Keyword("Preferred", Symbol.KeywordPreferred),
            new Keyword("PeerSummaryPage", Symbol.KeywordPeerSummaryPage, typeof(string)),
            new Keyword("Include", Symbol.KeywordInclude, typeof(string)),
            new Keyword("Host", Symbol.KeywordHost, typeof(string)),
            new Keyword("Port", Symbol.KeywordPort, typeof(int)),
            new Keyword("User", Symbol.KeywordUser, typeof(string)),
            new Keyword("Pass", Symbol.KeywordPass, typeof(string)),
            new Keyword("File", Symbol.KeywordFile, typeof(string)),
            new Keyword("Address", Symbol.KeywordAddress, typeof(string)),
            new Keyword("IP", Symbol.KeywordIp, typeof(string)),
            new Keyword("EnableSsl", Symbol.KeywordEnableSsl, typeof(bool)),
            new Keyword("Name", Symbol.KeywordName, typeof(string)),
            new Keyword("InitialRun", Symbol.KeywordInitialRun, typeof(bool)),
            new Keyword("FixedRun", Symbol.KeywordFixedRun, typeof(bool)),
            new Keyword("Frequency", Symbol.KeywordFrequency, typeof(int)),
            new Keyword("Create", Symbol.KeywordCreate, typeof(bool)),
            new Keyword("Upgrade", Symbol.KeywordUpgrade, typeof(bool)),
            new Keyword("Import", Symbol.KeywordImport, typeof(bool)),
            new Keyword("ShowTimestamp", Symbol.KeywordShowTimestamp, typeof(bool)),
            new Keyword("ShowSeverity", Symbol.KeywordShowSeverity, typeof(bool)),
            new Keyword("TimeFormat", Symbol.KeywordTimeFormat, typeof(string)),
            new Keyword("HostID", Symbol.KeywordHostId, typeof(int)),
            new Keyword("HostAddress", Symbol.KeywordHostAddress, typeof(string)),
            new Keyword("FilePath", Symbol.KeywordFilePath, typeof(string)),
            new Keyword("WebPath", Symbol.KeywordWebPath, typeof(string)),
            new Keyword("Recipient", Symbol.KeywordRecipient, typeof(string)),
            new Keyword("Subject", Symbol.KeywordSubject, typeof(string)),
            new Keyword("Sender", Symbol.KeywordSender, typeof(string)),
            new Keyword("Mail", Symbol.KeywordMail, typeof(string)),
            new Keyword("PageTitle", Symbol.KeywordPageTitle, typeof(string)),
            new Keyword("PoolMember", Symbol.KeywordPoolMember, typeof(bool)),
            new Keyword("QueryDirect", Symbol.KeywordQueryDirect, typeof(bool)),
            new Keyword("ServerID", Symbol.KeywordServerId, typeof(int)),
            new Keyword("ContentTitle", Symbol.KeywordContentTitle, typeof(string)),
            new Keyword("Link", Symbol.KeywordLink, typeof(string)),
            new Keyword("PeerPages", Symbol.KeywordPeerPage, typeof(string)),
            new Keyword("GraphPage", Symbol.KeywordGraphPage, typeof(string)),
            new Keyword("Title", Symbol.KeywordTitle, typeof(string)),
            new Keyword("Width", Symbol.KeywordWidth, typeof(int)),
            new Keyword("Height", Symbol.KeywordHeight, typeof(int)),
            new Keyword("Timespan", Symbol.KeywordTimespan, typeof(int)),
            new Keyword("FilterFactor", Symbol.KeywordFilterFactor, typeof(double)),
            new Keyword("Jitter", Symbol.KeywordJitter, typeof(double)),
            new Keyword("Offset", Symbol.KeywordOffset, typeof(double)),
            new Keyword("GFrequency", Symbol.KeywordGraphFrequency, typeof(double)),
            new Keyword("Stability", Symbol.KeywordGraphStability, typeof(double)),
            new Keyword("Balance", Symbol.KeywordGraphBalance, typeof(double)),
            new Keyword("Delay", Symbol.KeywordGraphDelay, typeof(double)),
            new Keyword("Received", Symbol.KeywordGraphReceived, typeof(double)),
            new Keyword("Ignored", Symbol.KeywordGraphIgnored, typeof(double)),
            new Keyword("Dropped", Symbol.KeywordGraphDropped, typeof(double)),
            new Keyword("Sent", Symbol.KeywordGraphSent, typeof(double)),
            new Keyword("NotSent", Symbol.KeywordGraphNotSent, typeof(double)),
            new Keyword("AvgReceived", Symbol.KeywordGraphAvgReceived, typeof(double)),
            new Keyword("AvgIgnored", Symbol.KeywordGraphAvgIgnored, typeof(double)),
            new Keyword("AvgDropped", Symbol.KeywordGraphAvgDropped, typeof(double)),
            new Keyword("AvgSent", Symbol.KeywordGraphAvgSent, typeof(double)),
            new Keyword("AvgNotSent", Symbol.KeywordGraphAvgNotSent, typeof(double)),
            new Keyword("PlotInterval", Symbol.KeywordGraphPlotInterval, typeof(double)),
            new Keyword("PacketRate", Symbol.KeywordGraphPacketRate, typeof(double)),
            new Keyword("Graph", Symbol.KeywordGraph, typeof(string)),
            new Keyword("LinkIndex", Symbol.KeywordLinkIndex, typeof(int)),
            new Keyword("Prefix", Symbol.KeywordPrefix, typeof(string)),
            new Keyword("Directory", Symbol.KeywordDirectory, typeof(string)),
            new Keyword("Caption", Symbol.KeywordCaption, typeof(string)),
            new Keyword("Page", Symbol.KeywordPage, typeof(string)),
            new Keyword("Heartbeat", Symbol.KeywordHeartbeat, typeof(int)),
            new Keyword("Rate", Symbol.KeywordRate, typeof(int)),
            new Keyword("ExecUser", Symbol.KeywordExecUser, typeof(string)),
            new Keyword("Group", Symbol.KeywordGroup, typeof(string)),
            new Keyword("Mode", Symbol.KeywordMode, typeof(int)),
            new Keyword("Dropdown", Symbol.KeywordDropdown, typeof(string)),
            new Keyword("ConnectionString", Symbol.KeywordConString, typeof(string)),
            new Keyword("CertificateFile", Symbol.KeywordCertFile, typeof(string)),
            new Keyword("CertificatePassword", Symbol.KeywordCertPass, typeof(string)),
            new Keyword("ConnectionTimeout", Symbol.KeywordConnectionTimeout, typeof(int)),
            // Deprecated keywords
            new Keyword("DatabaseName", Symbol.KeywordDatabaseName, typeof(string)),
            new Keyword("DatabaseUser", Symbol.KeywordDatabaseUser, typeof(string)),
            new Keyword("DatabasePass", Symbol.KeywordDatabasePass, typeof(string)),
            new Keyword("SmtpHost", Symbol.KeywordSmtpHost, typeof(string)),
            new Keyword("SmtpPort", Symbol.KeywordSmtpPort, typeof(int)),
            new Keyword("SmtpUser", Symbol.KeywordSmtpUser, typeof(string)),
            new Keyword("SmtpPass", Symbol.KeywordSmtpPass, typeof(string))
        };

        public string Name { get; }

        public Type ValueType { get; }

        public string LowerText { get; }

        public Symbol Symbol { get; }

        public override bool Equals(object obj)
        {
            var other = obj as Keyword;
            if (other == null)
                return false;

            return other.LowerText == LowerText;
        }

        public static Keyword Find(Symbol symbol)
        {
            return Keywords.FirstOrDefault(k => k.Symbol == symbol);
        }

        public override int GetHashCode()
        {
            return LowerText.GetHashCode();
        }
    }
}