﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Ntp.Analyzer.Config.Table
{
    public sealed class SymbolToken : Token
    {
        public SymbolToken(Symbol symbol)
        {
            this.symbol = symbol;
            Text = string.Empty;
        }

        public SymbolToken(Symbol symbol, string text)
            : this(symbol)
        {
            Text = text;
        }

        private readonly Symbol symbol;

        public override TokenType TokenType => TokenType.Symbol;

        public override Symbol Symbol => symbol;

        public override string Text { get; }

        public override bool Equals(object obj)
        {
            var other = obj as SymbolToken;
            return other?.symbol == symbol;
        }

        public override int GetHashCode()
        {
            return symbol.GetHashCode();
        }
    }
}