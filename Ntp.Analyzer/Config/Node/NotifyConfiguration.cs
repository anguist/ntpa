// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class NotifyConfiguration : ConfigurationNode, IJobConfiguration
    {
        internal NotifyConfiguration(
            string configName,
            bool? initialRun,
            bool? fixedRun,
            int? frequency,
            string mail,
            string subject,
            string sender,
            string server,
            int? port,
            string user,
            string pass,
            bool? ssl)
            : base(configName)
        {
            this.frequency = frequency;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            Mail = mail;
            Subject = subject;
            Sender = sender;
            SmptServer = server;
            this.port = port;
            SmtpUser = user;
            SmtpPass = pass;
            this.ssl = ssl;
        }

        private readonly bool? fixedRun;

        private readonly int? frequency;
        private readonly bool? initialRun;
        private readonly int? port;
        private readonly bool? ssl;

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordRecipient)]
        public string Mail { get; }

        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordSubject, Layout = Layout.Quoted)]
        public string Subject { get; }

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordSender)]
        public string Sender { get; }

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordHost)]
        public string SmptServer { get; }

        [NtpaIndex(21)]
        [NtpaSetting(Symbol.KeywordPort, 25)]
        public int SmtpPort => port ?? 25;

        [NtpaIndex(22)]
        [NtpaSetting(Symbol.KeywordUser)]
        public string SmtpUser { get; }

        [NtpaIndex(23)]
        [NtpaSetting(Symbol.KeywordPass)]
        public string SmtpPass { get; }

        [NtpaIndex(24)]
        [NtpaSetting(Symbol.KeywordEnableSsl, false)]
        public bool EnableSsl => ssl ?? false;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency, 1440)]
        public int Frequency => frequency ?? 1440;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, true)]
        public bool InitialRun => initialRun ?? true;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, true)]
        public bool FixedRun => fixedRun ?? true;
    }
}