// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class LogConfiguration : ConfigurationNode, ILogConfiguration
    {
        internal LogConfiguration(
            string name,
            LogType? logType,
            Severity? threshold,
            bool? showTimestamp,
            bool? showSeverity,
            string timeFormat,
            string file
            ) : base(name)
        {
            this.logType = logType;
            this.threshold = threshold;
            this.showTimestamp = showTimestamp;
            this.showSeverity = showSeverity;
            this.timeFormat = timeFormat;
            File = file;
        }

        private readonly LogType? logType;
        private readonly bool? showSeverity;
        private readonly bool? showTimestamp;
        private readonly string timeFormat;
        private readonly Severity? threshold;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFile)]
        public string File { get; }

        [NtpaIndex(21)]
        [NtpaSetting(Symbol.KeywordSeverity, Severity.Info)]
        public Severity Threshold => threshold ?? Severity.Info;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordType, LogType.File)]
        public LogType LogType => logType ?? LogType.File;

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordShowTimestamp, true)]
        public bool ShowTimestamp => showTimestamp ?? true;

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordShowSeverity, true)]
        public bool ShowSeverity => showSeverity ?? true;

        [NtpaIndex(21)]
        [NtpaSetting(Symbol.KeywordTimeFormat, "yyyy-MM-dd HH:mm:ss", Layout = Layout.Quoted)]
        public string TimeFormat => timeFormat ?? "yyyy-MM-dd HH:mm:ss";
    }
}