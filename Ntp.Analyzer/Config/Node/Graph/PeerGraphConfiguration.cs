// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Drawing.Imaging;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Table;
using Ntp.Analyzer.Interface;
using Ntp.Analyzer.Objects;
using Ntp.Common.Process;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Config.Node.Graph
{
    /// <summary>
    /// Peer graph configuration used when creating <seealso cref="Ntp.Analyzer.Graph.PeerGraph" />.
    /// </summary>
    public sealed class PeerGraphConfiguration : GraphBaseConfiguration, IPeerGraphConfiguration, IJobConfiguration
    {
        internal PeerGraphConfiguration(
            string name,
            int? frequency,
            bool? initialRun,
            bool? fixedRun,
            string title,
            int? width,
            int? height,
            int? timespan,
            DateTimeKind? graphTime,
            double? filterFactor,
            double? balance,
            double? delay,
            double? jitter,
            double? offset,
            DestinationCollection destinations,
            WebLinkCollection links)
            : base(name)
        {
            this.frequency = frequency;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            this.timespan = timespan;
            this.height = height;
            this.width = width;
            this.graphTime = graphTime;
            FilterFactor = filterFactor;
            Jitter = jitter;
            Offset = offset;
            Balance = balance;
            Delay = delay;
            Title = title;
            Destinations = destinations;
            ConfigLinks = links;
        }

        private readonly bool? fixedRun;
        private readonly int? frequency;
        private readonly DateTimeKind? graphTime;
        private readonly int? height;
        private readonly bool? initialRun;
        private readonly int? timespan;
        private readonly int? width;

        [NtpaIndex(50)]
        [NtpaSetting(Symbol.KeywordDestinations)]
        public override DestinationCollection Destinations { get; }

        [NtpaIndex(51)]
        [NtpaSetting(Symbol.KeywordWebLinks)]
        public override WebLinkCollection ConfigLinks { get; }

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency, 5)]
        public int Frequency => frequency ?? 5;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, false)]
        public bool InitialRun => initialRun ?? false;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, true)]
        public bool FixedRun => fixedRun ?? true;

        /// <summary>
        /// Gets the timezone to use for timeline.
        /// </summary>
        /// <value>The graph timezone.</value>
        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordGraphTime, DateTimeKind.Utc)]
        public override DateTimeKind GraphTime => graphTime ?? DateTimeKind.Utc;

        /// <summary>
        /// Gets the title of this graph.
        /// </summary>
        /// <value>The title.</value>
        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordTitle, Layout = Layout.Quoted)]
        public override string Title { get; }

        /// <summary>
        /// Gets the timespan for this graph.
        /// </summary>
        /// <value>The timespan.</value>
        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordTimespan, 60*12)]
        public override int Timespan => timespan ?? 60*12;

        /// <summary>
        /// Gets the width in pixels of generated graph.
        /// </summary>
        /// <value>The width.</value>
        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordWidth, 1024)]
        public override int Width => width ?? 1024;

        /// <summary>
        /// Gets the height in pixels of generated graph.
        /// </summary>
        /// <value>The height.</value>
        [NtpaIndex(21)]
        [NtpaSetting(Symbol.KeywordHeight, 550)]
        public override int Height => height ?? 550;

        /// <summary>
        /// Gets the format to use for destinations.
        /// </summary>
        /// <value>The format.</value>
        [NtpaIndex(22)]
        public override ImageFormat Format => ImageFormat.Png;

        /// <summary>
        /// Gets the filter factor. Used to filter away extreme values.
        /// </summary>
        /// <value>The filter factor.</value>
        [NtpaIndex(40)]
        [NtpaSetting(Symbol.KeywordFilterFactor)]
        public double? FilterFactor { get; }

        /// <summary>
        /// Gets the jitter multiplication factor.
        /// </summary>
        /// <value>The factor.</value>
        [NtpaIndex(31)]
        [NtpaSetting(Symbol.KeywordJitter)]
        public double? Jitter { get; }

        /// <summary>
        /// Gets the offset multiplication factor.
        /// </summary>
        /// <value>The factor.</value>
        [NtpaIndex(30)]
        [NtpaSetting(Symbol.KeywordOffset)]
        public double? Offset { get; }

        /// <summary>
        /// Gets the balance multiplication factor.
        /// </summary>
        /// <value>The balance.</value>
        [NtpaIndex(32)]
        [NtpaSetting(Symbol.KeywordGraphBalance)]
        public double? Balance { get; }

        /// <summary>
        /// Gets the delay multiplication factor.
        /// </summary>
        /// <value>The delay.</value>
        [NtpaIndex(33)]
        [NtpaSetting(Symbol.KeywordGraphDelay)]
        public double? Delay { get; }

        public override string GetTitle(NamedObject namedObject)
        {
            return string.Concat(namedObject.DisplayName, " ", Title);
        }

        public override string GetAltName(GraphSetConfiguration owner, string postfix)
        {
            string peerName = postfix;
            return GraphName + "-" + peerName.Replace('.', '-') + ".png";
        }

        public override Uri GetLink(GraphSetConfiguration owner, string postfix)
        {
            return WebPath.
                Append(Links.Destinations[owner.LinkIndex - 1].Link).
                AppendExtension(postfix.Replace('.', '-')).
                AppendExtension(".png");
        }
    }
}