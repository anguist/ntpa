// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Drawing.Imaging;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Table;
using Ntp.Analyzer.Interface;
using Ntp.Analyzer.Objects;
using Ntp.Common.Process;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Config.Node.Graph
{
    /// <summary>
    /// Traffic graph configuration used when creating <seealso cref="Analyzer.Graph.TrafficGraph" />.
    /// </summary>
    public sealed class TrafficGraphConfiguration : GraphBaseConfiguration, ITrafficGraphConfiguration,
        IJobConfiguration
    {
        internal TrafficGraphConfiguration(
            string name,
            int? frequency,
            bool? initialRun,
            bool? fixedRun,
            string title,
            int? width,
            int? height,
            int? timespan,
            DateTimeKind? graphTime,
            double? received,
            double? ignored,
            double? dropped,
            double? sent,
            double? notSent,
            double? receivedAvg,
            double? ignoredAvg,
            double? droppedAvg,
            double? sentAvg,
            double? notSentAvg,
            int? plotInterval,
            int? packetRate,
            DestinationCollection destinations,
            WebLinkCollection links)
            : base(name)
        {
            this.frequency = frequency;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            Title = title;
            this.timespan = timespan;
            this.height = height;
            this.width = width;
            this.graphTime = graphTime;
            Received = received;
            Ignored = ignored;
            Dropped = dropped;
            Sent = sent;
            NotSent = notSent;
            ReceivedAvg = receivedAvg;
            IgnoredAvg = ignoredAvg;
            DroppedAvg = droppedAvg;
            SentAvg = sentAvg;
            NotSentAvg = notSentAvg;
            this.plotInterval = plotInterval;
            this.packetRate = packetRate;
            Destinations = destinations;
            ConfigLinks = links;
        }

        private readonly bool? fixedRun;
        private readonly int? frequency;
        private readonly DateTimeKind? graphTime;
        private readonly int? height;
        private readonly bool? initialRun;
        private readonly int? packetRate;
        private readonly int? plotInterval;
        private readonly int? timespan;
        private readonly int? width;

        [NtpaIndex(50)]
        [NtpaSetting(Symbol.KeywordDestinations)]
        public override DestinationCollection Destinations { get; }

        [NtpaIndex(51)]
        [NtpaSetting(Symbol.KeywordWebLinks)]
        public override WebLinkCollection ConfigLinks { get; }

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency, 5)]
        public int Frequency => frequency ?? 5;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, false)]
        public bool InitialRun => initialRun ?? false;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, true)]
        public bool FixedRun => fixedRun ?? true;

        /// <summary>
        /// Gets the title of this graph.
        /// </summary>
        /// <value>The title.</value>
        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordTitle, Layout = Layout.Quoted)]
        public override string Title { get; }

        /// <summary>
        /// Gets the timespan for this graph.
        /// </summary>
        /// <value>The timespan.</value>
        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordTimespan, 60*12)]
        public override int Timespan => timespan ?? 60*12;

        /// <summary>
        /// Gets the width in pixels of generated graph.
        /// </summary>
        /// <value>The width.</value>
        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordWidth, 1024)]
        public override int Width => width ?? 1024;

        /// <summary>
        /// Gets the height in pixels of generated graph.
        /// </summary>
        /// <value>The height.</value>
        [NtpaIndex(21)]
        [NtpaSetting(Symbol.KeywordHeight, 550)]
        public override int Height => height ?? 550;

        /// <summary>
        /// Gets the format to use for destinations.
        /// </summary>
        /// <value>The format.</value>
        [NtpaIndex(22)]
        public override ImageFormat Format => ImageFormat.Png;

        [NtpaIndex(31)]
        [NtpaSetting(Symbol.KeywordGraphReceived)]
        public double? Received { get; }

        [NtpaIndex(32)]
        [NtpaSetting(Symbol.KeywordGraphIgnored)]
        public double? Ignored { get; }

        [NtpaIndex(33)]
        [NtpaSetting(Symbol.KeywordGraphDropped)]
        public double? Dropped { get; }

        [NtpaIndex(34)]
        [NtpaSetting(Symbol.KeywordGraphSent)]
        public double? Sent { get; }

        [NtpaIndex(35)]
        [NtpaSetting(Symbol.KeywordGraphNotSent)]
        public double? NotSent { get; }

        [NtpaIndex(36)]
        [NtpaSetting(Symbol.KeywordGraphAvgReceived)]
        public double? ReceivedAvg { get; }

        [NtpaIndex(37)]
        [NtpaSetting(Symbol.KeywordGraphAvgIgnored)]
        public double? IgnoredAvg { get; }

        [NtpaIndex(38)]
        [NtpaSetting(Symbol.KeywordGraphAvgDropped)]
        public double? DroppedAvg { get; }

        [NtpaIndex(39)]
        [NtpaSetting(Symbol.KeywordGraphAvgSent)]
        public double? SentAvg { get; }

        [NtpaIndex(40)]
        [NtpaSetting(Symbol.KeywordGraphAvgNotSent)]
        public double? NotSentAvg { get; }

        [NtpaIndex(13)]
        [NtpaSetting(Symbol.KeywordGraphPlotInterval, 15)]
        public int PlotInterval => plotInterval ?? 15;

        [NtpaIndex(14)]
        [NtpaSetting(Symbol.KeywordGraphPacketRate, 1)]
        public int PacketRate => packetRate ?? 1;

        /// <summary>
        /// Gets the timezone to use for timeline.
        /// </summary>
        /// <value>The graph timezone.</value>
        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordGraphTime, DateTimeKind.Utc)]
        public override DateTimeKind GraphTime => graphTime ?? DateTimeKind.Utc;

        /// <summary>
        /// Gets the alternative name when referred to from web pages.
        /// </summary>
        /// <returns>The alternate name.</returns>
        /// <param name="owner">Owner.</param>
        /// <param name="postfix">Text to prepend</param>
        public override string GetAltName(GraphSetConfiguration owner, string postfix)
        {
            int index = owner.LinkIndex;
            string name = Links.Destinations[index - 1].Link;

            // remove any path from name
            int pos = name.LastIndexOf('/');
            name = pos != -1 ? name.Remove(0, pos + 1) : name;

            return name;
        }

        /// <summary>
        /// Gets the link to use when referred to from web pages.
        /// </summary>
        /// <returns>The link.</returns>
        /// <param name="owner">Owner.</param>
        /// <param name="postfix">Text to prepend</param>
        public override Uri GetLink(GraphSetConfiguration owner, string postfix)
        {
            return WebPath.Append(Links.Destinations[owner.LinkIndex - 1].Link);
        }

        public override string GetTitle(NamedObject namedObject)
        {
            return Title;
        }
    }
}