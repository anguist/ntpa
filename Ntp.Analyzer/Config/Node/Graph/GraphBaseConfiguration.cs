// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Export;
using Ntp.Analyzer.Interface;
using Ntp.Analyzer.Objects;

namespace Ntp.Analyzer.Config.Node.Graph
{
    /// <summary>
    /// Base class for graph configurations.
    /// </summary>
    public abstract class GraphBaseConfiguration : HostSubConfiguration, IGraphBaseConfiguration
    {
        protected GraphBaseConfiguration(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Gets the name of this graph.
        /// </summary>
        /// <value>The name.</value>
        public string GraphName => ConfigName;

        public abstract DestinationCollection Destinations { get; }

        public abstract WebLinkCollection ConfigLinks { get; }

        protected WebLinkCollection Links
            => new WebLinkCollection(null, new List<WebLink> {new WebLink(Destinations.First().Link)});

        /// <summary>
        /// Gets the title of this graph.
        /// </summary>
        /// <value>The title.</value>
        public abstract string Title { get; }

        /// <summary>
        /// Gets the timespan for this graph.
        /// </summary>
        /// <value>The timespan.</value>
        public abstract int Timespan { get; }

        /// <summary>
        /// Gets the width in pixels of generated graph.
        /// </summary>
        /// <value>The width.</value>
        public abstract int Width { get; }

        /// <summary>
        /// Gets the height in pixels of generated graph.
        /// </summary>
        /// <value>The height.</value>
        public abstract int Height { get; }

        /// <summary>
        /// Gets the format to use for destinations.
        /// </summary>
        /// <value>The format.</value>
        public abstract ImageFormat Format { get; }

        public abstract DateTimeKind GraphTime { get; }

        public IEnumerable<StreamDestination> Locations => Destinations.Destinations;

        public abstract string GetAltName(GraphSetConfiguration owner, string postfix);

        public string GetAltName(GraphSetConfiguration owner)
        {
            return GetAltName(owner, null);
        }

        public abstract Uri GetLink(GraphSetConfiguration owner, string postfix);

        public Uri GetLink(GraphSetConfiguration owner)
        {
            return GetLink(owner, null);
        }

        public abstract string GetTitle(NamedObject namedObject);
    }
}