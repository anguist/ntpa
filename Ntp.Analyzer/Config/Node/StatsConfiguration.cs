// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Config.Node
{
    public class StatConfiguration : HostSubConfiguration, IJobConfiguration
    {
        internal StatConfiguration(
            string configName,
            ReadingBulkConfiguration bulk,
            DateTimeKind? timeStamp,
            int? frequency,
            bool? initialRun,
            bool? fixedRun
            )
            : base(configName)
        {
            Bulk = bulk;
            this.frequency = frequency ?? 0;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            this.timeStamp = timeStamp;
        }

        private readonly bool? fixedRun;
        private readonly int frequency;
        private readonly bool? initialRun;
        private readonly DateTimeKind? timeStamp;

        public ReadingBulkConfiguration Bulk { get; }

        [NtpaIndex(4)]
        [NtpaSetting(Symbol.KeywordTimeStamp, DateTimeKind.Utc)]
        public DateTimeKind TimeStampConfig => timeStamp ?? DateTimeKind.Utc;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, true)]
        public bool InitialRunConfig => initialRun ?? true;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, true)]
        public bool FixedRunConfig => fixedRun ?? true;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency)]
        public string FrequencyConfig => Bulk != null ? Bulk.Name : frequency.ToString();

        public DateTimeKind TimeStamp => Bulk?.TimeStamp ?? (timeStamp ?? TimeStampConfig);

        public bool InitialRun => Bulk?.InitialRun ?? InitialRunConfig;

        public bool FixedRun => Bulk?.FixedRun ?? FixedRunConfig;

        public int Frequency => Bulk?.Frequency ?? frequency;
    }
}