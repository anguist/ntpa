﻿using System;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class DriftStatConfiguration : StatConfiguration
    {
        internal DriftStatConfiguration(
            string configName,
            string file,
            ReadingBulkConfiguration bulk,
            DateTimeKind? timeStamp,
            int? frequency,
            bool? initialRun,
            bool? fixedRun
            )
            : base(configName, bulk, timeStamp, frequency, initialRun, fixedRun)
        {
            FileName = file;
        }

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordFile)]
        public string FileName { get; }
    }
}