// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Config.Table;
using Ntp.Analyzer.Import;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class HostConfiguration : ConfigurationNode
    {
        internal HostConfiguration(
            string configName,
            int hostId,
            string serverName,
            ServerType? serverType,
            string filePath,
            Uri webPath,
            AboutPageConfiguration aboutPage,
            HostGraphPageConfiguration hostGraphPage,
            StatConfiguration hostIoStats,
            StatConfiguration hostStats,
            MenuConfiguration menu,
            PeerGraphPageConfiguration peerGraphPage,
            StatConfiguration peerStats,
            DriftStatConfiguration driftStats,
            IEnumerable<HostGraphConfiguration> hostGraphs,
            IEnumerable<HostPageConfiguration> hostPages,
            IEnumerable<PeerGraphConfiguration> peerGraphs,
            IEnumerable<PeerPageConfiguration> peerPages,
            IEnumerable<PeerSummaryPageConfiguration> peerSummaryPages,
            IEnumerable<TrafficGraphConfiguration> trafficGraphs
            )
            : base(configName)
        {
            HostId = hostId;
            ConfigFilePath = filePath;
            AboutPage = aboutPage;
            HostGraphPage = hostGraphPage;
            HostIoStats = hostIoStats;
            HostStats = hostStats;
            Menu = menu;
            PeerGraphPage = peerGraphPage;
            PeerStats = peerStats;
            DriftStats = driftStats;
            this.serverName = serverName;
            this.serverType = serverType;
            this.hostPages = new List<HostPageConfiguration>(hostPages);
            this.hostGraphs = new List<HostGraphConfiguration>(hostGraphs);
            this.peerSummaryPages = new List<PeerSummaryPageConfiguration>(peerSummaryPages);
            this.peerPages = new List<PeerPageConfiguration>(peerPages);
            this.peerGraphs = new List<PeerGraphConfiguration>(peerGraphs);
            TrafficGraphs = new List<TrafficGraphConfiguration>(trafficGraphs);
            this.webPath = webPath;
            webPathString = webPath?.OriginalString;
        }

        private readonly List<HostGraphConfiguration> hostGraphs;
        private readonly List<HostPageConfiguration> hostPages;
        private readonly List<PeerGraphConfiguration> peerGraphs;
        private readonly List<PeerPageConfiguration> peerPages;
        private readonly List<PeerSummaryPageConfiguration> peerSummaryPages;
        private readonly string serverName;
        private readonly ServerType? serverType;
        private readonly Uri webPath;
        private readonly string webPathString;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordHostId)]
        public int HostId { get; }

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordHostAddress, "localhost")]
        public string ServerName => serverName ?? "localhost";

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordHostType, ServerType.Ntpq)]
        public ServerType ServerType => serverType ?? ServerType.Ntpq;

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordFilePath, Layout = Layout.Quoted)]
        public string ConfigFilePath { get; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        /// <remarks>HostConfiguration is a file path root.</remarks>
        /// <value>The file path.</value>
        public override string FilePath => ConfigFilePath;

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordWebPath, "/")]
        public string ConfigWebPath => webPathString ?? "/";

        public Uri WebPath => webPath ?? new Uri("/", UriKind.Relative);

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordHostStats)]
        public StatConfiguration HostStats { get; }

        [NtpaIndex(21)]
        [NtpaSetting(Symbol.KeywordHostIoStats)]
        public StatConfiguration HostIoStats { get; }

        [NtpaIndex(22)]
        [NtpaSetting(Symbol.KeywordPeerStats)]
        public StatConfiguration PeerStats { get; }

        [NtpaIndex(23)]
        [NtpaSetting(Symbol.KeywordDriftStats)]
        public DriftStatConfiguration DriftStats { get; }

        [NtpaIndex(30)]
        [NtpaSetting(Symbol.KeywordMenu)]
        public MenuConfiguration Menu { get; }

        [NtpaIndex(40)]
        [NtpaSetting(Symbol.KeywordAboutPage)]
        public AboutPageConfiguration AboutPage { get; }

        [NtpaIndex(50)]
        [NtpaSettingsCollection(Symbol.KeywordHostPage)]
        public IEnumerable<HostPageConfiguration> HostPages => hostPages;

        [NtpaIndex(51)]
        [NtpaSetting(Symbol.KeywordHostGraphPage)]
        public HostGraphPageConfiguration HostGraphPage { get; }

        [NtpaIndex(60)]
        [NtpaSettingsCollection(Symbol.KeywordPeerPage)]
        public IEnumerable<PeerPageConfiguration> PeerPages => peerPages;

        [NtpaIndex(61)]
        [NtpaSettingsCollection(Symbol.KeywordPeerSummaryPage)]
        public IEnumerable<PeerSummaryPageConfiguration> PeerSummaryPages => peerSummaryPages;

        [NtpaIndex(62)]
        [NtpaSetting(Symbol.KeywordPeerGraphPage)]
        public PeerGraphPageConfiguration PeerGraphPage { get; }

        [NtpaIndex(70)]
        [NtpaSettingsCollection(Symbol.KeywordHostGraph)]
        public IEnumerable<HostGraphConfiguration> HostGraphs => hostGraphs;

        [NtpaIndex(72)]
        [NtpaSettingsCollection(Symbol.KeywordTrafficGraph)]
        public List<TrafficGraphConfiguration> TrafficGraphs { get; }

        [NtpaIndex(71)]
        [NtpaSettingsCollection(Symbol.KeywordPeerGraph)]
        public IEnumerable<PeerGraphConfiguration> PeerGraphs => peerGraphs;
    }
}