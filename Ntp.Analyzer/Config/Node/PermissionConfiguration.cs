﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class PermissionConfiguration : ConfigurationNode
    {
        internal PermissionConfiguration(
            string configName,
            uint? appUserId,
            string appUserName,
            uint? userId,
            string userName,
            uint? groupId,
            string groupName,
            uint? fileMode
            ) : base(configName)
        {
            AppUserId = appUserId;
            UserId = userId;
            GroupId = groupId;
            AppUserName = appUserName ?? AppUserId?.ToString();
            UserName = userName ?? UserId?.ToString();
            GroupName = groupName ?? GroupId?.ToString();
            this.fileMode = fileMode;
        }

        private readonly uint? fileMode;

        public uint? AppUserId { get; }

        public uint? UserId { get; }

        public uint? GroupId { get; }

        /// <summary>
        /// The user which is used in execution context.
        /// </summary>
        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordExecUser)]
        public string AppUserName { get; }

        /// <summary>
        /// The owner of created files.
        /// </summary>
        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordUser)]
        public string UserName { get; }

        /// <summary>
        /// The group owner of created files.
        /// </summary>
        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordGroup)]
        public string GroupName { get; }

        /// <summary>
        /// The file mode to apply for created files.
        /// </summary>
        [NtpaIndex(4)]
        [NtpaSetting(Symbol.KeywordMode, 644U)]
        public uint FileMode => fileMode ?? 644;
    }
}