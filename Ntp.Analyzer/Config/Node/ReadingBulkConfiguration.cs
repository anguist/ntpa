// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class ReadingBulkConfiguration : ConfigurationNode, IJobConfiguration
    {
        internal ReadingBulkConfiguration(
            string configName,
            string name,
            int? frequency,
            bool? initialRun,
            bool? fixedRun,
            DateTimeKind? timeStamp)
            : base(configName)
        {
            Name = name;
            Frequency = frequency ?? 0;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            this.timeStamp = timeStamp;
        }

        private readonly bool? fixedRun;

        private readonly bool? initialRun;
        private readonly DateTimeKind? timeStamp;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordName)]
        public string Name { get; }

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordTimeStamp, Symbol.KeywordTimeStampUtc)]
        public DateTimeKind TimeStamp => timeStamp ?? DateTimeKind.Utc;

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordFrequency)]
        public int Frequency { get; }

        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordFixedRun, true)]
        public bool FixedRun => fixedRun ?? true;

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordInitialRun, false)]
        public bool InitialRun => initialRun ?? false;
    }
}