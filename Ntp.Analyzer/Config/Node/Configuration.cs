// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class Configuration : ConfigurationNode
    {
        internal Configuration(
            string configName,
            DatabaseConfiguration database,
            HeartbeatConfiguration heartbeat,
            PermissionConfiguration permission,
            ClusterConfiguration cluster,
            NotifyConfiguration notify,
            IEnumerable<ReadingBulkConfiguration> bulks,
            IEnumerable<ListenerConfiguration> monitors,
            IEnumerable<LogConfiguration> logs,
            IEnumerable<HostConfiguration> servers)
            : base(configName)
        {
            Database = database;
            Heartbeat = heartbeat;
            Permission = permission;
            Cluster = cluster;
            Notify = notify;
            this.bulks = new List<ReadingBulkConfiguration>(bulks);
            this.monitors = new List<ListenerConfiguration>(monitors);
            this.logs = new List<LogConfiguration>(logs);
            this.servers = new List<HostConfiguration>(servers);
        }

        private readonly List<ReadingBulkConfiguration> bulks;
        private readonly List<LogConfiguration> logs;
        private readonly List<ListenerConfiguration> monitors;
        private readonly List<HostConfiguration> servers;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordDatabase)]
        public DatabaseConfiguration Database { get; }

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordPermission)]
        public PermissionConfiguration Permission { get; }

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordCluster)]
        public ClusterConfiguration Cluster { get; }

        [NtpaIndex(11)]
        [NtpaSettingsCollection(Symbol.KeywordListener)]
        public IEnumerable<ListenerConfiguration> Monitors => monitors;

        [NtpaIndex(22)]
        [NtpaSettingsCollection(Symbol.KeywordLog)]
        public IEnumerable<LogConfiguration> Log => logs;

        [NtpaIndex(30)]
        [NtpaSetting(Symbol.KeywordNotify)]
        public NotifyConfiguration Notify { get; }

        [NtpaIndex(23)]
        [NtpaSetting(Symbol.KeywordDaemon)]
        public HeartbeatConfiguration Heartbeat { get; }

        [NtpaIndex(40)]
        [NtpaSettingsCollection(Symbol.KeywordReading)]
        public IEnumerable<ReadingBulkConfiguration> Bulks => bulks;

        [NtpaIndex(50)]
        [NtpaSettingsCollection(Symbol.KeywordServer)]
        public IEnumerable<HostConfiguration> Servers => servers;
    }
}