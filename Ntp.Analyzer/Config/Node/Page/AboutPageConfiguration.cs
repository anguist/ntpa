// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Config.Node.Page
{
    public sealed class AboutPageConfiguration : PageConfigurationNode, IJobConfiguration, ILinkable
    {
        internal AboutPageConfiguration(
            string name,
            int? frequency,
            bool? initialRun,
            bool? fixedRun,
            string title,
            PageTheme? theme,
            int? serverId,
            string header,
            string content,
            Uri link,
            DestinationCollection destinations)
            : base(name, destinations, link)
        {
            this.frequency = frequency;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            this.theme = theme;
            ConfigLink = link;
            Title = title;
            ServerId = serverId;
            Header = header;
            Content = content;
        }

        private readonly bool? fixedRun;
        private readonly int? frequency;
        private readonly bool? initialRun;
        private readonly PageTheme? theme;

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordPageTemplate, PageTheme.Bootstrap)]
        public override PageTheme Theme => theme ?? PageTheme.Bootstrap;

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordPageTitle, Layout = Layout.Quoted)]
        public override string Title { get; }

        [NtpaIndex(50)]
        [NtpaSetting(Symbol.KeywordContentTitle, Layout = Layout.Quoted)]
        public string Header { get; }

        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordServerId)]
        public int? ServerId { get; }

        [NtpaIndex(51)]
        [NtpaSetting(Symbol.KeywordContent, Layout = Layout.Block)]
        public string Content { get; }

        [NtpaIndex(13)]
        [NtpaSetting(Symbol.KeywordLink)]
        public Uri ConfigLink { get; }

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency, 0)]
        public int Frequency => frequency ?? 0;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, true)]
        public bool InitialRun => initialRun ?? true;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, false)]
        public bool FixedRun => fixedRun ?? false;
    }
}