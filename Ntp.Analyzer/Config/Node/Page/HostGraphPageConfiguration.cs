// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Process;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Config.Node.Page
{
    public sealed class HostGraphPageConfiguration : GraphPageConfiguration, IJobConfiguration
    {
        internal HostGraphPageConfiguration(
            string configName,
            int? frequency,
            bool? initialRun,
            bool? fixedRun,
            int? linkIndex,
            HostPageConfiguration page,
            DestinationCollection destinations,
            Uri link)
            : base(configName, destinations, link)
        {
            this.frequency = frequency;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            this.linkIndex = linkIndex;
            ConfigLink = link;
            PageConfiguration = page;
        }

        private readonly bool? fixedRun;
        private readonly int? frequency;
        private readonly bool? initialRun;
        private readonly int? linkIndex;

        public override PageTheme Theme => PageTheme.Default;

        public override string Title => string.Empty;

        [NtpaIndex(10)]
        [NtpaReference(Symbol.KeywordGraphPage)]
        public HostPageConfiguration PageConfiguration { get; }

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordLinkIndex, 1)]
        public override int LinkIndex => linkIndex ?? 1;

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordLink)]
        public Uri ConfigLink { get; }

        public override IEnumerable<GraphSetConfiguration> GraphSets => PageConfiguration.Graphs;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency, 0)]
        public int Frequency => frequency ?? 0;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, true)]
        public bool InitialRun => initialRun ?? true;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, false)]
        public bool FixedRun => fixedRun ?? false;

        public string GetFileName(GraphSetConfiguration graphSet, GraphBaseConfiguration graph)
        {
            return graph.GetAltName(graphSet, null).
                Replace(".png", string.Empty).
                Replace('.', '-')
                   + ".html";
        }

        public Uri GetLink(GraphSetConfiguration graphSet, GraphBaseConfiguration graph)
        {
            return WebPath.
                Append(Link, graph.GetAltName(graphSet, null).Replace(".png", string.Empty).Replace('.', '-')).
                AppendExtension(".html");
        }

        internal override void Assemble()
        {
            base.Assemble();
            PageConfiguration.AttachGraphPage(this);
        }
    }
}