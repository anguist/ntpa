// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Globalization;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Analyzer.Config.Table;
using Ntp.Common.Process;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Config.Node.Page
{
    public sealed class PeerPageConfiguration : PageConfigurationNode, ILinkable, IJobConfiguration
    {
        internal PeerPageConfiguration(
            string configName,
            int? frequency,
            bool? initialRun,
            bool? fixedRun,
            DateTimeKind? showUtc,
            PageTheme? theme,
            string title,
            IEnumerable<GraphSetConfiguration> graphs,
            DestinationCollection destinations,
            Uri link)
            : base(configName, destinations, link)
        {
            this.frequency = frequency;
            this.initialRun = initialRun;
            this.fixedRun = fixedRun;
            this.showUtc = showUtc;
            this.theme = theme;
            this.graphs = new List<GraphSetConfiguration>(graphs);
            graphPages = new List<PeerGraphPageConfiguration>();
            Title = title;
            ConfigLink = link;
        }

        private readonly bool? fixedRun;
        private readonly int? frequency;
        private readonly List<PeerGraphPageConfiguration> graphPages;
        private readonly List<GraphSetConfiguration> graphs;
        private readonly bool? initialRun;
        private readonly DateTimeKind? showUtc;
        private readonly PageTheme? theme;

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordPageTemplate, PageTheme.Default)]
        public override PageTheme Theme => theme ?? PageTheme.Default;

        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordPageTime, DateTimeKind.Utc)]
        public DateTimeKind ShowUtc => showUtc ?? DateTimeKind.Utc;

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordPageTitle, Layout = Layout.Quoted)]
        public override string Title { get; }

        /// <summary>
        /// Gets the graphs to be displayed on this page.
        /// </summary>
        /// <value>The peer graphs.</value>
        [NtpaIndex(30)]
        [NtpaSettingsCollection(Symbol.KeywordGraphSet)]
        public IEnumerable<GraphSetConfiguration> Graphs => graphs;

        /// <summary>
        /// Gets the graph pages to be linked from this page.
        /// </summary>
        /// <value>The graph pages.</value>
        public IEnumerable<PeerGraphPageConfiguration> GraphPages => graphPages;

        [NtpaIndex(20)]
        [NtpaSetting(Symbol.KeywordLink)]
        public Uri ConfigLink { get; }

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordFrequency, 240)]
        public int Frequency => frequency ?? 240;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordInitialRun, true)]
        public bool InitialRun => initialRun ?? true;

        [NtpaIndex(3)]
        [NtpaSetting(Symbol.KeywordFixedRun, true)]
        public bool FixedRun => fixedRun ?? true;

        /// <summary>
        /// Gets the link related to specified peer.
        /// </summary>
        /// <returns>The link.</returns>
        /// <param name="peerId">Peer identifier.</param>
        public Uri GetLink(int peerId)
        {
            return WebPath.Append(Link, peerId.ToString(CultureInfo.InvariantCulture)).AppendExtension(".html");
        }

        internal void AttachGraphPage(PeerGraphPageConfiguration graphPage)
        {
            graphPages.Add(graphPage);
        }
    }
}