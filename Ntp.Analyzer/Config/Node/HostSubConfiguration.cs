// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Analyzer.Import;

namespace Ntp.Analyzer.Config.Node
{
    /// <summary>
    /// Configuration class for subsections of a <see cref="HostConfiguration" />.
    /// </summary>
    public abstract class HostSubConfiguration : ConfigurationNode
    {
        protected HostSubConfiguration(string name)
            : base(name)
        {
        }

        protected HostConfiguration Server => Parent as HostConfiguration;

        /// <summary>
        /// Gets the name of the server in the <see cref="HostSubConfiguration" />.
        /// </summary>
        /// <value>The name of the server.</value>
        public string ServerName => Server.ServerName;

        /// <summary>
        /// Gets the type of the server in the <see cref="HostSubConfiguration" />.
        /// </summary>
        /// <value>The type of the server.</value>
        public ServerType ServerType => Server.ServerType;

        /// <summary>
        /// Gets the host identifier in the <see cref="HostSubConfiguration" />.
        /// </summary>
        /// <value>The host identifier.</value>
        public int HostId => Server.HostId;

        /// <summary>
        /// Gets the file path in the <see cref="HostSubConfiguration" />.
        /// </summary>
        /// <value>The web path.</value>
        public override string FilePath => Server.FilePath;

        /// <summary>
        /// Gets the web path in the <see cref="HostSubConfiguration" />.
        /// </summary>
        /// <value>The web path.</value>
        public Uri WebPath => Server.WebPath;
    }
}