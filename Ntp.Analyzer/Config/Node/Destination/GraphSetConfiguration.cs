// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Config.Table;

namespace Ntp.Analyzer.Config.Node.Destination
{
    public sealed class GraphSetConfiguration : ConfigurationNode
    {
        internal GraphSetConfiguration(
            string name,
            string title,
            int? linkIndex,
            IEnumerable<GraphBaseConfiguration> graphs)
            : base(name)
        {
            this.title = title;
            this.linkIndex = linkIndex;
            this.graphs = new List<GraphBaseConfiguration>(graphs);
        }

        private readonly List<GraphBaseConfiguration> graphs;
        private readonly int? linkIndex;
        private readonly string title;

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordTitle, "Graphs", Layout = Layout.Quoted)]
        public string Title => title ?? "Graphs";

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordLinkIndex, 1)]
        public int LinkIndex => linkIndex ?? 1;

        [NtpaIndex(10)]
        [NtpaReferenceCollection(Symbol.KeywordGraph)]
        public IEnumerable<GraphBaseConfiguration> Graphs => graphs;
    }
}