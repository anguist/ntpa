// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Config.Attribute;
using Ntp.Analyzer.Config.Table;
using Ntp.Data;

namespace Ntp.Analyzer.Config.Node
{
    public sealed class DatabaseConfiguration : ConfigurationNode, IDatabaseConfiguration
    {
        internal DatabaseConfiguration(
            string configName,
            SqlDatabaseProvider provider,
            string host,
            int? port,
            string name,
            string user,
            string pass,
            string connString,
            string certFile,
            string certPass,
            int? connectionTimeout,
            int? protocol,
            bool? ssl,
            bool? initialize,
            bool? upgrade,
            bool? import
            )
            : base(configName)
        {
            Provider = provider;
            Host = host;
            Port = port;
            Name = name;
            User = user;
            Pass = pass;
            ConnectionString = connString;
            CertificateFile = certFile;
            CertificatePassword = certPass;
            Protocol = protocol;
            ConnectionTimeout = connectionTimeout;
            this.ssl = ssl;
            this.initialize = initialize;
            this.upgrade = upgrade;
            this.import = import;
        }

        private readonly bool? import;

        private readonly bool? initialize;
        private readonly bool? ssl;
        private readonly bool? upgrade;

        [NtpaIndex(50)]
        [NtpaSetting(Symbol.KeywordCreate, false)]
        public bool Initialize => initialize ?? false;

        [NtpaIndex(51)]
        [NtpaSetting(Symbol.KeywordUpgrade, false)]
        public bool Upgrade => upgrade ?? false;

        [NtpaIndex(52)]
        [NtpaSetting(Symbol.KeywordImport, true)]
        public bool Import => import ?? true;

        [NtpaIndex(2)]
        [NtpaSetting(Symbol.KeywordConString)]
        public string ConnectionString { get; }

        [NtpaIndex(15)]
        [NtpaSetting(Symbol.KeywordEnableSsl, false)]
        public bool EnableSsl => ssl ?? false;

        /*
        new Keyword("SslMode", Symbol.KeywordSslMode),
        new Keyword("Required", Symbol.KeywordRequired),
        new Keyword("Preferred", Symbol.KeywordPreferred),
        */

        [NtpaIndex(17)]
        [NtpaSetting(Symbol.KeywordProtocol)]
        public int? Protocol { get; }

        [NtpaIndex(30)]
        [NtpaSetting(Symbol.KeywordCertFile)]
        public string CertificateFile { get; }


        [NtpaIndex(31)]
        [NtpaSetting(Symbol.KeywordCertPass)]
        public string CertificatePassword { get; }

        /// <summary>
        /// Get the time to wait (in seconds) while trying to establish a connection
        /// before terminating the attempt and generating an error.
        /// </summary>
        [NtpaIndex(40)]
        [NtpaSetting(Symbol.KeywordConnectionTimeout)]
        public int? ConnectionTimeout { get; }

        [NtpaIndex(1)]
        [NtpaSetting(Symbol.KeywordDatabaseProvider)]
        public SqlDatabaseProvider Provider { get; }

        [NtpaIndex(10)]
        [NtpaSetting(Symbol.KeywordHost)]
        public string Host { get; }

        [NtpaIndex(11)]
        [NtpaSetting(Symbol.KeywordPort)]
        public int? Port { get; }

        [NtpaIndex(12)]
        [NtpaSetting(Symbol.KeywordName)]
        public string Name { get; }

        [NtpaIndex(13)]
        [NtpaSetting(Symbol.KeywordUser)]
        public string User { get; }

        [NtpaIndex(14)]
        [NtpaSetting(Symbol.KeywordPass, Layout = Layout.Quoted)]
        public string Pass { get; }
    }
}