// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Text;

namespace Ntp.Analyzer.Render
{
    public abstract class BootstrapPageRender : HtmlObjectRender
    {
        protected BootstrapPageRender(Uri webPath, string title)
            : base(webPath)
        {
            Title = title;
        }

        protected virtual string Title { get; }

        public override string Render()
        {
            var builder = new StringBuilder();

            builder.AppendLine("<body>");
            builder.Append(RenderPageBody());

            return builder.ToString();
        }

        public override string RenderFooter()
        {
            var builder = new StringBuilder();

            builder.Append(RenderPageFooter());

            builder.AppendLine(this.AppendWithWebpath(
                @"  <script src=""{0}{1}""></script>",
                "js/jquery.js"));

            builder.AppendLine(this.AppendWithWebpath(
                @"  <script src=""{0}{1}""></script>",
                "js/bootstrap.min.js"));

            builder.AppendLine("</body>");
            builder.AppendLine("</html>");

            return builder.ToString();
        }

        public override string RenderHead()
        {
            var builder = new StringBuilder();

            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine(@"<html lang=""en""><head>");
            builder.AppendLine(@"  <meta charset=""UTF-8"">");
            builder.AppendLine(@"  <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">");

            builder.Append("  <title>");
            builder.Append(Title);
            builder.AppendLine("</title>");

            builder.AppendLine(this.AppendWithWebpath(
                @"  <link href=""{0}{1}"" rel=""stylesheet"" media=""screen"">",
                "css/bootstrap.min.css"));

            builder.Append(RenderPageHead());

            builder.AppendLine("</head>");

            return builder.ToString();
        }

        protected abstract string RenderPageBody();

        protected abstract string RenderPageFooter();

        protected abstract string RenderPageHead();
    }
}