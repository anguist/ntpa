// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Text;

namespace Ntp.Analyzer.Render.Peer
{
    public sealed class DefaultPeerPageRender : HtmlObjectRender
    {
        public DefaultPeerPageRender(Uri webPath, string name)
            : base(webPath)
        {
            this.name = name;
        }

        private readonly string name;

        public override string Render()
        {
            return "<body>";
        }

        public override string RenderFooter()
        {
            var builder = new StringBuilder();

            builder.AppendLine("</body>");
            builder.AppendLine("</html>");

            return builder.ToString();
        }

        public override string RenderHead()
        {
            var builder = new StringBuilder();

            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine("<html><head>");
            builder.AppendLine(@"<meta charset=""UTF-8"">");
            builder.AppendLine(@"<style type=""text/css"">");
            builder.AppendLine("body {font-family: Helvetica; font-size: 14px; line-height:100%; }");
            builder.AppendLine("h2 { font-family: monospace; display: inline-block; }");
            builder.AppendLine("h4 { font-family: monospace; display: inline-block; }");
            builder.AppendLine("a:link { color:#33348e; text-decoration: none; }");
            builder.AppendLine("a:visited { color:#33348e; text-decoration: none; }");
            builder.AppendLine("a:hover { color:#33348e; text-decoration: underline;; }");
            builder.AppendLine("a:active { color:#7476b4; text-decoration: underline; }");
            builder.AppendLine(
                "table { border-collapse:collapse; border-spacing: 0px; width: 975px; table-layout: fixed; }");
            builder.AppendLine(
                "td { padding: 0px; padding-right: 0px; padding-left: 0px; padding-top: 2px; padding-bottom: 3px; }");
            builder.AppendLine("col.fst { width: 150px; }");
            builder.AppendLine("col.sec { width: 275px; }");
            builder.AppendLine("col.thd { width: 125px; }");
            builder.AppendLine("col.frt { width: 425px; }");
            builder.AppendLine("</style>");
            builder.AppendLine("<title>");
            builder.AppendLine(name);
            builder.AppendLine("</title>");
            builder.AppendLine("</head>");

            return builder.ToString();
        }
    }
}