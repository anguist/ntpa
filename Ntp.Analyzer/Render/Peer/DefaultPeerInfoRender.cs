// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Globalization;
using System.Text;
using Ntp.Analyzer.Objects;

namespace Ntp.Analyzer.Render.Peer
{
    public sealed class DefaultPeerInfoRender : HtmlObjectRender
    {
        public DefaultPeerInfoRender(Uri webPath, TimeServer server, string name)
            : base(webPath)
        {
            this.server = server;
            this.name = name;
        }

        private const string Na = "&nbsp;";
        private readonly string name;
        private readonly TimeServer server;

        public override string Render()
        {
            var builder = new StringBuilder();

            builder.AppendLine("<table>");
            builder.AppendLine(@"<col class=""fst""><col class=""sec""><col class=""thd""><col class=""frt"">");
            builder.AppendLine("<tbody>");

            builder.AppendLine("<tr>");
            builder.Append(@"  <td>Server location</td><td colspan=""3"">");
            builder.Append(server != null ? server.Location : Na);
            builder.AppendLine("</td>");
            builder.AppendLine("</tr>");

            builder.AppendLine("<tr>");
            builder.Append(@"  <td>Synchronization</td><td colspan=""3"">");
            builder.Append(server != null ? server.Server : Na);
            builder.AppendLine("</td>");
            builder.AppendLine("</tr>");

            builder.AppendLine("<tr>");
            builder.Append("  <td>IP address</td><td>");
            builder.Append(server?.Address?.ToString() ?? string.Empty);
            builder.AppendLine("</td>");
            builder.Append("  <td>Access policy</td><td>");
            builder.Append(server != null ? GetServerAccess() : Na);
            builder.AppendLine("</td>");
            builder.AppendLine("</tr>");

            builder.AppendLine("<tr>");
            builder.Append("  <td>IPv6 address</td><td>");
            builder.Append(server != null ? server.V6Address ?? "N/A" : Na);
            builder.AppendLine("</td>");
            builder.Append("  <td>Service area</td><td>");
            builder.Append(server != null ? GetServiceArea() : Na);
            builder.AppendLine("</td>");
            builder.AppendLine("</tr>");

            builder.AppendLine("<tr>");
            builder.Append(@"  <td>Pool member info</td><td>");
            builder.Append(server != null ? GetPoolMemberLink() : Na);
            builder.AppendLine("</td>");
            builder.Append("  <td>Provider page</td><td>");
            builder.Append(server != null ? GetProviderLink() : Na);
            builder.AppendLine("</td>");
            builder.AppendLine("</tr>");

            builder.AppendLine("<tr>");
            builder.Append("<td>Description updated</td><td>");
            builder.Append(server?.Updated.ToLongDateString() ?? Na);
            builder.AppendLine("</td>");
            if (server != null && server.Id <= 10000)
            {
                builder.Append(
                    @"  <td>Additional info</td><td><a href=""http://support.ntp.org/bin/view/Servers/PublicTimeServer");
                builder.Append(server.Id.ToString(CultureInfo.InvariantCulture).PadLeft(6, '0'));
                builder.AppendLine(@""">support.ntp.org</a></td>");
            }
            else
            {
                builder.Append($@"  <td>Additional info</td><td>{Na}</td>");
            }
            builder.AppendLine("</tr>");

            builder.AppendLine("</tbody>");
            builder.AppendLine("</table>");

            return builder.ToString();
        }

        public override string RenderFooter()
        {
            return string.Empty;
        }

        public override string RenderHead()
        {
            var builder = new StringBuilder();

            builder.Append("<h2>");
            builder.Append(server != null ? server.Name : name);
            builder.Append("</h2>&nbsp;&nbsp;&nbsp;&nbsp;<h4>Stratum ");
            builder.Append(server != null ? GetStratumText() : Na);
            builder.AppendLine(" server</h4>");

            return builder.ToString();
        }

        #region Translators

        private string GetStratumText()
        {
            switch (server.Stratum)
            {
                case 1:
                    return "one";
                case 2:
                    return "two";
                case 3:
                    return "three";
                case 4:
                    return "four";
                case 5:
                    return "five";
                case 6:
                    return "six";
                case 7:
                    return "seven";
                default:
                    return "Unknown";
            }
        }

        private string GetServerAccess()
        {
            switch (server.AccessPolicy)
            {
                case "OpenAccess":
                    return "Open access";
                default:
                    return "Unknown";
            }
        }

        private string GetServiceArea()
        {
            string description = server.ServiceArea.Trim();

            if (description == string.Empty)
                return string.Empty;

            string firstLetter = description.Substring(0, 1);
            string rest = description.Substring(1);

            return firstLetter.ToUpper() + rest;
        }

        private string GetPoolMemberLink()
        {
            if (!server.IsPoolMember)
                return "Server is not a pool member";

            var builder = new StringBuilder();
            builder.Append(@"<a href=""http://www.pool.ntp.org/scores/");
            builder.Append(server.Address);
            builder.Append(@""">pool.ntp.org</a>");
            return builder.ToString();
        }

        private string GetProviderLink()
        {
            if (server.ProviderPage == null)
                return "Not found";

            var builder = new StringBuilder();
            builder.Append(@"<a href=""");
            builder.Append(server.ProviderUrl);
            builder.Append(@""">");
            builder.Append(server.ProviderPage);
            builder.Append("</a>");
            return builder.ToString();
        }

        #endregion
    }
}