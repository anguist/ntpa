// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Text;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Objects;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render.Peer
{
    public sealed class DefaultPeerGraphRender : HtmlObjectRender
    {
        public DefaultPeerGraphRender(
            Uri webPath,
            string peerName,
            TimeServer server,
            IEnumerable<GraphSetConfiguration> graphs)
            : base(webPath)
        {
            this.peerName = peerName;
            this.server = server;
            this.graphs = new List<GraphSetConfiguration>(graphs);
        }

        private readonly List<GraphSetConfiguration> graphs;

        private readonly string peerName;
        private readonly TimeServer server;

        public override string Render()
        {
            var builder = new StringBuilder();

            foreach (var graphSet in graphs)
            {
                foreach (var graph in graphSet.Graphs)
                {
                    string name = server != null ? server.Name : peerName;
                    builder.Append(@"<br><img src=""");
                    builder.Append(graph.GetLink(graphSet, name).ToHtmlString());
                    builder.Append(@""" alt=""");
                    builder.Append(graph.GetAltName(graphSet, name));
                    builder.AppendLine(@"""><br>");
                }
            }

            return builder.ToString();
        }

        public override string RenderFooter()
        {
            return string.Empty;
        }

        public override string RenderHead()
        {
            return string.Empty;
        }
    }
}