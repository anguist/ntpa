// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Ntp.Analyzer.Render
{
    public static class HtmlExtension
    {
        public static string AppendWithWebpath(this IHtmlObject htmlObject, string line, string link)
        {
            if (link.StartsWith("http") || link.StartsWith("https") || link.StartsWith("mailto"))
                return string.Format(line, string.Empty, link);

            return string.Format(line, htmlObject.WebPath, link);
        }

        public static string AppendWithWebpath(this IHtmlObject htmlObject, string line, string link, object arg0)
        {
            if (link.StartsWith("http") || link.StartsWith("https") || link.StartsWith("mailto"))
                return string.Format(line, string.Empty, link, arg0);

            return string.Format(line, htmlObject.WebPath, link, arg0);
        }

        public static string AppendWithWebpath(this IHtmlObject htmlObject, string line, string link, object arg0,
            object arg1)
        {
            if (link.StartsWith("http") || link.StartsWith("https") || link.StartsWith("mailto"))
                return string.Format(line, string.Empty, link, arg0, arg1);

            return string.Format(line, htmlObject.WebPath, link, arg0, arg1);
        }
    }
}