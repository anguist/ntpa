// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Text;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Objects;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render.Host
{
    public sealed class HostLineRender : HtmlObjectRender
    {
        public HostLineRender(StatusLine statusLine, PeerPageConfiguration peerPages)
            : base(peerPages?.WebPath)
        {
            this.statusLine = statusLine;
            this.peerPages = peerPages;
        }

        private readonly PeerPageConfiguration peerPages;
        private readonly StatusLine statusLine;

        private AssociationEntry Peer => statusLine.Peer;

        private TimeServer Server => statusLine.PeerActivity.Peer.Server;

        private string PeerName
        {
            get
            {
                string name = statusLine.PeerActivity.Peer.Name;
                return name.Substring(0, name.Length > 18 ? 18 : name.Length);
            }
        }

        private string Country => Server != null ? Server.Country : string.Empty;

        private string Location => string.IsNullOrEmpty(Server?.DisplayLocation)
            ? "Unknown"
            : Server.DisplayLocation;

        private int PageId => statusLine.PeerActivity.Id;

        public override string Render()
        {
            var builder = new StringBuilder();

            builder.Append($"<td>{Peer.TallyChar}</td>");
            builder.Append($"<td>{PeerName}</td>");
            builder.Append($@"<td class=""aright"">{Peer.Stratus}</td>");
            builder.Append($@"<td class=""aright"">{Peer.LastPoll}</td>");
            builder.Append($@"<td class=""aright2"">{Peer.Poll}</td>");
            builder.Append($@"<td class=""aright"">{Peer.Reach}</td>");
            builder.Append($@"<td class=""aright"">{Peer.Delay.ToString("0.000")}</td>");
            builder.Append($@"<td class=""aright"">{Peer.Offset.ToString("0.000")}</td>");
            builder.Append($@"<td class=""aright"">{Peer.Jitter.ToString("0.000")}</td>");
            builder.Append($@"<td class=""aright"">{Country}</td>");

            if (peerPages != null)
            {
                builder.Append(@"<td><a href=""");
                builder.Append(peerPages.GetLink(PageId).ToHtmlString());
                builder.Append(@""">");
                builder.Append(Location);
                builder.AppendLine("</a></td>");
            }
            else
            {
                builder.Append(@"<td>");
                builder.Append(Location);
                builder.AppendLine("</td>");
            }

            return builder.ToString();
        }

        public override string RenderFooter()
        {
            return "</tr>";
        }

        public override string RenderHead()
        {
            return "<tr>";
        }
    }
}