// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render.Host
{
    public sealed class BootstrapHostGraphRender : HtmlObjectRender
    {
        public BootstrapHostGraphRender(
            Uri webPath,
            HostPageConfiguration page,
            IEnumerable<GraphSetConfiguration> graphs)
            : base(webPath)
        {
            this.page = page;
            this.graphs = new List<GraphSetConfiguration>(graphs);
        }

        private readonly List<GraphSetConfiguration> graphs;
        private readonly HostPageConfiguration page;

        public override string Render()
        {
            var builder = new StringBuilder();
            int count = 1;

            foreach (var graphSet in graphs)
            {
                //string open = (count == 1) ? " in" : String.Empty;
                string open = string.Empty;

                builder.AppendLine(@"      <div class=""panel panel-default"">");
                builder.AppendLine(@"        <div class=""panel-heading"">");
                builder.AppendLine(@"          <h4 class=""panel-title"">");

                // Include panel counter
                builder.Append(@"            <a class=""accordion-toggle"" data-toggle=""collapse"" ");
                builder.Append(@"data-parent=""#accordion"" href=""#collapse");
                builder.Append(count);
                builder.AppendLine(@""">");

                builder.AppendLine($@"              {graphSet.Title}");
                builder.AppendLine(@"            </a>");
                builder.AppendLine(@"          </h4>");
                builder.AppendLine(@"        </div>");

                builder.Append(@"        <div id=""collapse");
                builder.Append(count);
                builder.Append($@""" class=""panel-collapse collapse{open}"">");
                builder.AppendLine();
                builder.AppendLine(@"          <div class=""panel-body"">");

                foreach (var graph in graphSet.Graphs)
                {
                    builder.Append(@"            <div class=""container theme-graph2"">");

                    if (page.GraphPages.Count() != 0)
                    {
                        builder.Append(@"<a href=""");
                        builder.Append(page.GraphPages.First().GetLink(graphSet, graph).ToHtmlString());
                        builder.Append(@""">");
                    }

                    builder.Append(@"<img class=""img-responsive"" src=""");
                    builder.Append(graph.GetLink(graphSet).ToHtmlString());
                    builder.Append(@""" alt=""");
                    builder.Append(graph.GetAltName(graphSet));
                    builder.Append(@""">");

                    if (page.GraphPages.Count() != 0)
                    {
                        builder.Append(@"</a>");
                    }

                    builder.AppendLine(@"</div>");
                }

                builder.AppendLine(@"          </div>");
                builder.AppendLine(@"        </div>");
                builder.AppendLine(@"      </div>");

                count++;
            }

            return builder.ToString();
        }

        public override string RenderFooter()
        {
            var builder = new StringBuilder();

            builder.AppendLine(@"    </div>");
            builder.AppendLine(@"  </div>");

            return builder.ToString();
        }

        public override string RenderHead()
        {
            var builder = new StringBuilder();

            builder.AppendLine(@"  <div class=""container theme-graph"">");
            builder.AppendLine(@"    <div class=""panel-group"" id=""accordion"">");

            return builder.ToString();
        }
    }
}