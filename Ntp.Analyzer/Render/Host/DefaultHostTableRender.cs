// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Text;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Import;

namespace Ntp.Analyzer.Render.Host
{
    public sealed class DefaultHostTableRender : HtmlObjectRender
    {
        public DefaultHostTableRender(HostPageConfiguration page)
            : base(page.WebPath)
        {
            columns = new List<string>
            {
                "&nbsp;",
                "Name",
                "st",
                "when",
                "poll",
                page.ServerType == ServerType.Ntpctl ? "trust" : "reach",
                "&nbsp;delay",
                "offset",
                "jitter",
                "iso&nbsp;",
                "location"
            }.ToArray();
        }

        private readonly string[] columns;

        public override string Render()
        {
            return string.Empty;
        }

        public override string RenderFooter()
        {
            var builder = new StringBuilder();

            builder.Append(MakeSpacer());
            builder.Append("<tr>");

            for (int index = 0; index < columns.Length; index++)
            {
                builder.Append(@"<td class=""but"">&nbsp;</td>");
            }

            builder.AppendLine("</tr>");
            builder.AppendLine("</table>");

            return builder.ToString();
        }

        public override string RenderHead()
        {
            var builder = new StringBuilder();

            builder.Append("<table><tr>");

            foreach (string column in columns)
            {
                builder.Append("<th>");
                builder.Append(column);
                builder.Append("</th>");
            }

            builder.AppendLine("</tr>");
            builder.Append(MakeSpacer());

            return builder.ToString();
        }

        private string MakeSpacer()
        {
            var builder = new StringBuilder();

            builder.Append("<tr>");

            for (int index = 0; index < columns.Length; index++)
            {
                builder.Append(@"<td class=""spc"">&nbsp;</td>");
            }

            builder.AppendLine("</tr>");

            return builder.ToString();
        }
    }
}