// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Text;

namespace Ntp.Analyzer.Render.Host
{
    public sealed class BootstrapHostPageRender : BootstrapPageRender
    {
        public BootstrapHostPageRender(Uri webPath, string title, int refresh)
            : base(webPath, title)
        {
            this.refresh = refresh;
        }

        private readonly int refresh;

        protected override string RenderPageBody()
        {
            return string.Empty;
        }

        protected override string RenderPageFooter()
        {
            return string.Empty;
        }

        protected override string RenderPageHead()
        {
            var builder = new StringBuilder();

            builder.AppendLine(this.AppendWithWebpath(
                @"  <link href=""{0}{1}"" rel=""stylesheet"" media=""screen"">",
                "css/host.css"));

            builder.AppendLine($@"  <meta http-equiv=""refresh"" content=""{refresh}"">");

            return builder.ToString();
        }
    }
}