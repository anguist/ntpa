// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Linq;
using System.Text;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render.Host
{
    public sealed class DefaultHostPageRender : HtmlObjectRender
    {
        public DefaultHostPageRender(HostPageConfiguration config, string ip, int refresh)
            : base(config.WebPath)
        {
            this.config = config;
            this.ip = ip;
            this.refresh = refresh;
        }

        private readonly HostPageConfiguration config;
        private readonly string ip;
        private readonly int refresh;

        public override string Render()
        {
            return "<body>";
        }

        public override string RenderFooter()
        {
            var builder = new StringBuilder();
            builder.AppendLine(string.Empty);

            DateTime now = DateTime.Now;

            builder.AppendLine("<br>");

            if (config.ShowUtc == DateTimeKind.Utc)
            {
                builder.Append("Time is ");
                builder.Append(now.ToLongDateString() + " ");
                builder.Append(TimeZoneInfo.Utc.StandardName);
            }
            else
            {
                builder.Append("Local time is ");
                builder.Append(now.ToLongDateString() + " ");
                builder.Append(TimeZoneInfo.Local.IsDaylightSavingTime(now)
                    ? TimeZoneInfo.Local.DaylightName
                    : TimeZoneInfo.Local.StandardName);
            }
            builder.Append(" " + now.ToShortTimeString() + ". ");

            builder.AppendLine("<br><br>");
            if (config.PoolMember)
            {
                string poolLink = "http://www.pool.ntp.org/scores/" + ip;
                string poolText = $@"<a href=""{poolLink}"">pool.ntp.org</a>";
                builder.Append("Server is pool member on ");
                builder.Append(poolText);
            }

            if (config.PeerSummaries.Pages.Count() != 0)
            {
                builder.AppendLine("<br><br>");
                builder.Append("Peer graphs are available with ");
                builder.Append(@"<a href=""");
                builder.Append(config.PeerSummaries.Pages.First().Link.ToHtmlString());
                builder.Append(@""">");
                builder.Append(config.PeerSummaries.Pages.First().Title);
                builder.Append(@"</a> and ");
                builder.Append(@"<a href=""");
                builder.Append(config.PeerSummaries.Pages.ToList()[1].Link.ToHtmlString());
                builder.Append(@""">");
                builder.Append(config.PeerSummaries.Pages.ToList()[1].Title);
                builder.Append(@"</a>.");
            }

            builder.AppendLine("<br><br>");
            builder.AppendLine("Page refreshes every 5 minutes.");

            builder.AppendLine("</body>");
            builder.AppendLine("</html>");

            return builder.ToString();
        }

        public override string RenderHead()
        {
            var builder = new StringBuilder();

            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine("<html><head>");
            builder.AppendLine(@"<meta charset=""UTF-8"">");
            builder.AppendLine(@"<style type=""text/css"" media=""screen"">");
            builder.AppendLine("body {font-family: monospace; line-height:100%; }");
            builder.AppendLine("table { border-collapse:collapse; border-spacing: 0; width: 940px; }");
            builder.AppendLine(
                "th { text-align: left; font-weight: bold; border-bottom: 2px solid; padding: 0px; padding-top: 5px; padding-bottom: 3px;}");
            builder.AppendLine(
                "td { padding: 0px; padding-right: 7px; padding-left: 2px; padding-top: 2px; padding-bottom: 2px; }");
            builder.AppendLine("td.aright { text-align: right; padding-right: 17px;}");
            builder.AppendLine("td.spc { padding: 0px; line-height: 5px; }");
            builder.AppendLine("td.but { border-top: 2px solid; padding: 0px; padding-top: 2px; line-height: 7px; }");
            builder.AppendLine("a:link { color:#33348e; text-decoration: none; }");
            builder.AppendLine("a:visited { color:#33348e; text-decoration: none; }");
            builder.AppendLine("a:hover { color:#33348e; text-decoration: underline;; }");
            builder.AppendLine("a:active { color:#7476b4; text-decoration: underline; }");
            builder.AppendLine("</style>");
            builder.AppendLine("<title>");
            builder.AppendLine(config.ServerName);
            builder.AppendLine("</title>");
            builder.AppendLine($@"<meta http-equiv=""refresh"" content=""{refresh}"">");
            builder.AppendLine("</head>");
            return builder.ToString();
        }
    }
}