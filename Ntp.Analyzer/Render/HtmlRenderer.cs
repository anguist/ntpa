// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Text;

namespace Ntp.Analyzer.Render
{
    /// <summary>
    /// Renders IHtmlObjects into plain text for display in browsers.
    /// </summary>
    public class HtmlRenderer : Renderer, IHtmlObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlRenderer" /> class.
        /// </summary>
        /// <param name="htmlObject">The HTML object.</param>
        public HtmlRenderer(IHtmlObject htmlObject)
        {
            this.htmlObject = htmlObject;
            children = new List<IHtmlObject>(htmlObject.Children);
        }

        private readonly List<IHtmlObject> children;
        private readonly IHtmlObject htmlObject;

        public Uri WebPath => htmlObject.WebPath;

        /// <summary>
        /// Gets the children of the IHtmlObject.
        /// </summary>
        /// <value>The children.</value>
        public IEnumerable<IHtmlObject> Children => children.ToArray();

        /// <summary>
        /// Renders the head of the object.
        /// </summary>
        /// <returns>The head.</returns>
        public string RenderHead()
        {
            return htmlObject.RenderHead();
        }

        /// <summary>
        /// Render this instance plus all child instances.
        /// </summary>
        public override string Render()
        {
            var builder = new StringBuilder();

            builder.Append(RenderHead());
            builder.Append(htmlObject.Render());

            foreach (IHtmlObject renderer in children)
            {
                var childRenderer = new HtmlRenderer(renderer);
                builder.Append(childRenderer.Render());
            }

            builder.Append(RenderFooter());

            return builder.ToString();
        }

        /// <summary>
        /// Renders the footer of the object.
        /// </summary>
        /// <returns>The footer.</returns>
        public string RenderFooter()
        {
            return htmlObject.RenderFooter();
        }
    }
}