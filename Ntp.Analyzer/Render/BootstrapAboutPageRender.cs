// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Globalization;
using System.Text;

namespace Ntp.Analyzer.Render
{
    public class BootstrapAboutPageRender : BootstrapPageRender
    {
        public BootstrapAboutPageRender(Uri webPath, string title, string header, string content, int? id)
            : base(webPath, title)
        {
            this.header = header;
            this.content = content;
            this.id = id;
        }

        private readonly string content;
        private readonly string header;
        private readonly int? id;

        protected override string RenderPageBody()
        {
            var builder = new StringBuilder();

            builder.AppendLine(@"<div class=""container theme-showcase"">");
            builder.AppendLine(@"<div class=""jumbotron"">");
            builder.Append("<h1>");
            builder.Append(header);
            builder.AppendLine("</h1>");
            builder.AppendLine(content);

            if (id != null)
            {
                builder.Append(@"<p class=""pool""><a class=""btn btn-lg btn-primary"" ");
                builder.Append(
                    $@"href=""http://support.ntp.org/bin/view/Servers/PublicTimeServer{id.Value.ToString(
                        CultureInfo.InvariantCulture).PadLeft(6, '0')}"">");
                builder.AppendLine(@"View info on support.ntp.org &raquo;</a></p>");
            }

            builder.AppendLine("</div>");
            builder.AppendLine("</div>");

            return builder.ToString();
        }

        protected override string RenderPageFooter()
        {
            return string.Empty;
        }

        protected override string RenderPageHead()
        {
            return this.AppendWithWebpath(
                @"  <link href=""{0}{1}"" rel=""stylesheet"" media=""screen"">",
                "css/graph.css");
        }
    }
}