// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Page;
using Ntp.Analyzer.Data;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render.Summary
{
    public sealed class DefaultPeerSummaryGraphRender : HtmlObjectRender
    {
        public DefaultPeerSummaryGraphRender(
            int hostId,
            PeerSummaryPageConfiguration page,
            IEnumerable<GraphSetConfiguration> graphs)
            : base(page.WebPath)
        {
            this.hostId = hostId;
            this.page = page;
            this.graphs = new List<GraphSetConfiguration>(graphs);
        }

        private readonly List<GraphSetConfiguration> graphs;
        private readonly int hostId;
        private readonly PeerSummaryPageConfiguration page;

        public override string Render()
        {
            var builder = new StringBuilder();

            var peers = DataFace.Instance.PeerActivities.
                Where(p => p.Host.Id == hostId && p.IsActive);

            foreach (var entry in peers)
            {
                foreach (var graphSet in graphs)
                {
                    foreach (var graph in graphSet.Graphs)
                    {
                        builder.Append(@"<a href=""");
                        builder.Append(page.GetPeerLink(entry.Id).ToHtmlString());
                        builder.Append(@""">");
                        builder.Append(@"<img src=""");
                        builder.Append(
                            graph.GetLink(graphSet, entry.Id.ToString(CultureInfo.InvariantCulture)).ToHtmlString());
                        builder.Append(@""" alt=""");
                        builder.Append(graph.GetAltName(graphSet, entry.Peer.Name));
                        builder.Append(@""">");
                        builder.AppendLine(@"</a>");
                    }
                }
            }

            return builder.ToString();
        }

        public override string RenderFooter()
        {
            return string.Empty;
        }

        public override string RenderHead()
        {
            return string.Empty;
        }
    }
}