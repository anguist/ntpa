// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Text;
using Ntp.Analyzer.Config.Node.Destination;
using Ntp.Analyzer.Config.Node.Graph;
using Ntp.Analyzer.Objects;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render.Graph
{
    public sealed class BootstrapGraphPageRender : BootstrapPageRender
    {
        public BootstrapGraphPageRender(
            Uri webPath,
            NamedObject namedObject,
            string title,
            string prefix,
            GraphSetConfiguration graphSet,
            GraphBaseConfiguration graph)
            : base(webPath, title)
        {
            this.namedObject = namedObject;
            this.prefix = prefix;
            this.graphSet = graphSet;
            this.graph = graph;
        }

        private readonly GraphBaseConfiguration graph;
        private readonly GraphSetConfiguration graphSet;
        private readonly NamedObject namedObject;
        private readonly string prefix;

        protected override string Title
        {
            get
            {
                string title = base.Title;

                if (title != string.Empty)
                    title += " - ";

                return title + graph.GetTitle(namedObject);
            }
        }

        protected override string RenderPageBody()
        {
            var builder = new StringBuilder();

            builder.AppendLine(@"  <div class=""container"">");
            builder.Append(@"    <img src=""");
            builder.Append(graph.GetLink(graphSet, prefix).ToHtmlString());
            builder.Append(@""" alt=""");
            builder.Append(graph.GetAltName(graphSet, prefix));
            builder.AppendLine(@""">");
            builder.AppendLine("  </div>");

            return builder.ToString();
        }

        protected override string RenderPageFooter()
        {
            return string.Empty;
        }

        protected override string RenderPageHead()
        {
            return string.Empty;
        }
    }
}