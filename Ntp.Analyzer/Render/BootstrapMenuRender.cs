// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Linq;
using System.Text;
using Ntp.Analyzer.Config.Node.Navigation;
using Ntp.Common.Web;

namespace Ntp.Analyzer.Render
{
    public sealed class BootstrapMenuRender : HtmlObjectRender
    {
        public BootstrapMenuRender(Uri webPath, MenuConfiguration menu, ILinkable page)
            : base(webPath)
        {
            this.menu = menu;
            this.page = page;
        }

        private readonly MenuConfiguration menu;
        private readonly ILinkable page;

        public override string Render()
        {
            var b = new StringBuilder();

            b.AppendLine(@"  <div class=""navbar navbar-inverse navbar-fixed-top"">");
            b.AppendLine(@"    <div class=""container"">");
            b.AppendLine(@"      <div class=""navbar-header"">");
            b.AppendLine(
                @"        <button type=""button"" class=""navbar-toggle"" data-toggle=""collapse"" data-target="".navbar-collapse"">");
            b.AppendLine(@"          <span class=""icon-bar""></span>");
            b.AppendLine(@"          <span class=""icon-bar""></span>");
            b.AppendLine(@"          <span class=""icon-bar""></span>");
            b.AppendLine(@"        </button>");

            var headItem =
                menu.MenuItems.SingleOrDefault(i => i is HeadMenuItemConfiguration) as HeadMenuItemConfiguration;

            if (headItem != null)
            {
                b.AppendLine(
                    $@"        <a class=""navbar-brand"" href=""{headItem.Link.ToHtmlString()}"">{headItem.Caption}</a>");
            }

            b.AppendLine(@"      </div>");
            b.AppendLine(@"      <div class=""navbar-collapse collapse"">");
            b.AppendLine(@"        <ul class=""nav navbar-nav"">");

            foreach (var item in menu.MenuItems.Where(i => !(i is HeadMenuItemConfiguration)))
            {
                string active = item.Page == page ? @" class=""active""" : string.Empty;

                if (item is LinkMenuItemConfiguration || item is PageMenuItemConfiguration)
                {
                    b.AppendLine(
                        $@"          <li{active}><a href=""{item.Link.ToHtmlString()}"">{item.Caption}</a></li>");
                }
                else if (item is DropdownItemConfiguration)
                {
                    b.AppendLine(@"          <li class=""dropdown"">");
                    b.AppendLine(
                        @"            <a href=""#"" class=""dropdown-toggle"" data-toggle=""dropdown"">Peers <b class=""caret""></b></a>");
                    b.AppendLine(@"            <ul class=""dropdown-menu"">");

                    var dropdownItem = item as DropdownItemConfiguration;

                    foreach (var subItem in dropdownItem.MenuItems)
                    {
                        if (subItem is LinkMenuItemConfiguration || subItem is PageMenuItemConfiguration)
                        {
                            b.AppendLine(
                                $@"              <li><a href=""{subItem.Link.ToHtmlString()}"">{subItem.Caption}</a></li>");
                        }
                        else if (subItem is SpacerMenuItemConfiguration)
                        {
                            b.AppendLine(@"              <li class=""divider""></li>");
                        }
                        else if (subItem is HeaderMenuItemConfiguration)
                        {
                            b.AppendLine($@"              <li class=""dropdown-header"">{subItem.Caption}</li>");
                        }
                        else
                        {
                            throw new NotSupportedException("Unknown item type.");
                        }
                    }

                    b.AppendLine(@"            </ul>");
                }
                else
                {
                    throw new NotSupportedException("Unknown item type.");
                }
            }

            b.AppendLine(@"        </ul>");
            b.AppendLine(@"      </div>");
            b.AppendLine(@"    </div>");
            b.AppendLine(@"  </div>");

            return b.ToString();
        }

        public override string RenderFooter()
        {
            return string.Empty;
        }

        public override string RenderHead()
        {
            return string.Empty;
        }
    }
}