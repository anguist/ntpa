﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Data.Common;
using System.Threading;
using Ntp.Common.App;
using Ntp.Common.Log;
using Ntp.Data.Log;

namespace Ntp.Data
{
    public sealed class SqlDatabaseChecker
    {
        public SqlDatabaseChecker(ISqlFactory factory, IApplicationController controller, LogBase log)
        {
            this.controller = controller;
            this.factory = factory;
            this.log = log;
            stop = false;
        }

        private readonly IApplicationController controller;
        private readonly ISqlFactory factory;
        private readonly LogBase log;
        private bool stop;

        public void CheckConnection()
        {
            controller.ExitApplication += ControllerExitApplication;

            var connection = factory.CreateGenericConnection();
            log.AwaitingDbLink(connection);

            bool first = true;
            bool done = false;

            do
            {
                try
                {
                    connection.Open();
                    done = true;
                }
                catch (DbException ex)
                {
                    log.DbLinkError(ex);

                    if (first)
                    {
                        log.DbLinkDown();
                    }

                    first = false;
                    Thread.Sleep(5000);
                }
            } while (!done && !stop);

            if (done)
            {
                log.DbLinkUp();

                try
                {
                    connection.Close();
                }
                catch (Exception e)
                {
                    log.WriteLine(e);
                }
            }

            controller.ExitApplication -= ControllerExitApplication;
        }

        private void ControllerExitApplication(object sender, EventArgs e)
        {
            stop = true;
        }
    }
}