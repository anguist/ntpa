// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Globalization;
using System.Linq;
using System.Text;
using Ntp.Analyzer.Config.Node;

namespace Ntp.Analyzer.Monitor.Server.TextCommand
{
    internal sealed class ServerCommand : MonitorTextCommand
    {
        public ServerCommand(string[] args)
            : base(args)
        {
        }

        protected override string ExecuteTextCommand()
        {
            var builder = new StringBuilder();

            builder.Append("Host".PadRight(18));
            builder.Append("#HP".PadLeft(4));
            builder.Append("#HG".PadLeft(4));
            builder.Append("#GP".PadLeft(4));
            builder.Append("#PP".PadLeft(4));
            builder.Append("#PG".PadLeft(4));
            builder.Append("#GP".PadLeft(4));
            builder.Append("#PS".PadLeft(4));
            builder.Append("#AB".PadLeft(4));
            builder.AppendLine();

            builder.AppendLine(string.Empty.PadLeft(18 + 8*4 + 2, '-'));

            foreach (HostConfiguration server in ApplicationState.Config.Servers)
            {
                builder.Append(server.ServerName.PadRight(18));
                builder.Append(server.HostPages.Count().ToString(CultureInfo.InvariantCulture).PadLeft(4));
                builder.Append(server.HostGraphs.Count().ToString(CultureInfo.InvariantCulture).PadLeft(4));
                builder.Append((server.HostGraphPage == null ? "0" : "1").PadLeft(4));
                builder.Append(server.PeerPages.Count().ToString(CultureInfo.InvariantCulture).PadLeft(4));
                builder.Append(server.PeerGraphs.Count().ToString(CultureInfo.InvariantCulture).PadLeft(4));
                builder.Append((server.PeerGraphPage == null ? "0" : "1").PadLeft(4));
                builder.Append(server.PeerSummaryPages.Count().ToString(CultureInfo.InvariantCulture).PadLeft(4));
                builder.Append((server.AboutPage == null ? "0" : "1").PadLeft(4));
                builder.AppendLine();
            }

            return builder.ToString();
        }
    }
}