// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Linq;
using System.Text;

namespace Ntp.Analyzer.Monitor.Server.TextCommand
{
    internal sealed class HelpCommand : MonitorTextCommand
    {
        public HelpCommand(string[] args)
            : base(args)
        {
        }

        public HelpCommand(bool error)
            : base(new string[0])
        {
            this.error = error;
        }

        private readonly bool error;

        protected override string ExecuteTextCommand()
        {
            var builder = new StringBuilder();

            if (error)
            {
                builder.AppendLine("Unknown command.");
                builder.AppendLine();
            }

            var version = new VersionCommand(new string[0]);
            builder.Append(version.Execute());
            builder.AppendLine(". the following commands are valid.");

            int length = CommandFactory.Commands.Max(c => c.Description.Length);
            length += 15 + 3;

            builder.AppendLine(string.Empty.PadLeft(length, '-'));

            foreach (ICommandDescription command in CommandFactory.Commands.OrderBy(c => c.Name))
            {
                builder.Append(command.Name.PadRight(15));
                builder.AppendLine(command.Description);
            }

            return builder.ToString();
        }
    }
}