// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Globalization;
using System.Text;

namespace Ntp.Analyzer.Monitor.Server.TextCommand
{
    internal sealed class UptimeCommand : MonitorTextCommand
    {
        public UptimeCommand(string[] args)
            : base(args)
        {
        }

        protected override string ExecuteTextCommand()
        {
            TimeSpan runtime = DateTime.Now.Subtract(ApplicationState.StartupTime);

            var builder = new StringBuilder();

            builder.Append("Daemon was started ");
            builder.Append(ApplicationState.StartupTime.ToShortDateString());
            builder.Append(" ");
            builder.Append(TimeZone.CurrentTimeZone.StandardName);
            builder.Append(" ");
            builder.Append(ApplicationState.StartupTime.ToLongTimeString());
            builder.AppendLine();

            builder.Append("Uptime ");
            builder.Append(FormatTimespan(runtime));
            builder.AppendLine();

            return builder.ToString();
        }

        private string FormatTimespan(TimeSpan time)
        {
            string days;

            if (time.TotalDays >= 1 && time.TotalDays < 2)
            {
                days = "1 day ";
            }
            else if (time.TotalDays >= 2)
            {
                days = time.TotalDays.ToString(CultureInfo.InvariantCulture) + " days ";
            }
            else
            {
                days = string.Empty;
            }

            string hours;

            if (time.Hours >= 1 && time.Hours < 2)
            {
                hours = "1 hour ";
            }
            else if (time.Hours >= 2)
            {
                hours = time.Hours.ToString(CultureInfo.InvariantCulture) + " hours ";
            }
            else
            {
                hours = string.Empty;
            }

            string minutes;

            if (time.Minutes >= 1 && time.Minutes < 2)
            {
                minutes = "1 minute ";
            }
            else if (time.Minutes >= 2)
            {
                minutes = time.Minutes.ToString(CultureInfo.InvariantCulture) + " minutes ";
            }
            else
            {
                minutes = string.Empty;
            }

            string seconds;

            if (time.Seconds >= 1 && time.Seconds < 2)
            {
                seconds = "1 second";
            }
            else if (time.Seconds >= 2)
            {
                seconds = time.Seconds.ToString(CultureInfo.InvariantCulture) + " seconds";
            }
            else
            {
                seconds = string.Empty;
            }

            return string.Concat(days, hours, minutes, seconds);
        }
    }
}