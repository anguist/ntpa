// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Ntp.Analyzer.Monitor.Server.TextCommand;

namespace Ntp.Analyzer.Monitor.Server
{
    public static class CommandFactory
    {
        public static readonly IList<ICommandDescription> Commands = new List<ICommandDescription>
        {
            new CommandDescription<ConfigFileCommand>("config", "Shows name and path of current configuration."),
            new CommandDescription<HelpCommand>("help", "Shows valid commands."),
            new CommandDescription<JobsCommand>("jobs", "Shows a list of jobs in scheduler."),
            new CommandDescription<NextJobCommand>("next", "Shows when next scheduled job will be active."),
            new CommandDescription<PidCommand>("pid", "Shows process id of NTPA daemon."),
            new CommandDescription<PingCommand>("ping", "Responds if daemon is running."),
            new CommandDescription<ProgCommand>("prog", "Shows active configuration."),
            new CommandDescription<ScheduleCommand>("schedule", "Shows schedule for active jobs."),
            new CommandDescription<ServerCommand>("pages", "Shows number of configured pages on NTPA daemon."),
            new CommandDescription<TimeCommand>("time", "Shows current server time in UTC."),
            new CommandDescription<UptimeCommand>("uptime", "Shows uptime of NTPA daemon."),
            new CommandDescription<VersionCommand>("version", "Shows name and version."),
            new CommandDescription<RunningCommand>("running", "Shows a list of running jobs."),
            new CommandDescription<ProcCommand>("proc", "Shows a list of jobs and execution time."),
            new CommandDescription<ActivityCommand>("activity", "Shows recent activity in scheduler.")
        };

        internal static Command Create(string command, string[] args)
        {
            var commandDescription = Commands.SingleOrDefault(c => c.Name == command);

            if (commandDescription == null)
                return new HelpCommand(true);

            return Activator.CreateInstance(commandDescription.Type, new object[] {args}) as Command;
        }
    }
}