﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using Ntp.Analyzer.Data.Changes;
using Ntp.Analyzer.Data.Log;
using Ntp.Analyzer.Data.Sql;
using Ntp.Common.Log;
using Ntp.Data;
using Ntp.Data.Provider;
using Ntp.Data.Schema;

namespace Ntp.Analyzer.Data
{
    public sealed class DatabaseInitializer
    {
        public DatabaseInitializer(bool initialize, bool upgrade, LogBase log)
        {
            this.initialize = initialize;
            this.upgrade = upgrade;
            this.log = log;

            updater = new DatabaseUpdater(SqlDatabaseFactory.Instance, log)
            {
                new Change01(),
                new Change02(),
                new Change03()
            };
        }

        private readonly bool initialize;

        private readonly LogBase log;

        private readonly DatabaseUpdater updater;

        private readonly bool upgrade;

        public bool Execute()
        {
            if (initialize)
            {
                CreateDatabase();
                CreateTables();
            }

            if (upgrade)
            {
                updater.ApplyChanges();
            }

            return updater.CheckLatestVersion();
        }

        private void CreateDatabase()
        {
            try
            {
                SqlDatabaseFactory.Instance.CreateDatabase();
            }
            catch (Exception e)
            {
                log.CreateDatabaseError(e);
            }
        }

        private void CreateTables()
        {
            TimeServerDatabaseMapper timeServerMapper;
            PeerDatabaseMapper peerMapper;
            HostDatabaseMapper hostMapper;

            var initializers = new List<ITableInitializer>();
            initializers.Add(timeServerMapper = new TimeServerDatabaseMapper(log));
            initializers.Add(peerMapper = new PeerDatabaseMapper(timeServerMapper, log));
            initializers.Add(hostMapper = new HostDatabaseMapper(log));
            initializers.Add(new HostReadingDatabaseMapper(hostMapper, peerMapper, log));
            initializers.Add(new PeerReadingDatabaseMapper(hostMapper, peerMapper, log));
            initializers.Add(new HostIoReadingDatabaseMapper(hostMapper, log));
            initializers.Add(new DriftReadingDatabaseMapper(hostMapper, log));
            initializers.Add(new PeerActivityDatabaseMapper(hostMapper, peerMapper, log));
            initializers.Add(new AssociationEntryMapper(log));

            foreach (var initializer in initializers)
            {
                try
                {
                    initializer.CheckTable();
                }
                catch (Exception e)
                {
                    log.CreateTableError(e);
                }
            }
        }
    }
}