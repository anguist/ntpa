// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;
using Ntp.Data.Provider;

namespace Ntp.Analyzer.Data.Sql
{
    /// <summary>
    /// OR/M mapper for tables with a large amount of rows.
    /// </summary>
    public abstract class FilteredSqlDatabaseMapper<T> : SqlDatabaseMapper<T>
        where T : PersistentObject
    {
        protected FilteredSqlDatabaseMapper(LogBase log)
            : base(log)
        {
        }

        /// <summary>
        /// Gets or sets the time to use when extracting data. Only readings with a timestamp
        /// later than FilterTime gets extracted.
        /// </summary>
        /// <value>The time.</value>
        public DateTime? FilterTime { get; set; }

        /// <summary>
        /// Gets or sets the host
        /// </summary>
        /// <value>The host.</value>
        public Host FilterHost { get; set; }

        /// <summary>
        /// Gets or sets the peer to use when extracting data.
        /// </summary>
        /// <value>The filter peer.</value>
        public Peer FilterPeer { get; set; }

        protected override string PrepareSql(string sql)
        {
            string preparedSql = base.PrepareSql(sql).Trim();

            if (FilterTime == null && FilterHost == null && FilterPeer == null)
                return preparedSql;

            bool filterAdded = false;

            preparedSql += " WHERE ";

            if (FilterTime != null)
            {
                var time = FilterTime.Value.ToUniversalTime();
                var datePart = SqlDatabaseFactory.Instance.DateAddMinutes("time", "zone");
                preparedSql += $"{datePart} > \'{time.ToString("yyyy-MM-dd HH:mm:ss")}\'";
                filterAdded = true;
            }

            if (FilterHost != null)
            {
                if (filterAdded)
                {
                    preparedSql += " AND ";
                }

                preparedSql += $"hostId = {FilterHost.Id}";
                filterAdded = true;
            }

            if (FilterPeer == null)
                return preparedSql;

            if (filterAdded)
            {
                preparedSql += " AND ";
            }

            preparedSql += $"peerId = {FilterPeer.Id}";

            return preparedSql;
        }
    }
}