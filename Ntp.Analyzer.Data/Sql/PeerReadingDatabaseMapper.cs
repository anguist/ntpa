// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Ntp.Analyzer.Data.Log;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;
using Ntp.Data.Log;

namespace Ntp.Analyzer.Data.Sql
{
    /// <summary>
    /// OR/M mapper for table peerReading.
    /// </summary>
    public sealed class PeerReadingDatabaseMapper : FilteredSqlDatabaseMapper<PeerReading>
    {
        internal PeerReadingDatabaseMapper(
            HostDatabaseMapper hostMapper,
            PeerDatabaseMapper peerMapper,
            LogBase log)
            : base(log)
        {
            this.hostMapper = hostMapper;
            this.peerMapper = peerMapper;
        }

        private const string SelectSql =
            "SELECT " +
            "id, time, zone, hostId, peerId, last, reach, delay, [offset], jitter " +
            "FROM peerReading";

        private const string InsertSql =
            "INSERT INTO peerReading ( time, zone, hostId, peerId, last, reach, delay, [offset], jitter ) " +
            "VALUES ( @time, @zone, @hostId, @peerId, @last, @reach, @delay, @offset, @jitter );{0};";

        private readonly HostDatabaseMapper hostMapper;
        private readonly PeerDatabaseMapper peerMapper;

        protected override bool UseCache => false;

        protected override string TableName => "peerReading";

        protected override string CreateSql => "CREATE TABLE peerReading ( " +
                                               "    id {0} PRIMARY KEY, " +
                                               "    time  TIMESTAMP NOT NULL, " +
                                               "    zone INT NOT NULL, " +
                                               "    hostId INT NOT NULL, " +
                                               "    peerId INT NOT NULL, " +
                                               "    last INT NOT NULL, " +
                                               "    reach INT NOT NULL, " +
                                               "    delay DOUBLE PRECISION NOT NULL, " +
                                               "    [offset] DOUBLE PRECISION NOT NULL, " +
                                               "    jitter DOUBLE PRECISION NOT NULL, " +
                                               "    FOREIGN KEY (hostId) REFERENCES host(id), " +
                                               "    FOREIGN KEY (peerId) REFERENCES peer(id) " +
                                               "){1};";

        /// <summary>
        /// Read all data from table in a sequential manner.
        /// </summary>
        /// <returns>The enumerator.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public override IEnumerator<PeerReading> GetEnumerator()
        {
            lock (MapperLocker)
            {
                bool error = false;

                try
                {
                    Open();
                    Command.CommandText = PrepareSql(SelectSql);
                    Log.SqlExecute(Command.CommandText);
                    Reader = Command.ExecuteReader();
                }
                catch (Exception e)
                {
                    Log.ReadError(TableName, e);
                    error = true;
                }

                if (error)
                    yield break;

                while (Reader.Read())
                {
                    int id = Convert.ToInt32(Reader["id"]);
                    var time = Convert.ToDateTime(Reader["time"]);
                    int zone = Convert.ToInt32(Reader["zone"]);
                    int hostId = Convert.ToInt32(Reader["hostId"]);
                    var host = hostMapper[hostId];
                    int peerId = Convert.ToInt32(Reader["peerId"]);
                    var peer = peerMapper[peerId];
                    int last = Convert.ToInt32(Reader["last"]);
                    int reach = Convert.ToInt32(Reader["reach"]);
                    double delay = Convert.ToDouble(Reader["delay"]);
                    double offset = Convert.ToDouble(Reader["offset"]);
                    double jitter = Convert.ToDouble(Reader["jitter"]);

                    var reading = new PeerReading(
                        id, time, zone, host, peer,
                        last, reach, delay, offset, jitter);

                    yield return reading;
                }

                Close();
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected override void Insert(PeerReading item)
        {
            lock (MapperLocker)
            {
                try
                {
                    Open();
                    Command.CommandText = PrepareInsertSql(InsertSql);
                    Command.Parameters.Add(CreateParameter("@time", item.Time));
                    Command.Parameters.Add(CreateParameter("@zone", item.UtcOffset));
                    Command.Parameters.Add(CreateParameter("@hostId", item.Host.Id));
                    Command.Parameters.Add(CreateParameter("@peerId", item.Peer.Id));
                    Command.Parameters.Add(CreateParameter("@last", item.LastPoll));
                    Command.Parameters.Add(CreateParameter("@reach", item.Reach));
                    Command.Parameters.Add(CreateParameter("@delay", item.Delay));
                    Command.Parameters.Add(CreateParameter("@offset", item.Offset));
                    Command.Parameters.Add(CreateParameter("@jitter", item.Jitter));
                    Command.Prepare();
                    Log.SqlExecute(Command.CommandText, Command.Parameters);
                    var idObject = Command.ExecuteScalar();
                    item.SetId(Convert.ToInt32(idObject));
                }
                catch (Exception e)
                {
                    Log.InsertError(TableName, e);
                }
                finally
                {
                    Close();
                }
            }
        }

        protected override void ReadContent()
        {
            throw new NotSupportedException(LogMessages.DatabaseCacheError);
        }

        protected override void Update(PeerReading item)
        {
            throw new NotSupportedException(string.Format(LogMessages.DatabaseNoUpdate, TableName));
        }
    }
}