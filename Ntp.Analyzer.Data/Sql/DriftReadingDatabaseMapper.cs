﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Ntp.Analyzer.Data.Log;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;
using Ntp.Data.Log;

namespace Ntp.Analyzer.Data.Sql
{
    /// <summary>
    /// OR/M mapper for table driftReading.
    /// </summary>
    public sealed class DriftReadingDatabaseMapper : FilteredSqlDatabaseMapper<DriftReading>
    {
        internal DriftReadingDatabaseMapper(HostDatabaseMapper hostMapper, LogBase log)
            : base(log)
        {
            this.hostMapper = hostMapper;
        }

        private const string SelectSql =
            "SELECT " +
            "id, time, zone, hostId, drift " +
            "FROM driftReading";

        private const string InsertSql =
            "INSERT INTO driftReading ( time, zone, hostId, drift ) " +
            "VALUES ( @time, @zone, @hostId, @drift );{0};";

        private readonly HostDatabaseMapper hostMapper;

        protected override bool UseCache => false;

        protected override string TableName => "driftReading";

        protected override string CreateSql => "CREATE TABLE driftReading ( " +
                                               "    id {0} PRIMARY KEY, " +
                                               "    time  TIMESTAMP NOT NULL, " +
                                               "    zone INT NOT NULL, " +
                                               "    hostId INT NOT NULL, " +
                                               "    drift DOUBLE PRECISION NOT NULL, " +
                                               "    FOREIGN KEY (hostId) REFERENCES host(id) " +
                                               "){1};";

        /// <summary>
        /// Read all data from table in a sequential manner.
        /// </summary>
        /// <returns>The enumerator.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public override IEnumerator<DriftReading> GetEnumerator()
        {
            lock (MapperLocker)
            {
                bool error = false;
                string sql = PrepareSql(SelectSql);

                try
                {
                    Open();
                    Command.CommandText = sql;
                    Log.SqlExecute(Command.CommandText);
                    Reader = Command.ExecuteReader();
                }
                catch (Exception e)
                {
                    Log.ReadError(TableName, e);
                    error = true;
                }

                if (error)
                    yield break;

                while (Reader.Read())
                {
                    int id = Convert.ToInt32(Reader["id"]);
                    var time = Convert.ToDateTime(Reader["time"]);
                    int zone = Convert.ToInt32(Reader["zone"]);
                    int hostId = Convert.ToInt32(Reader["hostId"]);
                    var host = hostMapper[hostId];
                    double drift = Convert.ToDouble(Reader["drift"]);
                    yield return new DriftReading(id, time, zone, host, drift);
                }

                Close();
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected override void Insert(DriftReading item)
        {
            lock (MapperLocker)
            {
                try
                {
                    Open();
                    Command.CommandText = PrepareInsertSql(InsertSql);
                    Command.Parameters.Add(CreateParameter("@time", item.Time));
                    Command.Parameters.Add(CreateParameter("@zone", item.UtcOffset));
                    Command.Parameters.Add(CreateParameter("@hostId", item.Host.Id));
                    Command.Parameters.Add(CreateParameter("@drift", item.Value));
                    Command.Prepare();
                    Log.SqlExecute(Command.CommandText, Command.Parameters);
                    var idObject = Command.ExecuteScalar();
                    item.SetId(Convert.ToInt32(idObject));
                }
                catch (Exception e)
                {
                    Log.InsertError(TableName, e);
                }
                finally
                {
                    Close();
                }
            }
        }

        protected override void ReadContent()
        {
            throw new NotSupportedException(LogMessages.DatabaseCacheError);
        }

        protected override void Update(DriftReading item)
        {
            throw new NotSupportedException(string.Format(LogMessages.DatabaseNoUpdate, TableName));
        }
    }
}