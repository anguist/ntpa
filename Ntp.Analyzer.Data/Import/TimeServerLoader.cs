// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Globalization;
using Ntp.Analyzer.Data.Log;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Data.Import
{
    internal sealed class TimeServerLoader : TimeServerWebAdapter
    {
        internal TimeServerLoader(LogBase log)
            : base(log)
        {
        }

        private const string ServerTagStart = @"<div class=""twikiAfterText""></div><div class=""twikiForm"">";
        private const string ServerTagEnd = @"</div><!-- /twikiForm --></div><!-- /patternContent-->";

        protected override string Provider => "support.ntp.org";

        public override TimeServer Import(int orgId)
        {
            string table = ImportServer(orgId);
            if (table == null)
                return null;

            var importer = new TimeServerImporter(Log);
            var server = importer.ParseTable(table, orgId);
            return server;
        }

        private string ImportServer(int orgId)
        {
            string orgString = orgId.ToString(CultureInfo.InvariantCulture).PadLeft(6, '0');
            string url = $"http://support.ntp.org/bin/view/Servers/PublicTimeServer{orgString}";
            string html = FetchHtml(url, orgId);

            if (html == null)
                return null;

            html = html.Replace("<nos>", string.Empty);

            try
            {
                int serverTagPos = html.IndexOf(ServerTagStart, StringComparison.Ordinal);
                int tableStartPos = html.IndexOf("<table", serverTagPos, StringComparison.Ordinal);
                int tableEndPos = html.IndexOf(ServerTagEnd, serverTagPos, StringComparison.Ordinal);
                string table = html.Substring(tableStartPos, tableEndPos - tableStartPos);
                return table;
            }
            catch (Exception e)
            {
                Log.TimeServerParseError("HTML", e);
                return null;
            }
        }
    }
}