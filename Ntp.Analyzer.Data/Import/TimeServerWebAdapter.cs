﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using System.Net;
using Ntp.Analyzer.Data.Log;
using Ntp.Analyzer.Objects;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Data.Import
{
    public abstract class TimeServerWebAdapter
    {
        protected TimeServerWebAdapter(LogBase log)
        {
            Log = log;
        }

        private static bool enabled;

        protected readonly LogBase Log;

        protected abstract string Provider { get; }

        public static TimeServerWebAdapter Create(LogBase log)
        {
            // Use the ExactlyAdapter for now
            return enabled ? new ExactlyAdapter(log) : null;
        }

        public abstract TimeServer Import(int orgId);

        public static void Initialize(bool enable)
        {
            enabled = enable;
        }

        protected string FetchHtml(string url, int orgId)
        {
            if (orgId >= 5000)
            {
                Log.TimeServerMaxId(Provider);
                return null;
            }

            string html = Download(url, orgId);
            if (html != null)
                return html;

            Log.TimeServerNotReceived(orgId);
            return null;
        }

        private string Download(string url, int orgId)
        {
            try
            {
                var client = new WebClient();
                // TODO: Set timeout
                client.Headers.Add(
                    "user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2;)"
                    );

                var stream = client.OpenRead(url);
                Log.TimeServerDownload(Provider, url, orgId);

                if (stream != null)
                {
                    var reader = new StreamReader(stream);
                    string html = reader.ReadToEnd();
                    reader.Close();
                    return html;
                }
            }
            catch (WebException e)
            {
                var response = e.Response as HttpWebResponse;
                if (response != null && response.StatusCode == HttpStatusCode.NotFound)
                {
                    Log.TimeServerNotFound(Provider, orgId);
                }
                else
                {
                    Log.TimeServerError(Provider, orgId);
                }

                Log.WriteLine(e);
            }
            catch (Exception e)
            {
                Log.TimeServerError(Provider, orgId);
                Log.WriteLine(e);
            }

            return null;
        }
    }
}