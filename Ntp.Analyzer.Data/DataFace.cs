// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Ntp.Analyzer.Data.Sql;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Data
{
    /// <summary>
    /// Singleton facade class used to access memory persistent data.
    /// </summary>
    public sealed class DataFace
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ntp.Analyzer.Data.DataFace" /> class.
        /// </summary>
        private DataFace(LogBase log)
        {
            this.log = log;
            Hosts = new HostDatabaseMapper(log);
            Servers = new TimeServerDatabaseMapper(log);
            Peers = new PeerDatabaseMapper(Servers, log);
            PeerActivities = new PeerActivityDatabaseMapper(Hosts, Peers, log);
            ReadingBulks = new ReadingBulkMapper(log);
        }

        private readonly LogBase log;

        /// <summary>
        /// Gets the Singleton instance.
        /// </summary>
        /// <value>The instance.</value>
        public static DataFace Instance { get; private set; }

        /// <summary>
        /// Gets the host mapper.
        /// </summary>
        /// <value>The hosts.</value>
        public HostDatabaseMapper Hosts { get; }

        /// <summary>
        /// Gets the peer mapper.
        /// </summary>
        /// <value>The peers.</value>
        public PeerDatabaseMapper Peers { get; }

        /// <summary>
        /// Gets the time server mapper.
        /// </summary>
        /// <value>The servers.</value>
        public TimeServerDatabaseMapper Servers { get; }

        /// <summary>
        /// Gets the host reading mapper.
        /// </summary>
        /// <value>The host readings.</value>
        public HostReadingDatabaseMapper HostReadings => new HostReadingDatabaseMapper(Hosts, Peers, log);

        /// <summary>
        /// Gets the host IO reading mapper.
        /// </summary>
        /// <value>The host IO reading mapper.</value>
        public HostIoReadingDatabaseMapper HostIoReadings => new HostIoReadingDatabaseMapper(Hosts, log);

        /// <summary>
        /// Gets the peer reading mapper.
        /// </summary>
        /// <value>The peer readings.</value>
        public PeerReadingDatabaseMapper PeerReadings => new PeerReadingDatabaseMapper(Hosts, Peers, log);

        /// <summary>
        /// Gets the host drift reading mapper.
        /// </summary>
        /// <value>The host drift reading mapper.</value>
        public DriftReadingDatabaseMapper DriftReadings => new DriftReadingDatabaseMapper(Hosts, log);

        /// <summary>
        /// Gets the peer activity mapper.
        /// </summary>
        /// <value>The peer activity mapper.</value>
        public PeerActivityDatabaseMapper PeerActivities { get; }

        /// <summary>
        /// Gets the reading bulk mapper.
        /// </summary>
        /// <value>The reading bulk mapper.</value>
        public ReadingBulkMapper ReadingBulks { get; }

        /// <summary>
        /// Gets the association entry mapper.
        /// </summary>
        /// <value>The the association entry mapper.</value>
        public AssociationEntryMapper AssociationEntries => new AssociationEntryMapper(log);

        public static void Initialize(LogBase log)
        {
            Instance = new DataFace(log);
        }
    }
}