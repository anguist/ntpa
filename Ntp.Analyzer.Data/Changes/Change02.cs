﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using Ntp.Data.Schema;

namespace Ntp.Analyzer.Data.Changes
{
    public sealed class Change02 : IVersionChanges
    {
        public Change02()
        {
            changes = new List<SqlChangeSet>
            {
                // Adjust host name and IP length to RFC values
                new SqlChangeSet(
                    "ALTER TABLE host " +
                    "MODIFY COLUMN name VARCHAR(255) NOT NULL;"
                    ),
                new SqlChangeSet(
                    "ALTER TABLE host " +
                    "MODIFY COLUMN ip VARCHAR(45) NOT NULL;"
                    ),
                new SqlChangeSet(
                    "ALTER TABLE peer " +
                    "MODIFY COLUMN name VARCHAR(255) NOT NULL;"
                    ),
                new SqlChangeSet(
                    "ALTER TABLE peer " +
                    "MODIFY COLUMN ip VARCHAR(45) NOT NULL;"
                    ),
                // Add time column to readings
                new SqlChangeSet(
                    "ALTER TABLE hostIoReading " +
                    "ADD COLUMN zone INT NOT NULL DEFAULT 0;"
                    ),
                new SqlChangeSet(
                    "ALTER TABLE hostReading " +
                    "ADD COLUMN zone INT NOT NULL DEFAULT 0;"
                    ),
                new SqlChangeSet(
                    "ALTER TABLE peerReading " +
                    "ADD COLUMN zone INT NOT NULL DEFAULT 0;"
                    ),
                new SqlChangeSet(
                    "ALTER TABLE peerActivity " +
                    "ADD COLUMN zone INT NOT NULL DEFAULT 0;"
                    )
            };
        }

        private readonly List<SqlChangeSet> changes;

        public string VersionText => "v0.7.2";

        public int VersionNumber => 000720;

        IEnumerator IEnumerable.GetEnumerator()
        {
            return changes.GetEnumerator();
        }

        public IEnumerator<SqlChangeSet> GetEnumerator()
        {
            return changes.GetEnumerator();
        }
    }
}