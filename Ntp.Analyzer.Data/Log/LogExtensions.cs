﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Ntp.Common.Log;

namespace Ntp.Analyzer.Data.Log
{
    internal static class LogMessages
    {
        internal const string DatabaseCacheError = "Mapper does not implement caching.";
        internal const string DatabaseNoUpdate = "Updating rows in {0} table is not supported.";
    }

    internal static class LogExtensions
    {
        internal static void CreateDatabaseError(this LogBase log, Exception e)
        {
            log.WriteLine("Failed to create database.", Severity.Error);
            log.WriteLine(e);
        }

        internal static void CreateTable(this LogBase log, string table)
        {
            log.WriteLine(
                $"Creating new table '{table}'.",
                Severity.Notice);
        }

        internal static void CreateTableError(this LogBase log, Exception e)
        {
            log.WriteLine("Could not create database table.", Severity.Error);
            log.WriteLine(e);
        }

        internal static void DeleteError(this LogBase log, string table, Exception e)
        {
            log.WriteLine($"Error while deleting from table {table}: {e.Message}", Severity.Warn);
            log.WriteLine(e, Severity.Warn);
            Advice(log);
        }

        internal static void InsertError(this LogBase log, string table, Exception e)
        {
            log.WriteLine($"Error while inserting into table {table}: {e.Message}", Severity.Warn);
            log.WriteLine(e, Severity.Warn);
            Advice(log);
        }

        internal static void ReadError(this LogBase log, string table, Exception e)
        {
            log.WriteLine($"Error while reading from table {table}: {e.Message}", Severity.Warn);
            log.WriteLine(e, Severity.Warn);
            Advice(log);
        }

        internal static void TableExists(this LogBase log, string table)
        {
            log.WriteLine(
                $"Skipping table '{table}'. Already exist.",
                Severity.Debug);
        }

        internal static void UpdateError(this LogBase log, string table, Exception e)
        {
            log.WriteLine($"Error while updating table {table}: {e.Message}", Severity.Warn);
            log.WriteLine(e, Severity.Warn);
            Advice(log);
        }

        private static void Advice(LogBase log)
        {
            log.WriteLine("Please check your configuration.", Severity.Notice);
        }
    }

    internal static class LogMessagesImport
    {
        internal static void TimeServerDownload(this LogBase log, string provider, string url, int orgId)
        {
            log.WriteLine($"Fetching data from {provider} for server ID {orgId}", Severity.Info);
            log.WriteLine($"Downloading ... {url}", Severity.Debug);
        }

        internal static void TimeServerError(this LogBase log, string provider, int orgId)
        {
            log.WriteLine($"Error while contacting {provider} with ID {orgId}", Severity.Warn);
        }

        internal static void TimeServerFetchError(this LogBase log, Exception e)
        {
            log.WriteLine("Failed to fetch time server data.", Severity.Warn);
            log.WriteLine(e, Severity.Warn);
        }

        internal static void TimeServerMaxId(this LogBase log, string provider)
        {
            log.WriteLine($"Not contacting {provider} for ID > 10000.", Severity.Debug);
        }

        internal static void TimeServerNotFound(this LogBase log, string provider, int orgId)
        {
            log.WriteLine($"Time server not found at {provider}. ID: {orgId}", Severity.Info);
        }

        internal static void TimeServerNotReceived(this LogBase log, int orgId)
        {
            log.WriteLine($"Nothing received for time server ID {orgId}.", Severity.Debug);
        }

        internal static void TimeServerParseError(this LogBase log, string type, Exception e)
        {
            log.WriteLine($"Could not parse time server {type}.", Severity.Warn);
            log.WriteLine(e, Severity.Warn);
        }
    }
}