// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Ntp.Common.Process;

namespace Ntp.Analyzer.Monitor.Client
{
    public sealed class TextRequest : Request, IRequest
    {
        public TextRequest(IPAddress ip, int port)
            : base(ip, port)
        {
        }

        public TextRequest(IPEndPoint server)
            : base(server)
        {
        }

        private StringBuilder result;

        public string Send(string command)
        {
            var clientSocket = new Socket(
                AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);

            try
            {
                IAsyncResult conRes = clientSocket.BeginConnect(Server, null, null);
                bool success = conRes.AsyncWaitHandle.WaitOne(3000, true);

                if (!success)
                {
                    throw new ApplicationException("Failed to connect to server.");
                }

                byte[] sendBuffer = Encoding.UTF8.GetBytes(command);

                SocketError error;
                clientSocket.Send(sendBuffer, 0, sendBuffer.Length, SocketFlags.None, out error);

                const int size = 4096;

                result = new StringBuilder(4096);
                var buffer = new byte[size];

                int length;

                do
                {
                    length = clientSocket.Receive(buffer, 0, size, SocketFlags.None, out error);
                    string response = Encoding.ASCII.GetString(buffer);
                    response = response.Substring(0, length);
                    result.Append(response);
                } while (length > 0);

                return result.ToString();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return "Failed to connect to server.";
            }
            finally
            {
                if (clientSocket.IsBound)
                    clientSocket.Shutdown(SocketShutdown.Both);

                clientSocket.Close();
            }
        }
    }
}