﻿namespace Ntp.Analyzer.Validate.Gui
{
    partial class FormValidator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.checkBoxValidate = new System.Windows.Forms.CheckBox();
            this.checkBoxDefaultValues = new System.Windows.Forms.CheckBox();
            this.checkBoxTabulator = new System.Windows.Forms.CheckBox();
            this.groupBoxFile = new System.Windows.Forms.GroupBox();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.labelFile = new System.Windows.Forms.Label();
            this.textBoxFile = new System.Windows.Forms.TextBox();
            this.panelButtom = new System.Windows.Forms.Panel();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panelTop.SuspendLayout();
            this.groupBoxOptions.SuspendLayout();
            this.groupBoxFile.SuspendLayout();
            this.panelButtom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTop.AutoSize = true;
            this.panelTop.Controls.Add(this.groupBoxOptions);
            this.panelTop.Controls.Add(this.groupBoxFile);
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(654, 111);
            this.panelTop.TabIndex = 0;
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxOptions.Controls.Add(this.checkBoxValidate);
            this.groupBoxOptions.Controls.Add(this.checkBoxDefaultValues);
            this.groupBoxOptions.Controls.Add(this.checkBoxTabulator);
            this.groupBoxOptions.Location = new System.Drawing.Point(493, 13);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(149, 85);
            this.groupBoxOptions.TabIndex = 1;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options";
            // 
            // checkBoxValidate
            // 
            this.checkBoxValidate.AutoSize = true;
            this.checkBoxValidate.Location = new System.Drawing.Point(18, 19);
            this.checkBoxValidate.Name = "checkBoxValidate";
            this.checkBoxValidate.Size = new System.Drawing.Size(86, 17);
            this.checkBoxValidate.TabIndex = 7;
            this.checkBoxValidate.Text = "Validate only";
            this.checkBoxValidate.UseVisualStyleBackColor = true;
            this.checkBoxValidate.CheckedChanged += new System.EventHandler(this.checkBoxValidate_CheckedChanged);
            // 
            // checkBoxDefaultValues
            // 
            this.checkBoxDefaultValues.AutoSize = true;
            this.checkBoxDefaultValues.Location = new System.Drawing.Point(18, 62);
            this.checkBoxDefaultValues.Name = "checkBoxDefaultValues";
            this.checkBoxDefaultValues.Size = new System.Drawing.Size(124, 17);
            this.checkBoxDefaultValues.TabIndex = 6;
            this.checkBoxDefaultValues.Text = "Show Default values";
            this.checkBoxDefaultValues.UseVisualStyleBackColor = true;
            this.checkBoxDefaultValues.CheckedChanged += new System.EventHandler(this.checkBoxDefaultValues_CheckedChanged);
            // 
            // checkBoxTabulator
            // 
            this.checkBoxTabulator.AutoSize = true;
            this.checkBoxTabulator.Location = new System.Drawing.Point(18, 41);
            this.checkBoxTabulator.Name = "checkBoxTabulator";
            this.checkBoxTabulator.Size = new System.Drawing.Size(89, 17);
            this.checkBoxTabulator.TabIndex = 5;
            this.checkBoxTabulator.Text = "Use tabulator";
            this.checkBoxTabulator.UseVisualStyleBackColor = true;
            this.checkBoxTabulator.CheckedChanged += new System.EventHandler(this.checkBoxTabulator_CheckedChanged);
            // 
            // groupBoxFile
            // 
            this.groupBoxFile.Controls.Add(this.buttonSelect);
            this.groupBoxFile.Controls.Add(this.labelFile);
            this.groupBoxFile.Controls.Add(this.textBoxFile);
            this.groupBoxFile.Location = new System.Drawing.Point(13, 13);
            this.groupBoxFile.Name = "groupBoxFile";
            this.groupBoxFile.Size = new System.Drawing.Size(474, 85);
            this.groupBoxFile.TabIndex = 0;
            this.groupBoxFile.TabStop = false;
            this.groupBoxFile.Text = "Configuration";
            // 
            // buttonSelect
            // 
            this.buttonSelect.Location = new System.Drawing.Point(390, 21);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(46, 20);
            this.buttonSelect.TabIndex = 3;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = true;
            this.buttonSelect.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelFile
            // 
            this.labelFile.AutoSize = true;
            this.labelFile.Location = new System.Drawing.Point(6, 23);
            this.labelFile.Name = "labelFile";
            this.labelFile.Size = new System.Drawing.Size(23, 13);
            this.labelFile.TabIndex = 2;
            this.labelFile.Text = "File";
            // 
            // textBoxFile
            // 
            this.textBoxFile.Location = new System.Drawing.Point(49, 23);
            this.textBoxFile.Name = "textBoxFile";
            this.textBoxFile.Size = new System.Drawing.Size(335, 20);
            this.textBoxFile.TabIndex = 1;
            // 
            // panelButtom
            // 
            this.panelButtom.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelButtom.Controls.Add(this.textBoxOutput);
            this.panelButtom.Location = new System.Drawing.Point(0, 117);
            this.panelButtom.Name = "panelButtom";
            this.panelButtom.Size = new System.Drawing.Size(653, 382);
            this.panelButtom.TabIndex = 1;
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxOutput.Location = new System.Drawing.Point(0, 0);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutput.Size = new System.Drawing.Size(653, 382);
            this.textBoxOutput.TabIndex = 0;
            // 
            // FormValidator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 499);
            this.Controls.Add(this.panelButtom);
            this.Controls.Add(this.panelTop);
            this.Name = "FormValidator";
            this.Text = "NTP Analyzer Validator";
            this.panelTop.ResumeLayout(false);
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxOptions.PerformLayout();
            this.groupBoxFile.ResumeLayout(false);
            this.groupBoxFile.PerformLayout();
            this.panelButtom.ResumeLayout(false);
            this.panelButtom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.GroupBox groupBoxFile;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Label labelFile;
        private System.Windows.Forms.TextBox textBoxFile;
        private System.Windows.Forms.Panel panelButtom;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.CheckBox checkBoxValidate;
        private System.Windows.Forms.CheckBox checkBoxDefaultValues;
        private System.Windows.Forms.CheckBox checkBoxTabulator;
    }
}

