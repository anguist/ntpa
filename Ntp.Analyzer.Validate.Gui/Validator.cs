﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Ntp.Analyzer.Config;
using Ntp.Analyzer.Config.Compiler;
using Ntp.Analyzer.Config.Node;

namespace Ntp.Analyzer.Validate.Gui
{
    public partial class FormValidator : Form
    {
        public FormValidator()
        {
            InitializeComponent();
            file = null;
        }

        private string file;

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            file = openFileDialog.FileName;
            textBoxFile.Text = file;
            textBoxOutput.Clear();
            ValidateConfiguration();
        }

        private void checkBoxDefaultValues_CheckedChanged(object sender, EventArgs e)
        {
            ValidateConfiguration();
        }

        private void checkBoxTabulator_CheckedChanged(object sender, EventArgs e)
        {
            ValidateConfiguration();
        }

        private void checkBoxValidate_CheckedChanged(object sender, EventArgs e)
        {
            ValidateConfiguration();
        }

        private Configuration LoadConfig(string configFile)
        {
            if (!File.Exists(configFile))
            {
                textBoxOutput.Text = $"Cannot find configuration file {configFile}";
                return null;
            }

            var reader = new ConfigBuilder(configFile);
            var config = reader.Execute();
            if (config != null)
                return config;

            textBoxOutput.Lines = reader.Errors.ToArray();
            return null;
        }

        private void ValidateConfiguration()
        {
            if (string.IsNullOrEmpty(file))
                return;

            textBoxOutput.Clear();

            Configuration config;
            try
            {
                config = LoadConfig(file);
            }
            catch (Exception e)
            {
                textBoxOutput.Text = @"Unexpected error while loading configuration file: " + e.Message;
                return;
            }

            if (config == null)
                return;

            if (checkBoxValidate.Checked)
            {
                textBoxOutput.Text = @"Configuration is valid.";
                return;
            }

            try
            {
                var decompiler = new Decompiler
                {
                    Configuration = config,
                    IndentChar = checkBoxTabulator.Checked ? '\t' : ' ',
                    IndentSize = checkBoxTabulator.Checked ? 1 : 8,
                    ShowDefaultValues = checkBoxDefaultValues.Checked
                };

                textBoxOutput.Text = decompiler.Execute();
            }
            catch (Exception e)
            {
                textBoxOutput.Text = @"Unexpected error while decompiling configuration: " + e.Message;
            }
        }
    }
}