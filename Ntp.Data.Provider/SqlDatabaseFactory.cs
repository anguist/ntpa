﻿// 
// Copyright (c) 2013-2017 Carsten Sonne Larsen <cs@innolan.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Data;

namespace Ntp.Data.Provider
{
    public abstract class SqlDatabaseFactory : ISqlFactory
    {
        protected static IDatabaseConfiguration Config;

        public static SqlDatabaseFactory Instance
        {
            get
            {
                switch (Config.Provider)
                {
                    case SqlDatabaseProvider.MySql:
                        return new MySqlFactory();
                    case SqlDatabaseProvider.PostgreSql:
                        return new PostgreSqlFactory();
                    case SqlDatabaseProvider.Unknown:
                        throw new NotSupportedException("Unknown SQL data provider was configured.");
                    default:
                        throw new NotSupportedException("No SQL data provider was configured.");
                }
            }
        }

        public abstract string PrepareCheckTableSql(string table);

        public abstract IDbConnection CreateConnection();

        public abstract IDbConnection CreateGenericConnection();

        public abstract IDbCommand CreateCommand();

        public abstract IDbDataParameter CreateParameter(string name, object value);

        public abstract void CreateDatabase();

        public abstract string DateAddMinutes(string dateColumn, string minuteColumn);

        public static void Initialize(IDatabaseConfiguration factoryConfiguration)
        {
            Config = factoryConfiguration;
        }

        public abstract string PrepareCreateTableSql(string sql);

        public abstract string PrepareInsertSql(string sql);

        public abstract string PrepareSql(string sql);
    }
}